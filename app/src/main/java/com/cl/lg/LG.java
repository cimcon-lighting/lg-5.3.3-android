package com.cl.lg;

import android.app.Application;
import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

import com.cl.lg.networking.API;
import com.cl.lg.utils.AppConstants;
import java.util.concurrent.TimeUnit;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.SSLSocketFactory;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class LG extends Application {

    SharedPreferences spf;
    String apiURL = "";

    public LG() {

    }

    public API networkCall(Context context, boolean isFromSecurtyCodeUI){
        Retrofit retrofit;
        spf = context.getSharedPreferences(AppConstants.SPF, MODE_PRIVATE);
        if (isFromSecurtyCodeUI)
            apiURL = AppConstants.BASE_URL;
        else
            apiURL = spf.getString(AppConstants.API_URL, "");

        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();

        if (AppConstants.isLogDisplay) {
            interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        } else
            interceptor.setLevel(HttpLoggingInterceptor.Level.NONE);

        /*OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
        httpClient.connectTimeout(180, TimeUnit.SECONDS);
        httpClient.readTimeout(180, TimeUnit.SECONDS);
        httpClient.addInterceptor(interceptor);*/

        Retrofit.Builder builder =
                new Retrofit.Builder()
                        .baseUrl(apiURL)
                        .addConverterFactory(GsonConverterFactory.create());

        retrofit = builder.client(getUnsafeOkHttpClient().build())
                .build();
        return retrofit.create(API.class);
    }

    //httpClient.build()
    public static OkHttpClient.Builder getUnsafeOkHttpClient() {

        try {
            // Create a trust manager that does not validate certificate chains
            final TrustManager[] trustAllCerts = new TrustManager[]{
                    new X509TrustManager() {
                        @Override
                        public void checkClientTrusted(java.security.cert.X509Certificate[] chain, String authType) {
                            Log.i("****",authType);
                        }

                        @Override
                        public void checkServerTrusted(java.security.cert.X509Certificate[] chain, String authType) {
                            Log.i("****",authType);
                        }

                        @Override
                        public java.security.cert.X509Certificate[] getAcceptedIssuers() {
                            return new java.security.cert.X509Certificate[]{};
                        }
                    }
            };

            // Install the all-trusting trust manager
            final SSLContext sslContext = SSLContext.getInstance("SSL");
            sslContext.init(null, trustAllCerts, new java.security.SecureRandom());

            HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
            if (AppConstants.isLogDisplay)
                interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
            else
                interceptor.setLevel(HttpLoggingInterceptor.Level.NONE);

            OkHttpClient.Builder builder = new OkHttpClient.Builder();

            builder.connectTimeout(180, TimeUnit.SECONDS);
            builder.readTimeout(180, TimeUnit.SECONDS);
            builder.addInterceptor(interceptor);

            // Create an ssl socket factory with our all-trusting manager
            final SSLSocketFactory sslSocketFactory = sslContext.getSocketFactory();
            builder.sslSocketFactory(sslSocketFactory, (X509TrustManager) trustAllCerts[0]);
            builder.hostnameVerifier(new HostnameVerifier() {
                @Override
                public boolean verify(String hostname, SSLSession session) {
                    return true;
                }
            });
            return builder;
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

}