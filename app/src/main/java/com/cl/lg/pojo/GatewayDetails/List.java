
package com.cl.lg.pojo.GatewayDetails;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class List {

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("rtuGroupId")
    @Expose
    private String rtuGroupId;
    @SerializedName("gatewayName")
    @Expose
    private String gatewayName;
    @SerializedName("gatewayDescription")
    @Expose
    private String gatewayDescription;
    @SerializedName("protocol")
    @Expose
    private String protocol;
    @SerializedName("version")
    @Expose
    private String version;
    @SerializedName("faulty")
    @Expose
    private Integer faulty;
    @SerializedName("dateTimeField")
    @Expose
    private Object dateTimeField;
    @SerializedName("deviceAddress")
    @Expose
    private String deviceAddress;
    @SerializedName("logRate")
    @Expose
    private String logRate;
    @SerializedName("commCount")
    @Expose
    private String commCount;
    @SerializedName("slcCount")
    @Expose
    private String slcCount;
    @SerializedName("communicationType")
    @Expose
    private Integer communicationType;
    @SerializedName("latitude")
    @Expose
    private String latitude;
    @SerializedName("longitude")
    @Expose
    private String longitude;
    @SerializedName("scanChannel")
    @Expose
    private String scanChannel;
    @SerializedName("gatewayMACAddress")
    @Expose
    private String gatewayMACAddress;
    @SerializedName("details")
    @Expose
    private Details details;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getRtuGroupId() {
        return rtuGroupId;
    }

    public void setRtuGroupId(String rtuGroupId) {
        this.rtuGroupId = rtuGroupId;
    }

    public String getGatewayName() {
        return gatewayName;
    }

    public void setGatewayName(String gatewayName) {
        this.gatewayName = gatewayName;
    }

    public String getGatewayDescription() {
        return gatewayDescription;
    }

    public void setGatewayDescription(String gatewayDescription) {
        this.gatewayDescription = gatewayDescription;
    }

    public String getProtocol() {
        return protocol;
    }

    public void setProtocol(String protocol) {
        this.protocol = protocol;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public Integer getFaulty() {
        return faulty;
    }

    public void setFaulty(Integer faulty) {
        this.faulty = faulty;
    }

    public Object getDateTimeField() {
        return dateTimeField;
    }

    public void setDateTimeField(Object dateTimeField) {
        this.dateTimeField = dateTimeField;
    }

    public String getDeviceAddress() {
        return deviceAddress;
    }

    public void setDeviceAddress(String deviceAddress) {
        this.deviceAddress = deviceAddress;
    }

    public String getLogRate() {
        return logRate;
    }

    public void setLogRate(String logRate) {
        this.logRate = logRate;
    }

    public String getCommCount() {
        return commCount;
    }

    public void setCommCount(String commCount) {
        this.commCount = commCount;
    }

    public String getSlcCount() {
        return slcCount;
    }

    public void setSlcCount(String slcCount) {
        this.slcCount = slcCount;
    }

    public Integer getCommunicationType() {
        return communicationType;
    }

    public void setCommunicationType(Integer communicationType) {
        this.communicationType = communicationType;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public String getScanChannel() {
        return scanChannel;
    }

    public void setScanChannel(String scanChannel) {
        this.scanChannel = scanChannel;
    }

    public String getGatewayMACAddress() {
        return gatewayMACAddress;
    }

    public void setGatewayMACAddress(String gatewayMACAddress) {
        this.gatewayMACAddress = gatewayMACAddress;
    }

    public Details getDetails() {
        return details;
    }

    public void setDetails(Details details) {
        this.details = details;
    }

}
