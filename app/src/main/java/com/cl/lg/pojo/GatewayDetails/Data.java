
package com.cl.lg.pojo.GatewayDetails;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Data {

    @SerializedName("list")
    @Expose
    private java.util.List<com.cl.lg.pojo.GatewayDetails.List> list = null;
    @SerializedName("totalRecords")
    @Expose
    private Integer totalRecords;

    public java.util.List<com.cl.lg.pojo.GatewayDetails.List> getList() {
        return list;
    }

    public void setList(java.util.List<com.cl.lg.pojo.GatewayDetails.List> list) {
        this.list = list;
    }

    public Integer getTotalRecords() {
        return totalRecords;
    }

    public void setTotalRecords(Integer totalRecords) {
        this.totalRecords = totalRecords;
    }

}
