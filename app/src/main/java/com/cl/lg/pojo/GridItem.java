package com.cl.lg.pojo;

import android.graphics.drawable.Drawable;

public class GridItem {

    Drawable mDrawable;
    String mText;
    public GridItem(Drawable mDrawable, String mText) {
        this.mDrawable = mDrawable;
        this.mText = mText;
    }
    public Drawable getmDrawable() {
        return mDrawable;
    }
    public String getmText() {
        return mText;
    }

}
