package com.cl.lg.pojo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class SecurityData {

    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("ClientID")
    @Expose
    private String clientID;
    @SerializedName("userid")
    @Expose
    private String userid;
    @SerializedName("APIURL")
    @Expose
    private String aPIURL;

    @SerializedName("apptype")
    @Expose
    private String apptype;

    @SerializedName("apptypemessage")
    @Expose
    private String apptypemessage;


    @SerializedName("nearmedistance")
    @Expose
    private String nearmedistance;


    @SerializedName("nearmedistancemessage")
    @Expose
    private String nearmedistancemessage;

    @SerializedName("ClientName")
    @Expose
    private String ClientName;

    public String getClientName() {
        return ClientName;
    }

    public void setClientName(String clientName) {
        ClientName = clientName;
    }

    public String getNearmedistance() {
        return nearmedistance;
    }

    public void setNearmedistance(String nearmedistance) {
        this.nearmedistance = nearmedistance;
    }

    public String getNearmedistancemessage() {
        return nearmedistancemessage;
    }

    public void setNearmedistancemessage(String nearmedistancemessage) {
        this.nearmedistancemessage = nearmedistancemessage;
    }

    public String getApptype() {
        return apptype;
    }

    public void setApptype(String apptype) {
        this.apptype = apptype;
    }

    public String getApptypemessage() {
        return apptypemessage;
    }

    public void setApptypemessage(String apptypemessage) {
        this.apptypemessage = apptypemessage;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getClientID() {
        return clientID;
    }

    public void setClientID(String clientID) {
        this.clientID = clientID;
    }

    public String getUserid() {
        return userid;
    }

    public void setUserid(String userid) {
        this.userid = userid;
    }

    public String getAPIURL() {
        return aPIURL;
    }

    public void setAPIURL(String aPIURL) {
        this.aPIURL = aPIURL;
    }

}