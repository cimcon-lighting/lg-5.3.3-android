
package com.cl.lg.pojo.LoginMaster;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class LoginMaster {


    @SerializedName("message")
    @Expose
    private String message;

    @SerializedName("data")
    @Expose
    private Data data;
    @SerializedName("status")
    @Expose
    private String status;

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {        return message;    }

    public void setMessage(String message) {        this.message = message;    }
}
