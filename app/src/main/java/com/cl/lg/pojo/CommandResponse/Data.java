
package com.cl.lg.pojo.CommandResponse;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Data {

    @SerializedName("msg")
    @Expose
    private String msg;
    @SerializedName("trackingIDs")
    @Expose
    private List<String> trackingIDs = null;

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public List<String> getTrackingIDs() {
        return trackingIDs;
    }

    public void setTrackingIDs(List<String> trackingIDs) {
        this.trackingIDs = trackingIDs;
    }

}
