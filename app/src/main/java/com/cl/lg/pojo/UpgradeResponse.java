package com.cl.lg.pojo;

import com.cl.lg.pojo.CommandResponse.Data;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class UpgradeResponse {

    @SerializedName("status")
    @Expose
    private String status;

    @SerializedName("message")
    @Expose
    private String msg;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }
}
