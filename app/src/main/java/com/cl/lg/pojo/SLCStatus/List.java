package com.cl.lg.pojo.SLCStatus;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class List {

    @SerializedName("roW_NUMBER")
    @Expose
    private Integer roWNUMBER;
    @SerializedName("gatewayId")
    @Expose
    private Integer gatewayId;
    @SerializedName("nmsStatus")
    @Expose
    private Integer nmsStatus;
    @SerializedName("slcNo")
    @Expose
    private Integer slcNo;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("datetime")
    @Expose
    private String dateTime;
    @SerializedName("d1@LS")
    @Expose
    private String d1LS;
    @SerializedName("d2@PS")
    @Expose
    private String d2PS;
    @SerializedName("d5@PO")
    @Expose
    private String d5PO;
    @SerializedName("d8@C")
    @Expose
    private String d8C;
    @SerializedName("d12@EOF")
    @Expose
    private String d12EOF;
    @SerializedName("d14@R")
    @Expose
    private String d14R;
    @SerializedName("d16@Day Burning")
    @Expose
    private String d16DayBurning;
    @SerializedName("d17@Pole Fault")
    @Expose
    private String d17PoleFault;
    @SerializedName("d18@Photocell Fault")
    @Expose
    private String d18PhotocellFault;
    @SerializedName("d19@Controller Fault")
    @Expose
    private String d19ControllerFault;
    @SerializedName("a1@Volt")
    @Expose
    private Double a1Volt;
    @SerializedName("a2@Cur")
    @Expose
    private Double a2Cur;
    @SerializedName("a3@KW")
    @Expose
    private Double a3KW;
    @SerializedName("a4@Cum_Kwh")
    @Expose
    private Double a4CumKwh;
    @SerializedName("a5@BHrs")
    @Expose
    private Double a5BHrs;
    @SerializedName("a6@Dim")
    @Expose
    private Double a6Dim;
    @SerializedName("a7@PF")
    @Expose
    private Double a7PF;
    @SerializedName("a8@LSC")
    @Expose
    private Double a8LSC;
    @SerializedName("a9@Mode-Mode")
    @Expose
    private Double a9ModeMode;
    @SerializedName("a10@Temperature")
    @Expose
    private Double a10Temperature;
    @SerializedName("tag8")
    @Expose
    private String tag8;

    boolean isChecked;

    String jsonString;

    public boolean isChecked() {
        return isChecked;
    }

    public void setChecked(boolean checked) {
        isChecked = checked;
    }

    public String getJsonString() {
        return jsonString;
    }

    public void setJsonString(String jsonString) {
        this.jsonString = jsonString;
    }

    public Integer getRoWNUMBER() {
        return roWNUMBER;
    }

    public void setRoWNUMBER(Integer roWNUMBER) {
        this.roWNUMBER = roWNUMBER;
    }

    public Integer getGatewayId() {
        return gatewayId;
    }

    public void setGatewayId(Integer gatewayId) {
        this.gatewayId = gatewayId;
    }

    public Integer getNmsStatus() {
        return nmsStatus;
    }

    public void setNmsStatus(Integer nmsStatus) {
        this.nmsStatus = nmsStatus;
    }

    public Integer getSlcNo() {
        return slcNo;
    }

    public void setSlcNo(Integer slcNo) {
        this.slcNo = slcNo;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDateTime() {
        return dateTime;
    }

    public void setDateTime(String dateTime) {
        this.dateTime = dateTime;
    }

    public String getD1LS() {
        return d1LS;
    }

    public void setD1LS(String d1LS) {
        this.d1LS = d1LS;
    }

    public String getD2PS() {
        return d2PS;
    }

    public void setD2PS(String d2PS) {
        this.d2PS = d2PS;
    }

    public String getD5PO() {
        return d5PO;
    }

    public void setD5PO(String d5PO) {
        this.d5PO = d5PO;
    }

    public String getD8C() {
        return d8C;
    }

    public void setD8C(String d8C) {
        this.d8C = d8C;
    }

    public String getD12EOF() {
        return d12EOF;
    }

    public void setD12EOF(String d12EOF) {
        this.d12EOF = d12EOF;
    }

    public String getD14R() {
        return d14R;
    }

    public void setD14R(String d14R) {
        this.d14R = d14R;
    }

    public String getD16DayBurning() {
        return d16DayBurning;
    }

    public void setD16DayBurning(String d16DayBurning) {
        this.d16DayBurning = d16DayBurning;
    }

    public String getD17PoleFault() {
        return d17PoleFault;
    }

    public void setD17PoleFault(String d17PoleFault) {
        this.d17PoleFault = d17PoleFault;
    }

    public String getD18PhotocellFault() {
        return d18PhotocellFault;
    }

    public void setD18PhotocellFault(String d18PhotocellFault) {
        this.d18PhotocellFault = d18PhotocellFault;
    }

    public String getD19ControllerFault() {
        return d19ControllerFault;
    }

    public void setD19ControllerFault(String d19ControllerFault) {
        this.d19ControllerFault = d19ControllerFault;
    }


    public String getTag8() {
        return tag8;
    }

    public void setTag8(String tag8) {
        this.tag8 = tag8;
    }

    public Double getA1Volt() {
        return a1Volt;
    }

    public void setA1Volt(Double a1Volt) {
        this.a1Volt = a1Volt;
    }

    public Double getA2Cur() {
        return a2Cur;
    }

    public void setA2Cur(Double a2Cur) {
        this.a2Cur = a2Cur;
    }

    public Double getA3KW() {
        return a3KW;
    }

    public void setA3KW(Double a3KW) {
        this.a3KW = a3KW;
    }

    public Double getA4CumKwh() {
        return a4CumKwh;
    }

    public void setA4CumKwh(Double a4CumKwh) {
        this.a4CumKwh = a4CumKwh;
    }

    public Double getA5BHrs() {
        return a5BHrs;
    }

    public void setA5BHrs(Double a5BHrs) {
        this.a5BHrs = a5BHrs;
    }

    public Double getA6Dim() {
        return a6Dim;
    }

    public void setA6Dim(Double a6Dim) {
        this.a6Dim = a6Dim;
    }

    public Double getA7PF() {
        return a7PF;
    }

    public void setA7PF(Double a7PF) {
        this.a7PF = a7PF;
    }

    public Double getA8LSC() {
        return a8LSC;
    }

    public void setA8LSC(Double a8LSC) {
        this.a8LSC = a8LSC;
    }

    public Double getA9ModeMode() {
        return a9ModeMode;
    }

    public void setA9ModeMode(Double a9ModeMode) {
        this.a9ModeMode = a9ModeMode;
    }

    public Double getA10Temperature() {
        return a10Temperature;
    }

    public void setA10Temperature(Double a10Temperature) {
        this.a10Temperature = a10Temperature;
    }
}