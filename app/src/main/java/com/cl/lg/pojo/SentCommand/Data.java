
package com.cl.lg.pojo.SentCommand;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Data {

    @SerializedName("trackNetworkList")
    @Expose
    private List<TrackNetworkList> trackNetworkList = null;
    @SerializedName("totalRecords")
    @Expose
    private String totalRecords;

    public List<TrackNetworkList> getTrackNetworkList() {
        return trackNetworkList;
    }

    public void setTrackNetworkList(List<TrackNetworkList> trackNetworkList) {
        this.trackNetworkList = trackNetworkList;
    }

    public String getTotalRecords() {
        return totalRecords;
    }

    public void setTotalRecords(String totalRecords) {
        this.totalRecords = totalRecords;
    }

}
