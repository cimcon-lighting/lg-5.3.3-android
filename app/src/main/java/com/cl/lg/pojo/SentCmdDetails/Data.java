
package com.cl.lg.pojo.SentCmdDetails;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Data {

    @SerializedName("trackDetail")
    @Expose
    private List<TrackDetail> trackDetail = null;
    @SerializedName("totalRecords")
    @Expose
    private String totalRecords;

    public List<TrackDetail> getTrackDetail() {
        return trackDetail;
    }

    public void setTrackDetail(List<TrackDetail> trackDetail) {
        this.trackDetail = trackDetail;
    }

    public String getTotalRecords() {
        return totalRecords;
    }

    public void setTotalRecords(String totalRecords) {
        this.totalRecords = totalRecords;
    }

}
