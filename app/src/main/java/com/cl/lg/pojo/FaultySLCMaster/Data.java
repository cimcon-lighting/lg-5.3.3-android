
package com.cl.lg.pojo.FaultySLCMaster;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Data {

    @SerializedName("slcList")
    @Expose
    private List<SlcList> slcList = null;
    @SerializedName("totalRecords")
    @Expose
    private String totalRecords;

    public List<SlcList> getSlcList() {
        return slcList;
    }

    public void setSlcList(List<SlcList> slcList) {
        this.slcList = slcList;
    }

    public String getTotalRecords() {
        return totalRecords;
    }

    public void setTotalRecords(String totalRecords) {
        this.totalRecords = totalRecords;
    }

}
