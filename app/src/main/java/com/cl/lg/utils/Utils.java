package com.cl.lg.utils;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.Rect;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.location.Location;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.DrawableRes;
import androidx.core.app.ActivityCompat;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.cl.lg.LG;
import com.cl.lg.R;
import com.cl.lg.activities.MainActivity;
import com.cl.lg.activities.SecurityCodeActivity;
import com.cl.lg.adapters.InfoGridAdapter;
import com.cl.lg.fragment.SLCDetailsFragment;
import com.cl.lg.fragment.StatusFragment;
import com.cl.lg.networking.API;
import com.cl.lg.pojo.Command;
import com.cl.lg.pojo.DashboardCounts.LampType;
import com.cl.lg.pojo.GridItem;
import com.cl.lg.pojo.LatLong;
import com.cl.lg.pojo.Parameters.Datum;
import com.cl.lg.pojo.UpgradeResponse;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.TileOverlayOptions;
import com.google.android.gms.maps.model.UrlTileProvider;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.lang.reflect.Type;
import java.math.BigDecimal;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Set;
import java.util.TreeSet;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.cl.lg.utils.AppConstants.TAG;

public class Utils {
    public void loadFragment(Fragment fragment, Activity activity) {
        try {
            if (activity != null) {

                if (!fragment.isVisible()) {
                    // create a FragmentManager
                    FragmentManager fm = activity.getFragmentManager();
                    // create a FragmentTransaction to begin the transaction and replace the Fragment
                    FragmentTransaction fragmentTransaction = fm.beginTransaction();
                    // replace the FrameLayout with new Fragment
                    fragmentTransaction.replace(R.id.frm1, fragment);
                    //if (fragment instanceof DashboardFragment)
                    fragmentTransaction.addToBackStack(null);
                    //fragmentTransaction.commit(); // save the changes
                    fragmentTransaction.commit();
                }
            }
        } catch (Exception e) {
        }
    }


    public void loadFragmentMain(Fragment fragment, Activity activity) {
        try {
            if (activity != null) {

                FragmentManager fm = activity.getFragmentManager();
                FragmentTransaction fragmentTransaction = fm.beginTransaction();
                fragmentTransaction.replace(R.id.frm1, fragment);
                //fragmentTransaction.commit(); // save the changes
                fragmentTransaction.commitAllowingStateLoss();
            }
        } catch (Exception e) {
        }
    }


   /* public void loadFragment2(android.support.v4.app.Fragment fragment, Activity activity) {
        try {
            if(activity!=null) {

                android.support.v4.app.FragmentManager fragmentManager = activity.getSupportFragmentManager();
                android.support.v4.app.FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                ExampleFragments fragment = new ExampleFragments();
                fragmentTransaction.replace(R.id.frag, fragment);
                fragmentTransaction.commit();

                FragmentTransaction ft = activity.getFragmentManager().beginTransaction();

                ft.replace(R.id.frm1, fragment);
                ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);

                // create a FragmentManager
                Manager
                // create a FragmentTransaction to begin the transaction and replace the Fragment
                FragmentTransaction fragmentTransaction = fragment.beginTransaction();
                // replace the FrameLayout with new Fragment
                fragmentTransaction.replace(R.id.frm1, fragment);

                //fragmentTransaction.commit(); // save the changes
                fragmentTransaction.commitAllowingStateLoss();


                FragmentTransaction ft = getFragmentManager().beginTransaction();
                ft.replace(R.id.simple_fragment, fragment);
                ft.setTransition(transition);
                if (addBacktoStack)
                    ft.addToBackStack(null);
                ft.commit();

            }
        }catch (Exception e){}
    }*/


    public static boolean isInternetAvailable(Context context) {
        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        return activeNetwork != null && activeNetwork.isConnectedOrConnecting();
    }

    public static String getDeviceLocale(SharedPreferences spf) {
        //String locale = Locale.getDefault().getLanguage();
        String locale = Resources.getSystem().getConfiguration().locale.getLanguage();
        String langCode;
        if (locale.equals(AppConstants.LANGUAGE_CODE_ENGLISH) || locale.equals(AppConstants.LANGUAGE_CODE_SPANISH) || locale.equals(AppConstants.LANGUAGE_CODE_PORTUGUES))
            langCode = spf.getString(AppConstants.LANGUAGE_LOCALE, locale);
        else
            langCode = AppConstants.LANGUAGE_CODE_ENGLISH;
        return langCode;
    }

    public static String getDeviceLocaleActual() {
        //String locale = Locale.getDefault().getLanguage();
        String locale = Resources.getSystem().getConfiguration().locale.getLanguage();
        String langCode;
        if (locale.equals(AppConstants.LANGUAGE_CODE_ENGLISH))
            langCode = AppConstants.LANGUAGE_CODE_ENGLISH;
        else if (locale.equals(AppConstants.LANGUAGE_CODE_SPANISH))
            langCode = AppConstants.LANGUAGE_CODE_SPANISH;
        else if (locale.equals(AppConstants.LANGUAGE_CODE_PORTUGUES))
            langCode = AppConstants.LANGUAGE_CODE_PORTUGUES;
        else
            langCode = AppConstants.LANGUAGE_CODE_ENGLISH;
        return langCode;
    }

    public void switchLanguage(Activity activity, String languageCode, boolean isFromMain) {
        try {
            if (activity != null) {
                LanguageHelper.setLanguage(activity, languageCode);
                if (!isFromMain)
                    relaunch(activity);
            }
        } catch (Exception e) {
        }
    }

    public void relaunch(Activity activity) {
        Intent intent;
        intent = new Intent(activity, MainActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.putExtra("language", "yes");
        activity.startActivity(intent);
        Runtime.getRuntime().exit(0);
        activity.finish();

    }

    public void dismissProgressDialog(Dialog pDialog) {
        try {
            if (pDialog != null && pDialog.isShowing()) {
                pDialog.dismiss();
            }
        } catch (Exception e) {

        }
    }

    public static void dialogForMessage(Activity activity, String message) {
        if (activity != null) {
            androidx.appcompat.app.AlertDialog.Builder builder = new androidx.appcompat.app.AlertDialog.Builder(activity);
            builder.setMessage(message)
                    .setTitle(R.string.app_name)
                    .setCancelable(true)
                    .setPositiveButton(activity.getResources().getString(R.string.ok), new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            dialogInterface.dismiss();
                        }
                    });
            //Creating dialog box
            androidx.appcompat.app.AlertDialog alert = builder.create();
            //Setting the title manually
            alert.requestWindowFeature(Window.FEATURE_NO_TITLE);
            alert.show();
        }
    }


    public void dialogForNavigatePlayStore(Activity activity, String message, String packageName) {
        if (activity != null) {
            androidx.appcompat.app.AlertDialog.Builder builder = new androidx.appcompat.app.AlertDialog.Builder(activity);
            builder.setMessage(message)
                    .setCancelable(true)
                    .setPositiveButton(activity.getResources().getString(R.string.ok), new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            dialogInterface.dismiss();
                            openPlayStore(activity, packageName);
                        }
                    })
                    .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    });
            //Creating dialog box
            androidx.appcompat.app.AlertDialog alert = builder.create();
            //Setting the title manually
            alert.requestWindowFeature(Window.FEATURE_NO_TITLE);
            alert.show();
        }
    }

    void openPlayStore(Activity activity, String packageName) {
        try {
            activity.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + packageName)));
        } catch (android.content.ActivityNotFoundException anfe) {
            activity.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + packageName)));
        }
    }


    public static void dialogForMessageRoute(Activity activity, String message) {
        if (activity != null) {
            androidx.appcompat.app.AlertDialog.Builder builder = new androidx.appcompat.app.AlertDialog.Builder(activity);
            builder.setMessage(message)
                    .setCancelable(true)
                    .setPositiveButton("Close", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            dialogInterface.dismiss();
                        }
                    });
            //Creating dialog box
            androidx.appcompat.app.AlertDialog alert = builder.create();
            //Setting the title manually
            alert.requestWindowFeature(Window.FEATURE_NO_TITLE);
            alert.show();
        }
    }

    public static void dialogForMessageWithTitle(Activity activity, String message, String title) {
        if (activity != null) {
            androidx.appcompat.app.AlertDialog.Builder builder = new androidx.appcompat.app.AlertDialog.Builder(activity);

            builder.setTitle(title)
                    .setMessage(message)
                    .setCancelable(true)
                    .setPositiveButton(activity.getResources().getString(R.string.ok), new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            dialogInterface.dismiss();

                        }
                    });
            //Creating dialog box
            androidx.appcompat.app.AlertDialog alert = builder.create();
            //Setting the title manually
            alert.requestWindowFeature(Window.FEATURE_NO_TITLE);
            alert.show();
        }
    }

    public static void dialogAuth(Activity activity, String message, String title) {
        if (activity != null) {
            androidx.appcompat.app.AlertDialog.Builder builder = new androidx.appcompat.app.AlertDialog.Builder(activity);

            final SharedPreferences spf = activity.getSharedPreferences(AppConstants.SPF_LOGIN, Context.MODE_PRIVATE);
            builder.setTitle(title)
                    .setMessage(message)
                    .setCancelable(true)
                    .setPositiveButton(activity.getResources().getString(R.string.ok), new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            dialogInterface.dismiss();
                            spf.edit().putBoolean(AppConstants.ALLOW_FACE_TOUCH_ID, true).apply();

                        }
                    }).setNegativeButton("Don't Allow", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    dialogInterface.dismiss();
                    spf.edit().putBoolean(AppConstants.ALLOW_FACE_TOUCH_ID, false).apply();
                }
            })

            ;
            //Creating dialog box
            androidx.appcompat.app.AlertDialog alert = builder.create();
            //Setting the title manually
            alert.requestWindowFeature(Window.FEATURE_NO_TITLE);
            alert.show();
        }
    }

    public void setMapType(final Context context, GoogleMap googleMap, final SharedPreferences spf) {
        final int selectedMapIndex = spf.getInt(AppConstants.SELECTED_MAP_INDEX, 0);

        if (selectedMapIndex == 0) {
            googleMap.clear();
            googleMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
        } else if (selectedMapIndex == 1) {
            googleMap.setMapType(GoogleMap.MAP_TYPE_SATELLITE);
        } else if (selectedMapIndex == 2) {
            googleMap.setMapType(GoogleMap.MAP_TYPE_HYBRID);
        } else if (selectedMapIndex == 3) {
            TileOverlayOptions options1 = new TileOverlayOptions();
            googleMap.setMapType(GoogleMap.MAP_TYPE_NONE);
            options1.tileProvider(new UrlTileProvider(256, 256) {
                @Override
                public synchronized URL getTileUrl(int x, int y, int zoom) {
                    String styleUrl = String.format(Locale.US, AppConstants.OPEN_STREET_MAP_STANDARD_URL, zoom, x, y);
                    //String s = String.format(Locale.US,selectedMap,zoom, x, y);
                    URL url = null;
                    try {
                        url = new URL(styleUrl);
                    } catch (MalformedURLException e) {
                        throw new AssertionError(e);
                    }
                    return url;
                }
            });
            googleMap.addTileOverlay(options1);
        }

         /*else if (selectedMap.equalsIgnoreCase(context.getResources().getString(R.string.google_map_terrian))) {
            googleMap.setMapType(GoogleMap.MAP_TYPE_TERRAIN);
        }*/
    }

    public Bitmap getMarkerBitmapFromView(Activity activity, View view, @DrawableRes int resId, ImageView marker_image) {


        if (resId == R.drawable.pin_gif) {
            Glide.with(activity).asGif().load(resId).into(marker_image);
            //Glide.with(activity).load(resId).into(marker_image);
        } else
            marker_image.setImageResource(resId);

        view.measure(View.MeasureSpec.UNSPECIFIED, View.MeasureSpec.UNSPECIFIED);
        view.layout(0, 0, view.getMeasuredWidth(), view.getMeasuredHeight());
        view.buildDrawingCache();
        Bitmap returnedBitmap = Bitmap.createBitmap(view.getMeasuredWidth(), view.getMeasuredHeight(),
                Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(returnedBitmap);
        canvas.drawColor(Color.WHITE, PorterDuff.Mode.SRC_IN);
        Drawable drawable = view.getBackground();
        if (drawable != null)
            drawable.draw(canvas);
        view.draw(canvas);
        return returnedBitmap;
    }

    public Bitmap getMarkerBitmapFromViewP(Activity activity, View view, @DrawableRes int resId, ImageView marker_image) {

        marker_image.setImageResource(resId);

        int height = 40;
        int width = 25;

        int paddingTop = 20;
        int paddingBottom = 5;
        int paddingLeft = 20;
        int paddingRight = 5;

        view.measure(View.MeasureSpec.UNSPECIFIED, View.MeasureSpec.UNSPECIFIED);
        view.layout(0, 0, view.getMeasuredWidth(), view.getMeasuredHeight());
        view.buildDrawingCache();

        Bitmap returnedBitmap = Bitmap.createBitmap(view.getMeasuredWidth(), view.getMeasuredHeight(),
                Bitmap.Config.ARGB_8888);

        Canvas canvas = new Canvas(returnedBitmap);
        canvas.drawColor(Color.WHITE, PorterDuff.Mode.SRC_IN);
        canvas.drawBitmap(returnedBitmap, paddingLeft, paddingTop, null);
        Drawable drawable = view.getBackground();
        if (drawable != null)
            drawable.draw(canvas);
        view.draw(canvas);

        return returnedBitmap;
    }


    public static boolean hasPermissions(Context context, String... permissions) {

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && context != null && permissions != null) {
            for (String permission : permissions) {
                if (ActivityCompat.checkSelfPermission(context, permission) != PackageManager.PERMISSION_GRANTED) {
                    return false;
                }
            }
        }
        return true;
    }

    public void openDetailFragment(Activity activity, String title, String statusPara, String powerPara, String neverComm, String servicereq, boolean isFromSLCMode, int llnever, boolean isFromLightStatus, boolean isFromService) {

        FragmentManager fm = activity.getFragmentManager();
        FragmentTransaction fragmentTransaction = fm.beginTransaction();

        SLCDetailsFragment fragment = new SLCDetailsFragment();
        Bundle objBundle = new Bundle();
        objBundle.putString(AppConstants.TITLE, title);
        objBundle.putString(AppConstants.STATUS_PARA, statusPara);
        objBundle.putString(AppConstants.POWER_PARA, powerPara);

        objBundle.putString(AppConstants.NEVER_COMM, neverComm);

        objBundle.putString(AppConstants.SERVICE_REQUEST, servicereq);
        objBundle.putBoolean(AppConstants.ISFROMSLCM, isFromSLCMode);
        objBundle.putBoolean(AppConstants.ISFROMSLCLIGHTSTATUS, isFromLightStatus);
        objBundle.putBoolean(AppConstants.IS_FROM_SERVICE, isFromService);
        objBundle.putInt(AppConstants.UI_ID, llnever);
        fragment.setArguments(objBundle);

        fragmentTransaction.replace(R.id.frm1, fragment);
        fragmentTransaction.addToBackStack(null);
        fragmentTransaction.commitAllowingStateLoss();

    }

    public void openStatus(Activity activity, int id, int visible, boolean isNearMe, int visibilityFromSent) {


        FragmentManager fm = activity.getFragmentManager();

        FragmentTransaction fragmentTransaction = fm.beginTransaction();
        StatusFragment fragment = new StatusFragment();

        Bundle objBundle = new Bundle();
        objBundle.putInt(AppConstants.UI_ID, id);
        objBundle.putInt(AppConstants.IS_SEARCH_ON, visible);
        objBundle.putBoolean(AppConstants.IS_NEAR_ME, isNearMe);
        objBundle.putInt(AppConstants.IS_SEARCH_ON_SENT_CMD, visibilityFromSent);

        fragment.setArguments(objBundle);

        fragmentTransaction.replace(R.id.frm1, fragment);
        fragmentTransaction.addToBackStack(null);
        fragmentTransaction.commit();

    }

    public void responseHandle(final Activity activity, int code, final ResponseBody body) {
        if (activity != null) {
            String msg = activity.getResources().getString(R.string.http_error_msg);
            String status = "";
            try {
                JSONObject jObjError = new JSONObject(body.string());
                msg = jObjError.getString("message").toString();
                status = jObjError.getString("status").toString();

            } catch (JSONException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            if (code == 401 || code == 500) {
                if (activity != null) {
                    androidx.appcompat.app.AlertDialog.Builder builder = new androidx.appcompat.app.AlertDialog.Builder(activity);
                    final String finalStatus = status;
                    builder.setMessage(getStringResourceByName(activity, msg))
                            .setCancelable(true)
                            .setPositiveButton(activity.getResources().getString(R.string.ok), new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialogInterface, int i) {
                                    dialogInterface.dismiss();
                                    //body.getStatus();
                                    if (!finalStatus.equals("-1")) {
                                        activity.finish();
                                        activity.startActivity(new Intent(activity, SecurityCodeActivity.class));
                                    }
                                }
                            });

                    //Creating dialog box
                    androidx.appcompat.app.AlertDialog alert = builder.create();
                    //Setting the title manually
                    alert.requestWindowFeature(Window.FEATURE_NO_TITLE);
                    alert.show();
                }
            } else
                Utils.dialogForMessage(activity, getStringResourceByName(activity, msg.toString()));
        }
    }


    public void responseHandleLogin(final Activity activity, int code, final ResponseBody body) {
        if (activity != null) {
            String msg = activity.getResources().getString(R.string.http_error_msg);
            String status = "";
            try {
                JSONObject jObjError = new JSONObject(body.string());
                msg = jObjError.getString("message").toString();
                status = jObjError.getString("status").toString();

            } catch (JSONException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }

            if (code == 401) {
                if (status.equals("-1")) {
                    if (msg.contains("|"))
                        dialogForMessagePlayStoreNavigate(activity, getStringResourceByName(activity, msg));
                    else
                        dialogForMessage(activity, getStringResourceByName(activity, msg));
                } else
                    dialogNavigateSecurityUi(activity, getStringResourceByName(activity, msg));

            } else if (code == 500)
                dialogForMessage(activity, getStringResourceByName(activity, msg));
            else if (status.equals("-1")) //any response code but status -1
                dialogForMessagePlayStoreNavigate(activity, getStringResourceByName(activity, msg));
            else //default message
                dialogForMessage(activity, getStringResourceByName(activity, msg));

        }
    }

    public void dialogNavigateSecurityUi(Activity activity, String msg) {
        if (activity != null) {
            androidx.appcompat.app.AlertDialog.Builder builder = new androidx.appcompat.app.AlertDialog.Builder(activity);
            String finalMsg = msg;
            builder.setMessage(getStringResourceByName(activity, msg))
                    .setTitle(R.string.app_name)
                    .setCancelable(true)
                    .setPositiveButton(activity.getResources().getString(R.string.ok), new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            //body.getStatus();
                            dialogInterface.dismiss();
                            activity.finish();
                            activity.startActivity(new Intent(activity, SecurityCodeActivity.class));
                        }
                    });

            //Creating dialog box
            androidx.appcompat.app.AlertDialog alert = builder.create();
            //Setting the title manually
            alert.requestWindowFeature(Window.FEATURE_NO_TITLE);
            alert.show();
        }
    }

    public static void addLog(String msg) {
        if (AppConstants.isLogDisplay) {

            try {
                SimpleDateFormat dateFormat = new SimpleDateFormat("MM-dd-yyyy '|' HH:mm:ss ZZZZZ", Locale.ENGLISH);
                Date dt = new Date();
                File root = Environment.getExternalStorageDirectory();
                if (root.canWrite()) {
                    File dir = new File(root + "/LightingGale");
                    if (!dir.exists())
                        dir.mkdir();

                    File file = new File(dir, "log.txt");

                    FileWriter writer = new FileWriter(file, true);
                    BufferedWriter out = new BufferedWriter(writer);
                    out.append("\n [" + dateFormat.format(new Date()) + "] " + msg + "\n");
                    out.close();
                }
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

 /*   public static void fabricEvent(Activity activity) {

        try {
            PackageInfo pInfo = activity.getPackageManager().getPackageInfo(activity.getPackageName(), 0);
            String version = activity.getResources().getString(R.string.version) + pInfo.versionName;
            Answers.getInstance().logCustom(new CustomEvent("Device Info")
                    .putCustomAttribute(
                            "Device Info", Build.MANUFACTURER +
                                    " | " + Build.MODEL +
                                    " | OS " + Build.VERSION.RELEASE +
                                    " | API: " + Build.VERSION.SDK_INT
                                    + " | " + version)
            );

        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }

        // TODO: Use your own string attributes to track common values over time
        // TODO: Use your own number attributes to track median value over time

    }*/

    public static void SaveArraylistInSPF(SharedPreferences spf, List<Datum> arrayList) {
        //Set the values
        Gson gson = new Gson();
        ArrayList<com.cl.lg.pojo.Parameters.Datum> textList = new ArrayList<com.cl.lg.pojo.Parameters.Datum>();
        textList.addAll(arrayList);
        String jsonText = gson.toJson(textList);
        spf.edit().putString(AppConstants.SPF_PARAMETERS_DATA, jsonText).apply();
        if (arrayList.size() > 0)
            spf.edit().putBoolean(AppConstants.SPF_PARAMETERS_DATA_ADDED, true).commit();
        else
            spf.edit().putBoolean(AppConstants.SPF_PARAMETERS_DATA_ADDED, false).commit();
    }

    public static ArrayList<Datum> getArraylist(SharedPreferences spf) {
        Gson gson = new Gson();
        String jsonText = spf.getString(AppConstants.SPF_PARAMETERS_DATA, null);
        Type type = new TypeToken<ArrayList<com.cl.lg.pojo.Parameters.Datum>>() {
        }.getType();
        ArrayList<Datum> objText = gson.fromJson(jsonText, type);  //EDIT: gso to gson
        return objText;
    }


    public static void SaveClientTypeList(SharedPreferences spf, List<com.cl.lg.pojo.ClientType.Datum> arrayList) {
        //Set the values
        Gson gson = new Gson();
        ArrayList<com.cl.lg.pojo.ClientType.Datum> textList = new ArrayList<>();
        textList.addAll(arrayList);
        String jsonText = gson.toJson(textList);
        spf.edit().putString(AppConstants.SPF_CLIENT_TYPE_DATA, jsonText).apply();
        if (arrayList.size() > 0)
            spf.edit().putBoolean(AppConstants.SPF_CLIENT_TYPE_DATA_ADDED, true).commit();
        else
            spf.edit().putBoolean(AppConstants.SPF_CLIENT_TYPE_DATA_ADDED, false).commit();
    }

    public static ArrayList<com.cl.lg.pojo.ClientType.Datum> getClientTypeList(SharedPreferences spf) {
        Gson gson = new Gson();
        String jsonText = spf.getString(AppConstants.SPF_CLIENT_TYPE_DATA, null);
        Type type = new TypeToken<ArrayList<com.cl.lg.pojo.ClientType.Datum>>() {
        }.getType();
        ArrayList<com.cl.lg.pojo.ClientType.Datum> objText = gson.fromJson(jsonText, type);  //EDIT: gso to gson
        return objText;
    }

    public static void SaveLampTypeInSPF(SharedPreferences spf, ArrayList<LampType> arrayList) {
        //Set the values
        Gson gson = new Gson();
        ArrayList<LampType> textList = new ArrayList<LampType>();
        textList.addAll(arrayList);
        String jsonText = gson.toJson(textList);
        spf.edit().putString(AppConstants.SPF_LAMP_TYPE_DATA, jsonText).apply();
        if (arrayList.size() > 0)
            spf.edit().putBoolean(AppConstants.SPF_LAMP_TYPE_DATA_ADDED, true).commit();
        else
            spf.edit().putBoolean(AppConstants.SPF_LAMP_TYPE_DATA_ADDED, false).commit();
    }

    public static ArrayList<LampType> getLampType(SharedPreferences spf) {
        Gson gson = new Gson();
        String jsonText = spf.getString(AppConstants.SPF_LAMP_TYPE_DATA, null);
        Type type = new TypeToken<ArrayList<LampType>>() {
        }.getType();
        ArrayList<LampType> objText = gson.fromJson(jsonText, type);  //EDIT: gso to gson
        return objText;
    }


    public String getSortedLatLngString(SharedPreferences spf, ArrayList<LatLng> arylist) {
        StringBuilder sb = new StringBuilder();
        try {
            Double lat, lng;
            lat = Double.valueOf(spf.getString(AppConstants.LATTITUDE, ""));
            lng = Double.valueOf(spf.getString(AppConstants.LONGITUDE, ""));

            Location currLocation = new Location("Current");
            currLocation.setLatitude(lat);
            currLocation.setLongitude(lng);

            double distance;

            Set<LatLong> hash_Set = new TreeSet<LatLong>();

            for (int i = 0; i < arylist.size(); i++) {
                Location location = new Location("Point" + i);
                location.setLongitude(arylist.get(i).longitude);
                location.setLatitude(arylist.get(i).latitude);
                distance = currLocation.distanceTo(location);

                LatLong objLatLng = new LatLong(arylist.get(i).latitude, arylist.get(i).longitude, distance);
                hash_Set.add(objLatLng);
            }

            for (LatLong lg : hash_Set) {
                if (hash_Set.size() == 1)
                    sb.append(lg.getLat() + "," + lg.getLng());
                else
                    sb.append("+to:" + lg.getLat() + "," + lg.getLng());

                Log.i(TAG, "Distance: " + lg.getDistance() + " | Lat: " + lg.getLng() + " Lng: " + lg.getLng());
            }
        } catch (Exception e) {

        }
        return sb.toString();
    }

    public interface ClickLampType {
        void setOnClickLampType(LampType objOnClickLampType);
    }

    public interface ClickNodeType {
        void setOnClickNodeType(com.cl.lg.pojo.ClientType.Datum objOnClickLampType);
    }

    ClickLampType objClickLampType;
    ClickNodeType objClickNodeType;

    public void ShowLampTypeDialog(final List<LampType> objList, String title, Activity activity, SharedPreferences spf, ClickLampType objLampType) {
        String event_name = spf.getString(AppConstants.GLOBAL_EVENT_NAME, "") + "LampType";
        Log.d(AppConstants.TAG, "Event:" + event_name);

        FirebaseAnalytics mFirebaseAnalytics = FirebaseAnalytics.getInstance(activity);
        Bundle bundle = new Bundle();
        bundle.putString(FirebaseAnalytics.Param.ITEM_NAME, event_name);
        mFirebaseAnalytics.logEvent(FirebaseAnalytics.Event.SELECT_CONTENT, bundle);

        objClickLampType = objLampType;
        AlertDialog dialogSelection = null;
        AlertDialog.Builder builder = new AlertDialog.Builder(activity);
        LayoutInflater inflater = activity.getLayoutInflater();
        View view = inflater.inflate(R.layout.dialog_title, null);

        TextView tvTitle1 = view.findViewById(R.id.tvDialogTitle1);
        tvTitle1.setText(title);
        builder.setCustomTitle(view);
        //builder.setTitle(title);

        //getting fresh shared preference values ...!
        int selectedIndex = spf.getInt(AppConstants.SELECTED_LAMP_TYPE_INDEX, 0);

        final String[] ary = new String[objList.size()];

        for (int i = 0; i < objList.size(); i++) {
            ary[i] = objList.get(i).getLampType();
        }

        builder.setSingleChoiceItems(ary, selectedIndex, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(final DialogInterface dialog1, int which) {
                objList.get(which).setChecked(true);

                LampType objMapTypePojo = objList.get(which);
                spf.edit().putBoolean(AppConstants.IS_LAMP_TYPE_SELECTED, true).commit();
                spf.edit().putString(AppConstants.SELECTED_LAMP_TYPE, objMapTypePojo.getLampType().toString()).commit();
                spf.edit().putString(AppConstants.SELECTED_LAMP_TYPE_ID, objMapTypePojo.getLampTypeID().toString()).commit();
                spf.edit().putInt(AppConstants.SELECTED_LAMP_TYPE_INDEX, which).commit();

                // new Utils().setMapType(getActivity(), googleMapGlobal, spf);
                Handler handler = new Handler();
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        //dialog.dismiss();
                        dialog1.dismiss();
                        objClickLampType.setOnClickLampType(objMapTypePojo);
                    }
                }, 500);
            }
        });

        builder.setNegativeButton(activity.getResources().getString(R.string.cancel), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });

        // create and show the alert dialog
        dialogSelection = builder.create();
        dialogSelection.show();
    }

    public void ShowNodeTypeDialog(final List<com.cl.lg.pojo.ClientType.Datum> objList, String title, Activity activity, SharedPreferences spf, ClickNodeType objNodeType) {
        String event_name = spf.getString(AppConstants.GLOBAL_EVENT_NAME, "") + "NodeType";
        Log.d(AppConstants.TAG, "Event:" + event_name);

        FirebaseAnalytics mFirebaseAnalytics = FirebaseAnalytics.getInstance(activity);
        Bundle bundle = new Bundle();
        bundle.putString(FirebaseAnalytics.Param.ITEM_NAME, event_name);
        mFirebaseAnalytics.logEvent(FirebaseAnalytics.Event.SELECT_CONTENT, bundle);

        objClickNodeType = objNodeType;
        AlertDialog dialogSelection = null;
        AlertDialog.Builder builder = new AlertDialog.Builder(activity);
        LayoutInflater inflater = activity.getLayoutInflater();
        View view = inflater.inflate(R.layout.dialog_title, null);

        TextView tvTitle1 = view.findViewById(R.id.tvDialogTitle1);
        tvTitle1.setText(title);
        builder.setCustomTitle(view);
        //builder.setTitle(title);

        //getting fresh shared preference values ...!
        int selectedIndex = spf.getInt(AppConstants.SELECTED_NODE_TYPE_INDEX, 0);

        final String[] ary = new String[objList.size()];

        for (int i = 0; i < objList.size(); i++) {
            ary[i] = objList.get(i).getClientType();
        }

        builder.setSingleChoiceItems(ary, selectedIndex, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(final DialogInterface dialog1, int which) {
                objList.get(which).setChecked(true);

                com.cl.lg.pojo.ClientType.Datum objMapTypePojo = objList.get(which);
                spf.edit().putBoolean(AppConstants.IS_NODE_TYPE_SELECTED, true).commit();
                spf.edit().putString(AppConstants.SELECTED_NODE_TYPE, objMapTypePojo.getClientType().toString()).commit();
                spf.edit().putString(AppConstants.SELECTED_NODE_TYPE_ID, objMapTypePojo.getValue().toString()).commit();
                spf.edit().putInt(AppConstants.SELECTED_NODE_TYPE_INDEX, which).commit();

                // new Utils().setMapType(getActivity(), googleMapGlobal, spf);
                Handler handler = new Handler();
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        //dialog.dismiss();
                        dialog1.dismiss();
                        objClickNodeType.setOnClickNodeType(objMapTypePojo);
                    }
                }, 500);
            }
        });

        builder.setNegativeButton(activity.getResources().getString(R.string.cancel), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });

        // create and show the alert dialog
        dialogSelection = builder.create();
        dialogSelection.show();
    }

    public String getCommaStringValue(String val) {

        String newString = "";
        try {
            for (int i = 0; i < val.length(); i++) {
                if ((val.length() - i - 1) % 3 == 0) {
                    newString += Character.toString(val.charAt(i)) + ",";
                } else {
                    newString += Character.toString(val.charAt(i));
                }
            }
        } catch (Exception e) {
            Log.e(TAG, e.getMessage());
        }
        return newString;
    }

    public static String bigDecimalData(String data) {
        if (!TextUtils.isEmpty(data)) {
            BigDecimal bd = new BigDecimal(Double.parseDouble(data));
            DecimalFormat df = new DecimalFormat("###,###,###");
            return df.format(bd);
        }
        return "";
    }

    public void sendEmailGmailApp(Activity activity, String subject, String body) {
        try {

            Log.i("Send email", "");

            String[] TO = {"Mobile.support@cimconlighting.com"};
            String[] CC = {""};

            Intent emailIntent = new Intent(Intent.ACTION_SEND);

            emailIntent.putExtra(Intent.EXTRA_EMAIL, TO);
            emailIntent.putExtra(Intent.EXTRA_CC, CC);
            emailIntent.putExtra(Intent.EXTRA_SUBJECT, subject);
            emailIntent.putExtra(Intent.EXTRA_TEXT, body);
            emailIntent.setType("text/html");
            emailIntent.setPackage("com.google.android.gm");

            activity.startActivity(Intent.createChooser(emailIntent, activity.getString(R.string.send_email)));


        } catch (android.content.ActivityNotFoundException ex) {
            Toast.makeText(activity, activity.getResources().getString(R.string.there_is_no_email_client_installed), Toast.LENGTH_SHORT).show();
        }
    }

    public void dialogForApptypeDefault(Activity activity, String message) {
        if (activity != null) {
            androidx.appcompat.app.AlertDialog.Builder builder = new androidx.appcompat.app.AlertDialog.Builder(activity);
            builder.setMessage(message)
                    .setCancelable(true)
                    .setNegativeButton(activity.getResources().getText(R.string.cancel), new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            dialogInterface.dismiss();
                        }
                    }).setPositiveButton(activity.getResources().getText(R.string.upgrade), new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    sendEmailGmailApp(activity, activity.getResources().getString(R.string.subjectUpgrade), "");
                }
            });
            //Creating dialog box
            androidx.appcompat.app.AlertDialog alert = builder.create();
            //Setting the title manually
            alert.requestWindowFeature(Window.FEATURE_NO_TITLE);
            alert.show();
        }
    }

    public void dialogForApptype(Activity activity, String msg) {

        Dialog dialogComands = new Dialog(activity);
        dialogComands.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialogComands.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        dialogComands.setContentView(R.layout.custom_dialog_upgrade);
        dialogComands.setCancelable(true);

        TextView tvUpgrade = dialogComands.findViewById(R.id.tvUpgrade);
        tvUpgrade.setText(msg);

        dialogComands.findViewById(R.id.ivCancel).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialogComands.dismiss();
            }
        });

        SharedPreferences spf = activity.getSharedPreferences(AppConstants.SPF, Context.MODE_PRIVATE);
        String event_name = spf.getString(AppConstants.GLOBAL_EVENT_NAME, "") + "upgradeVC";
        Log.d(AppConstants.TAG, "Event:" + event_name);

        FirebaseAnalytics mFirebaseAnalytics = FirebaseAnalytics.getInstance(activity);
        Bundle bundle = new Bundle();
        bundle.putString(FirebaseAnalytics.Param.ITEM_NAME, event_name);
        mFirebaseAnalytics.logEvent(FirebaseAnalytics.Event.SELECT_CONTENT, bundle);

        FrameLayout frmFrameLayout = dialogComands.findViewById(R.id.frmContactMe);

        frmFrameLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //dialogComands.dismiss();
                //sendEmailGmailApp(activity, activity.getResources().getString(R.string.subjectUpgrade), "");
                if (isInternetAvailable(activity))
                    callUpgradeApiCall(activity, dialogComands);
                else
                    Utils.dialogForMessage(activity, activity.getResources().getString(R.string.no_internet_connection));
            }
        });

        Rect displayRectangle = new Rect();
        Window window = activity.getWindow();

        window.getDecorView().getWindowVisibleDisplayFrame(displayRectangle);
        dialogComands.getWindow().setLayout((int) (displayRectangle.width() * 0.9f), (int) (displayRectangle.height() * 0.7f));
        dialogComands.show();
    }

    void callUpgradeApiCall(Activity activity, Dialog dialog) {
        ProgressDialog progressDialog = new ProgressDialog(activity);
        progressDialog.setMessage(activity.getResources().getString(R.string.loading));
        progressDialog.setCancelable(false);
        progressDialog.show();

        API objApi = new LG().networkCall(activity, true);
        SharedPreferences spf = activity.getSharedPreferences(AppConstants.SPF, Context.MODE_PRIVATE);
        objApi.getUpgradeConfirmation(
                spf.getString(AppConstants.USERNAME, ""),
                spf.getString(AppConstants.NAME, ""),
                spf.getString(AppConstants.CLIENT_ID, ""), AppConstants.IS_SECURED).enqueue(new Callback<UpgradeResponse>() {
            @Override
            public void onResponse(Call<UpgradeResponse> call, Response<UpgradeResponse> response) {
                if (response.code() == 200) {
                    UpgradeResponse objUpgradeResponse = response.body();
                    if (objUpgradeResponse.getStatus().equals("1")) {
                        dismissProgressDialog(dialog);
                        Utils.dialogForMessage(activity, getStringResourceByName(activity, objUpgradeResponse.getMsg().toString()));


                        SharedPreferences spf = activity.getSharedPreferences(AppConstants.SPF, Context.MODE_PRIVATE);
                        String event_name = spf.getString(AppConstants.GLOBAL_EVENT_NAME, "") + "contactMe";
                        Log.d(AppConstants.TAG, "Event:" + event_name);

                        FirebaseAnalytics mFirebaseAnalytics = FirebaseAnalytics.getInstance(activity);
                        Bundle bundle = new Bundle();
                        bundle.putString(FirebaseAnalytics.Param.ITEM_NAME, event_name);
                        mFirebaseAnalytics.logEvent(FirebaseAnalytics.Event.SELECT_CONTENT, bundle);

                    } else
                        Utils.dialogForMessage(activity, getStringResourceByName(activity, objUpgradeResponse.getMsg().toString()));
                } else
                    responseHandle(activity, response.code(), response.errorBody());

                dismissProgressDialog(progressDialog);

            }

            @Override
            public void onFailure(Call<UpgradeResponse> call, Throwable t) {
                dismissProgressDialog(progressDialog);
                Utils.dialogForMessage(activity, activity.getResources().getString(R.string.network_error));
            }
        });
    }

    public boolean isGoogleMapsInstalled(Activity activity) {
        try {
            ApplicationInfo info = activity.getPackageManager().getApplicationInfo("com.google.android.apps.maps", 0);
            return info.enabled;
        } catch (PackageManager.NameNotFoundException e) {
            return false;
        }
    }

    public String getStringResourceByName(Activity activity, String aString) {
        String result = "No Text";
        try {

            //Comapre in default locle en-us
            Locale tempLocale = new Locale(AppConstants.LANGUAGE_CODE_ENGLISH);
            Configuration conf = activity.getResources().getConfiguration();
            conf = new Configuration(conf);
            conf.setLocale(tempLocale);
            Context localizedContext = activity.createConfigurationContext(conf);
            String[] namesActual = localizedContext.getResources().getStringArray(R.array.backedMsg);

            int index = -1;
            for (int i = 0; i < namesActual.length; i++) {

                if (namesActual[i].toString().equalsIgnoreCase(aString)) {
                    // found a match
                    Log.i("TEST", i + " : " + namesActual[i]);
                    index = i;
                    break;
                }
            }

            //find value in respective locale.
            SharedPreferences spf = activity.getSharedPreferences(AppConstants.SPF, Context.MODE_PRIVATE);
            String localeDefault = Utils.getDeviceLocale(spf);
            String langCode = spf.getString(AppConstants.LANGUAGE_LOCALE, localeDefault.toString());
            Locale locale = new Locale(langCode);
            Configuration conf1 = activity.getResources().getConfiguration();
            conf1 = new Configuration(conf1);
            conf1.setLocale(locale);
            Context localizedContext1 = activity.createConfigurationContext(conf1);
            String[] names = localizedContext1.getResources().getStringArray(R.array.backedMsg);

            if (index != -1)
                result = names[index];
            else
                result = aString;

            return result;
        } catch (Exception e) {
            return aString;
        }

    }

    public String getStringResourceByName4(Activity context, String aString) {

        String result;
        int resId = 0;
        try {


               /* Class res = R.string.class;
                Field field = res.getDeclaredField(aString.toString());
                resId = field.getInt(null);*/

            //Field[] fields = context.getClass().getDeclaredFields();

            // for(int i=0;i<fields.length;i++){

            // //     Log.i(AppConstants.TAG,"Fields["+i+"]"+fields[i].getName());
            // }

            resId = context.getResources().getIdentifier(aString, "string", context.getPackageName()); //key required
            //resId = R.string.class.getField(aString).getInt(null);
            Log.i(AppConstants.TAG, "resID:" + resId);
            if (resId == 0)
                return aString;
            else
                return context.getString(resId);

        } catch (Exception e) {
            return aString;
        }
    }


    public String getStringResourceByName2(Activity context, String aString) {
        //https://stackoverflow.com/questions/7493287/android-how-do-i-get-string-from-resources-using-its-name
        try {
            Locale tempLocale = new Locale(AppConstants.LANGUAGE_CODE_ENGLISH);
            Configuration conf = context.getResources().getConfiguration();
            conf = new Configuration(conf);
            conf.setLocale(tempLocale);
            Context localizedContext = context.createConfigurationContext(conf);

            String result;


            Locale locale = new Locale(AppConstants.LANGUAGE_CODE_SPANISH);
            Configuration conf1 = context.getResources().getConfiguration();
            conf1 = new Configuration(conf1);
            conf1.setLocale(locale);
            Context localizedContext1 = context.createConfigurationContext(conf1);
            int resId = localizedContext1.getResources().getIdentifier(aString, "string", context.getPackageName());
            result = localizedContext1.getResources().getString(resId);
            if (resId == 0)
                return aString;
            else
                return result;
        } catch (Exception e) {
            Log.i(TAG, e.getMessage().toString());
            return aString;
        }
    }

    public static void hideKeyboard2(Activity activity) {
        try {
            InputMethodManager imm = (InputMethodManager) activity.getSystemService(Activity.INPUT_METHOD_SERVICE);
            //Find the currently focused view, so we can grab the correct window token from it.
            View view = activity.getCurrentFocus();
            //If no view currently has focus, create a new one, just so we can grab a window token from it
            if (view == null) {
                view = new View(activity);
            }
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        } catch (Exception e) {
        }
    }

    public static void hideKeyboard(Activity activity) {
        try {
            InputMethodManager imm = (InputMethodManager) activity.getSystemService(Activity.INPUT_METHOD_SERVICE);
            imm.toggleSoftInput(InputMethodManager.HIDE_IMPLICIT_ONLY, 0);

        } catch (Exception e) {
        }
    }

    public ArrayList<Command> getCommands(Activity activity) {
        ArrayList<Command> list = new ArrayList<>();

        Resources objResources = activity.getResources();
        list.add(new Command(activity.getResources().getString(R.string.light_on), activity.getResources().getDrawable(R.drawable.custom_rect_green), activity.getResources().getDrawable(R.drawable.lamp_on_16_12)));
        list.add(new Command(activity.getResources().getString(R.string.light_off), activity.getResources().getDrawable(R.drawable.custom_rect_gray), activity.getResources().getDrawable(R.drawable.lamp_off16_12)));

        list.add(new Command(activity.getResources().getString(R.string.light_dim), activity.getResources().getDrawable(R.drawable.custom_rect_orange), activity.getResources().getDrawable(R.drawable.lamp_dim_16_12)));
        list.add(new Command(activity.getResources().getString(R.string.read_data), activity.getResources().getDrawable(R.drawable.custom_rect_red), activity.getResources().getDrawable(R.drawable.read_data_4_12)));
        list.add(new Command(activity.getResources().getString(R.string.reset_trip), activity.getResources().getDrawable(R.drawable.custom_rect_blue), activity.getResources().getDrawable(R.drawable.reset_trip_4_12)));

        list.add(new Command(activity.getResources().getString(R.string.route), activity.getResources().getDrawable(R.drawable.custom_rect_cyan), activity.getResources().getDrawable(R.drawable.route_plan)));

        list.add(new Command(activity.getResources().getString(R.string.get_mode), activity.getResources().getDrawable(R.drawable.custom_rect_blue_dark), activity.getResources().getDrawable(R.drawable.get_mode_4_12)));
        list.add(new Command(activity.getResources().getString(R.string.set_mode), activity.getResources().getDrawable(R.drawable.custom_rect_blue_dark2), activity.getResources().getDrawable(R.drawable.set_mode_4_12)));
        return list;
    }

    public void dialog_info(Activity activity) {

        final Dialog dialog = new Dialog(activity);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_info);
        dialog.setCancelable(false);

        TextView btn_cancel_map = dialog.findViewById(R.id.btn_cancel_map);
        btn_cancel_map.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });


        // dialogButton.setOnTouchListener(Util.colorFilter());
        Rect displayRectangle = new Rect();
        Window window = activity.getWindow();

        window.getDecorView().getWindowVisibleDisplayFrame(displayRectangle);
        dialog.getWindow().setLayout((int) (displayRectangle.width() * 0.9f), (int) (displayRectangle.height() * 0.9f));
        dialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);

        dialog.show();
    }


    public void dialog_info_grid(Activity activity, boolean isZigbeeContains) {

        final Dialog dialog = new Dialog(activity);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_info_with_grid_layout);
        dialog.setCancelable(false);

        ArrayList<GridItem> mList = new ArrayList<>();

        Resources objResource = activity.getResources();

        mList.add(new GridItem(objResource.getDrawable(R.drawable.ok_1), objResource.getString(R.string.ok)));
        mList.add(new GridItem(objResource.getDrawable(R.drawable.astro_clock_with_override_4_12), objResource.getString(R.string.astro_clock_with_override)));
        mList.add(new GridItem(objResource.getDrawable(R.drawable.service_request_dashboad), objResource.getString(R.string.service)));
        mList.add(new GridItem(objResource.getDrawable(R.drawable.astro_clock_4_12), objResource.getString(R.string.astro_clock)));
        mList.add(new GridItem(objResource.getDrawable(R.drawable.photocell_4_12), objResource.getString(R.string.photocell)));
        mList.add(new GridItem(objResource.getDrawable(R.drawable.mix_mode_4_12), objResource.getString(R.string.mix_mode)));
        mList.add(new GridItem(objResource.getDrawable(R.drawable.shcedule_4_12), objResource.getString(R.string.schedule)));
        mList.add(new GridItem(objResource.getDrawable(R.drawable.manual_14_12), objResource.getString(R.string.manual)));
        mList.add(new GridItem(objResource.getDrawable(R.drawable.driver_fault_4_12), objResource.getString(R.string.driver_fault_p)));

        if (isZigbeeContains)
            mList.add(new GridItem(objResource.getDrawable(R.drawable.g_active_on_4_12), objResource.getString(R.string.gatway_on)));
        mList.add(new GridItem(objResource.getDrawable(R.drawable.lamp_on_4_12), objResource.getString(R.string.lamp_on)));
        if (isZigbeeContains)
            mList.add(new GridItem(objResource.getDrawable(R.drawable.gateway_off), objResource.getString(R.string.gateway_off)));
        mList.add(new GridItem(objResource.getDrawable(R.drawable.lamp_dim_4_12), objResource.getString(R.string.lamp_dim)));
        if (isZigbeeContains)
            mList.add(new GridItem(objResource.getDrawable(R.drawable.gateway_powerloss), objResource.getString(R.string.gatway_power_loss)));

        mList.add(new GridItem(objResource.getDrawable(R.drawable.lamp_off_4_12), objResource.getString(R.string.lamp_off)));
        mList.add(new GridItem(objResource.getDrawable(R.drawable.lamp_type_new), objResource.getString(R.string.lamp_type_sentence_case)));
        mList.add(new GridItem(objResource.getDrawable(R.drawable.never_comm_4_12), objResource.getString(R.string.never_comm)));
        mList.add(new GridItem(objResource.getDrawable(R.drawable.comm_fault_4_12), objResource.getString(R.string.comm_fault_pural)));
        mList.add(new GridItem(objResource.getDrawable(R.drawable.slc_yes), objResource.getString(R.string.yes)));

        RecyclerView rvGrid = dialog.findViewById(R.id.rvInfoGrid);
        rvGrid.setLayoutManager(new GridLayoutManager(activity, 2));
        rvGrid.setAdapter(new InfoGridAdapter(mList,activity));


        TextView btn_cancel_map = dialog.findViewById(R.id.btn_cancel_map);
        btn_cancel_map.setOnClickListener(view -> dialog.dismiss());


        // dialogButton.setOnTouchListener(Util.colorFilter());
        Rect displayRectangle = new Rect();
        Window window = activity.getWindow();

        window.getDecorView().getWindowVisibleDisplayFrame(displayRectangle);
        dialog.getWindow().setLayout((int) (displayRectangle.width() * 0.9f), (int) (displayRectangle.height() * 0.9f));
        dialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);

        dialog.show();
    }

    public static void dialogForMessagePlayStoreNavigate(Activity activity, String message) {
        try {
            if (activity != null) {
                String[] ary = message.split("\\|");
                AlertDialog.Builder builder = new AlertDialog.Builder(activity);
                builder.setMessage(ary[0])
                        .setTitle(R.string.app_name)
                        .setCancelable(false)
                        .setPositiveButton(activity.getResources().getString(R.string.ok), new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                if (ary[1] != null) {
                                    try {
                                        //https://play.google.com/store/apps/details?id=com.example.android
                                        activity.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(ary[1].trim())));
                                    } catch (Exception e) {
                                        dialogInterface.dismiss();
                                    }
                                } else
                                    dialogInterface.dismiss();
                            }
                        });
                //Creating dialog box
                AlertDialog alert = builder.create();
                //Setting the title manually
                alert.requestWindowFeature(Window.FEATURE_NO_TITLE);
                alert.show();
            }
        } catch (Exception e) {

        }
    }

    //clear stack of fragment
    public void clearStack(Activity activity) {
        FragmentManager fm = activity.getFragmentManager();
        int count = fm.getBackStackEntryCount();
        for (int i = 0; i < count; ++i) {
            fm.popBackStack();
            Log.i("***", "Fragment" + i);
        }
    }

}