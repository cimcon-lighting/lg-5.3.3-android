package com.cl.lg.utils;

public class AppConstants {

    //Live
    public static final String BASE_URL ="https://clapptest.cimconlms.com/";//
    //public static final String BASE_URL ="http://apps.cimconlighting.com/";

    public static final boolean isLogDisplay = true;

    //APIs
    public static final String API_SCODE = "LightingGale/api-bct/checkuniquecode";
    public static final String API_LOGIN = "General/API/get-token";
    public static final String API_GET_COMMANDS = "LightingGale/GetCommand";
    public static final String API_GET_STATUS = "LightingGale/GetCommandStatus";
    public static final String API_GET_GATEWAY = "api/v5/LightingGale/GetAllGatewayList";
    public static final String API_GET_TRACKID = "LightingGale/GetCommandTrackID";
    public static final String API_SENT_COMMAND = "LightingGale/SendCommandlist";
    public static final String API_SLC_STATUS_LIST = "api/v5/LightingGale/GetSlcStatusList";
    public static final String API_GET_MAP_DATA = "api/v5/LightingGale/GetMapsSLC";
    public static final String API_FORGOT_PASSWORD = "LightingGale/Forgotpassword";
    public static final String API_GET_DASHBOARD_COUNT = "api/v5/LightingGale/GetAllDashBoardCount";
    public static final String API_GET_MODE_CMD_LIST = "LightingGale/GetSetModeCommandList";
    public static final String API_GET_SLC_NAME = "api/v5/LightingGale/GetSLCSearch";
    public static final String API_GET_SLC_GP = "api/v5/LightingGale/GetSLCGroupSearch";
    public static final String API_SEND_CMD_DETAILS = "LightingGale/GetTrackDetailByTrackID";

    public static final String API_CMD_RESETTRIP = "LightingGale/ResetSLCTrip";
    public static final String API_CMD_SENDREADDATACOMMAND = "LightingGale/SendReadDataCommand";
    public static final String API_CMD_SWITCHONOFFSLC = "LightingGale/SwitchOnOffSLC";
    public static final String API_CMD_DIMSLC = "LightingGale/DIMSLCs";
    public static final String API_CMD_SETMODE =  "LightingGale/SetMode";
    public static final String API_CMD_GETMODE = "LightingGale/GetMode";

    //2.0
    public static final String API_GET_FAULTY_SLC_COUNT_="api/v5/LightingGale/GetFaultySLCCount";
    public static final String API_GET_FAULTY_SLC_DETAILS_="api/v5/LightingGale/GetFaultySLCDetails";
    public static final String API_GATWAY_COUNT="api/v5/LightingGale/GatewayCount";
    public static final String API_GATWAYLIST="api/v5/LightingGale/GetGatewayList";
    public static final String API_ASSIGNED_SLC="api/v5/LightingGale/AssignedSLC";
    public static final String API_DAYBURNER_OUTAGES="api/v5/LightingGale/ReportOutageDayBurner";
    public static final String API_STATUS_TOTAL="api/v5/LightingGale/SLCStatusTotal";
    public static final String API_HISTORICAL_DATA="api/v5/LightingGale/HistoryDataReport";
    public static final String API_PARAMETERS="api/v5/LightingGale/GetParameters";

    public static final String API_GET_SLC_UNMAPPED = "api/v5/LightingGale/GetSLCSearchUnMapped";
    public static final String API_UPGRADE="LightingGale/api-v2/UpgradeEmail";

    //ver 3.0
    public static final String API_GET_CLIENT_TYPE="LightingGale/GetClientType";
    public static final String API_GET_SAML_DETAIL="api/v5/GetSamlDetails";
    public static final String API_GET_SAML_TOKEN="api/v5/GetSAMLToken";


    public static final String ISLOGGEDIN = "isLoggedIn";
    public static final String isAppKilled = "isAppKilled";

    public static final String SPF = "SLCScannerWebSPF";
    public static final String SPF_LOGIN ="LG_LOGIN" ;

    public static final String DEVICE_ID = "device_id";
    public static final String TAG = "LG";

    public static final String LANGUAGE_CODE_ENGLISH = "en";
    public static final String LANGUAGE_CODE_SPANISH = "es";
    public static final String LANGUAGE_CODE_PORTUGUES = "pt";
    public static final String LANGUAGE_LOCALE = "language_locale";
    public static final String LANGUAGE_SELECTED = "language_selected";
    public static final String SELECTED_LANG_INDEX = "selected_lang_index";

    public static final String CLIENT_ID = "client_id";
    public static final String USER_ID = "user_id";

    public static final String SELECTED_LANG_TYPE = "selected_lang_type";
    public static final String SELECTED_LANG_TYPE_KEY = "selected_lang_type_key";
    public static final String USERNAME = "username";
    public static final String NAME = "name";
    public static final String EMAIL = "email";

    public static final String SELECTED_STATUS_INDEX = "selected_status_index";
    public static final String SELECTED_MAP_INDEX = "selected_map_index";
    public static final String SELECTED_AUTH_INDEX = "selected_auth_index";
    //public static final String OPEN_STREET_MAP_STANDARD_URL = "open_street_map_standard_url";
    public static final String OPEN_STREET_MAP_STANDARD_URL = "http://tile.openstreetmap.org/%d/%d/%d.png";
    public static final String ACCESS_TOKEN = "access_token";
    public static final String PHONE_NUMBER = "phone_number";
    public static final String TOKEN_TYPE = "token_type";

    public static final String page_size = "10";

    //status param
    public static final String COMMUNICATION_FAULT = "8&0";
    public static final String DRIVER_FAULT = "9&1,8&1";
    public static final String PHOTO_CELL_STATUS_FAULT = "2&1,8&1";
    public static final String LAMP_OFF = "1&0,8&1";

    //power + status param
    public static final String LAMP_ON = "6&=&0,|6&=&100";
    public static final String STATUS_PARA_LAMP_ON = "1&1,8&1";
    public static final String LAMP_DIM = "6&>&0,6&<&100";
    public static final String STATUS_PARA_LAMP_DIM = "1&1,8&1";
    public static final String LATTITUDE = "lattitude";
    public static final String LONGITUDE = "longitude";

    public static final String POWER_PARA = "power_para";
    public static final String STATUS_PARA = "status_para";
    public static final String NEVER_COMM = "never_comm";
    public static final String TITLE = "title";
    public static final String SERVICE_REQUEST = "service_request";
    public static final String Manual = "Manual ";
    public static final String MixedMode = "Mixed Mode";
    public static final String AstroClock = "Astro Clock";
    public static final String AstroClockWithOver = "Astro Clock With Photocell Override";
    public static final String Schedule = "Schedule";
    public static final String photocell = "Photocell";

    public static final String COMMUNICATION_FAULT_DETAILS = "Communication Fault";
    public static final String DRIVER_FAULT_DETAILS = "Driver Fault";
    public static final String VOLATAGE_UNDER_OVER = "Voltage Under Over";
    public static final String LAMP_FAULT = "Lamp Fault";
    public static final String DAY_BURNER = "Day Burner";
    public static final String OUTEAGES = "Lamp Outages";

    public static final String RESET_SLC_TRIP = "Reset SLC Trip";
    public static final String READ_DATA = "Read Data";
    public static final String SWITCH_ON = "Switch On";
    public static final String SWITCH_OFF = "Switch Off";
    public static final String SWITCH_DIM = "DIM";
    public static final String SET_MODE = "Set Mode";
    public static final String GET_MODE = "Get Mode";
    public static final String ROUTE = "Route";

    public static final String MAP_COMM_FAULT = "cfail";
    public static final String MAP_GATEWAY_DISCONNECTED = "goff";
    public static final String MAP_LAMPS_OFF = "off";
    public static final String MAP_LAMPS_ON = "on";
    public static final String MAP_LAMPS_DIMMED = "dim";
    public static final String MAP_GATEWAYS_CONNECTED = "gon";
    public static final String MAP_NOT_CONNECTED = "DNR";
    public static final String MAP_POWER_LOSS="gpowerloss";

    public static final String SELCTED_MAP_TYPE = "selcted_map_type";
    public static final String SELECTED_MAP_TYPE_KEY = "selected_map_type_key";

    public static final Integer ZIGBEE = 1;
    public static final Integer CISCO = 2;
    public static final Integer IngenuRPMA = 3;
    public static final Integer IngenuOSDI = 4;

    public static final String CLIENT_TYPE = "ctype";
    public static final String ISFROMSLCM = "isFromSLCMODE";
    public static final String IS_FROM_SLC_SERVICE_MODE = "isFromSLCServiceMODE";
    public static final String DASHBOARD_ARRAYLIST = "dashboard_arraylist";
    public static final String SLC_NO = "slc_no";
    public static final String isZigbeeContains="isZigbeeContains";

    public static final String JSON_STRING = "json_string";
    public static final String ISFROMSTATUS = "isfromstatus";
    public static final String UI_ID = "ui_id";
    public static final String UI_ID_status = "UI_ID_status";
    public static final String MINX = "minx";
    public static final String MINY = "miny";
    public static final String API_URL = "api_url";
    public static final String MESSAGE = "message";
    public static final String DATE_FORMAT = "date_format";

    public static final String IS_SEARCH_ON = "is_search_on";
    public static final String SLC_ID_NAV ="slc_id_nav" ;
    public static final String SLC_ID_VALUE_NAVE = "slc_id_value_nave";
    public static final String GP_ID_NAV = "gp_id_nav";
    public static final String GP_ID_VALUE_NAVE ="gp_id_value_nave" ;
    public static final String G_ID_NAV = "g_id_nav";
    public static final String G_ID_VALUE_NAVE ="g_id_value_nave " ;
    public static final String DISTANCE_NAV = "distance_nav";
    public static final String IS_NEAR_ME = "is_near_me";

    public static final String IS_SEARCH_ON_SENT_CMD = "is_search_on_sent_cmd";

    public static final String SENT_C_STATUS_VALUE = "sent_c_status_value";
    public static final String SENT_C_GATWAY_VALUE = "sent_c_gatway_value";
    public static final String SENT_C_TRACK_ID_VALUE ="sent_c_track_id_value" ;
    public static final String SENT_C_TO_DATE_VALE = "sent_c_to_date_vale";
    public static final String SENT_C_FROM_DATE = "sent_c_from_date";
    public static final String SENT_C_CMD_NAME_VALUE = "sent_c_cmd_name_value";
    public static final String SENT_C_CMD_NAME_ID = "sent_c_cmd_name_id";
    public static final String SENT_C_STATUS_ID ="sent_c_status_id";
    public static final String IS_REMMEBER = "is_remmeber";
    public static final String SECURITY_CODE ="SCODE" ;
    public static final String PASSWORD = "password";

    public static final String GRANT_TYPE_VALUE="password";
    public static final String GRANT_TYPE ="grant_type" ;
    public static final String ALLOW_FACE_TOUCH_ID = "allow_face_touch_id";
    public static final String IS_FACE_TOUCH_PRESSED = "is_face_touch_pressed";
    public static final String TEMP_PASS = "temp_pass";
    public static final String GATWAY_TYPE ="gatway_type" ;
    public static final String G_CONNECTED = "connected";
    public static final String G_DISCONNECTED = "disconnected";
    public static final String G_POWERLOSS = "powerloss";
    public static final String SLC_GROUP_ID = "slc_group_id";
    public static final String GATWAY_ID = "gatway_id";
    public static final String APN = "apn";
    public static final String MOBILE_NUMBER = "mobile_number";
    public static final String IP ="ip" ;
    public static final String PORT = "port";
    public static final String ExtendedPan="ExtendedPan";
    public static final String SHORT_PAN_ID = "short_pan_id";
    public static final String CHANNEL ="channel_no" ;
    public static final String SLC_SERVICE_PARAMS ="slc_service_params" ;

    public static final String BACK_STACK_ROOT_TAG = "root_fragment";
    public static final String DASHOBARD_TYPE = "dashobard_type";

    public static final String LIGHT_STATUS = "light_status";
    public static final String MODE ="mode" ;
    public static final String SLC_COMM ="SLC_COMM" ;
    public static final String ON = "On";
    public static final String OFF = "Off";
    public static final String DIM ="Dim" ;
    public static final String ISFROMSLCLIGHTSTATUS = "isfromslclightstatus";
    public static final String HISTORY = "historical_data";
    public static final String IS_FROM_HISTORY_DAY_BURNER ="is_from_history_day_burner" ;
    public static final String COUNT = "count" ;
    public static final String SOURTED_ROUTE ="shorted_route";
    public static final String SIZE_ROUTE = "size";
    public static final String ISFROMMAP ="isfrommap" ;
    public static final String SPF_PARAMETERS_DATA ="spf_parameters_data";
    public static final String SPF_LAMP_TYPE_DATA ="spf_lamp_type_data";
    public static final String SPF_CLIENT_TYPE_DATA ="spf_client_type_data";
    public static final String IS_FROM_SERVICE ="isFromService";

    public static final int PARAMS_LAMP_FAULT=0;
    public static final int PARAMS_DRIVER_FAULT=1;
    public static final int PARAMS_COMM_FAULT=2;
    public static final int PARAMS_VOLTAGE=3;
    public static final int PARAMS_DAY_BURNER=4;
    public static final int PARAMS_OUTAGES=5;

    public static final String SPF_PARAMETERS_DATA_ADDED = "spf_parameters_data_added";
    public static final String SPF_CLIENT_TYPE_DATA_ADDED = "spf_parameters_data_added";

    public static final String SELECTED_LAMP_TYPE ="selected_lamp_type";
    public static final String SELECTED_LAMP_TYPE_ID ="selected_lamp_type_id";
    public static final String SELECTED_LAMP_TYPE_INDEX = "selected_lamp_type_index";
    public static final String IS_LAMP_TYPE_SELECTED = "is_lamp_type_selected";
    public static final String SPF_LAMP_TYPE_DATA_ADDED = "spf_lamp_type_data_added";


    public static final String IS_NODE_TYPE_SELECTED = "is_node_type_selected";
    public static final String SELECTED_NODE_TYPE = "node_type";
    public static final String SELECTED_NODE_TYPE_ID ="selected_node_type_id";
    public static final String SELECTED_NODE_TYPE_INDEX = "selected_node_type_index";

    public static final String IS_COMM_FUALT = "is_comm_fualt";
    public static final String totalOk="totalOk";
    public static final String APP_TYPE = "app_type";

    public static final String APP_TYPE_MSG ="app_type_msg" ;

    public static final String APP_TYPE_PAID="paid";
    public static final String APP_TYPE_LITE="lite";

    public static final String IS_FROM_DASHBOARD ="isFromDashboard";
    public static final String IS_FROM_MAP ="is_from_map" ;
    public static final String IS_SEARCH_ON_DETAIL = "is_search_on_detail";
    public static final String MESSAGE_NEAR_ME = "message_near_me";
    public static final String MILES = "miles";
    public static final String MODE_TEXT = "mode_text";
    public static final String IS_SECURED="1";
    public static final String GATEWAY_ID_NAV = "gateway_id_nav";
    public static final String GATEWAY_ID_VALUE_NAME ="gateway_id_value_nave" ;
    public static final String CLIENT_NAME="client_name";
    public static final String GLOBAL_EVENT_NAME ="global_event_name" ;

    public static final String isSingleNodeType="issinglenodetype";
    public static final String IS_SINGLE_ZIGBEE = "IS_SINGLE_ZIGBEE";
    public static final String IS_SEARCH_PRESSED_STATUS = "IS_SEARCH_PRESSED_STATUS";


    public static final String YES ="yes" ;
    public static final String NO ="no" ;
    public static final String NEVER ="never" ;

    public static final String SOURCE="Android";
    public static final String ORIGINS="Mobile";

    public static final String FB_EVENT_DIALOG_INFO="info_dialog";

    public static final String CLIENT_NAME_LG="client_name_lg";

}