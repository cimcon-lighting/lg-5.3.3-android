package com.cl.lg.adapters;

import android.content.Context;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import android.content.SharedPreferences;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.cl.lg.R;
import com.cl.lg.pojo.SentCmdDetails.TrackDetail;
import com.cl.lg.utils.AppConstants;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class StatusListDetailsAdapter extends RecyclerView.Adapter<StatusListDetailsAdapter.MyViewHolder> {

    List<TrackDetail> mList;
    Context mContext;
    SharedPreferences spf;
    String format;

    public StatusListDetailsAdapter(Context mContext, ArrayList<TrackDetail> mList,String format) {
        this.mList = mList;
        this.mContext = mContext;
        spf = mContext.getSharedPreferences(AppConstants.TAG, Context.MODE_PRIVATE);
        this.format=format;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.raw_details_sent_command, parent, false);//
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {

        final TrackDetail objBean = mList.get(position);
        holder.tvSLC.setText(objBean.getSlc());
        holder.tvSLCStaus.setText(objBean.getStatus());

        if (objBean.getSlcResponseDateTime().equalsIgnoreCase("")) {
            holder.tvResponseTime.setText("N/A");
        } else {
            holder.tvResponseTime.setText(objBean.getSlcResponseDateTime());
        }

        if (objBean.getSlcAckDateTime().equalsIgnoreCase(""))
            holder.tvACKTime.setText("N/A");
        else
            holder.tvACKTime.setText(objBean.getSlcAckDateTime());

        if (position % 2 == 0) {
            holder.llTitles.setBackgroundColor(mContext.getResources().getColor(R.color.colorWhite));
        } else {
            holder.llTitles.setBackgroundColor(mContext.getResources().getColor(R.color.colorSkyBlueList));
        }

    }


    String getFormatedDateTime(String input, String format) {

        //Format of the date defined in the input String
        DateFormat df = new SimpleDateFormat(format + " hh:mm:ss aa");
        //Desired format: 24 hour format: Change the pattern as per the need
        DateFormat outputformat = new SimpleDateFormat(format + " HH:mm:ss");
        Date date = null;
        String output = "";
        try {
            //Converting the input String to Date
            date = df.parse(input);
            //Changing the format of date and storing it in String
            output = outputformat.format(date);
            //Displaying the date
            System.out.println(output);
        } catch (ParseException pe) {
            pe.printStackTrace();
        }
        return output;
    }

    @Override
    public int getItemCount() {
        return mList.size();
    }

    class MyViewHolder extends RecyclerView.ViewHolder {

        //@BindView(R.id.tvAssetValueDisplay)
        //TextView tvAssetValueDisplay;

        @BindView(R.id.tvSLC)
        TextView tvSLC;

        @BindView(R.id.tvSLCStaus)
        TextView tvSLCStaus;

        @BindView(R.id.tvACKTime)
        TextView tvACKTime;

        @BindView(R.id.tvResponseTime)
        TextView tvResponseTime;

        @BindView(R.id.llTitles)
        LinearLayout llTitles;

        public MyViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}