package com.cl.lg.adapters;

import android.content.Context;
import android.os.Build;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.cl.lg.R;
import com.cl.lg.pojo.CommonResponseStatusDetailsDialog;
import com.cl.lg.pojo.StatusResponse2;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Iterator;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class StatusListAdapterDynamic extends RecyclerView.Adapter<StatusListAdapterDynamic.MyViewHolderList> {

    Context mContext;
    List<StatusResponse2> mList;

    private MyCallbackForControl objMyCallbackForControl;
    boolean isInstaller = false;
    boolean isFromDetails = false;
    boolean isFromNever = false;
    int id_ll = 0;

    private int selectedPosition = -1;// no selection by default

    public interface MyCallbackForControl {
        void onClickStatusUI(int position, StatusResponse2 objDialogResponse);
    }

    public StatusListAdapterDynamic(Context mContext, List<StatusResponse2> objRecentlyInstalleds, MyCallbackForControl objMyCallbackForControl, boolean isFromDetails, boolean isFromNever) {
        mList = objRecentlyInstalleds;
        this.objMyCallbackForControl = objMyCallbackForControl;
        this.mContext = mContext;
        isInstaller = false;
        this.isFromDetails = isFromDetails;
        this.isFromNever = isFromNever;
    }

    @NonNull
    @Override
    public MyViewHolderList onCreateViewHolder(@NonNull ViewGroup viewGroup, int viewType) {
        View itemView = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.raw_status4, viewGroup, false);
        MyViewHolderList viewHolder = new MyViewHolderList(itemView);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull final MyViewHolderList myViewHolder, final int i) {

        final StatusResponse2 objList = mList.get(i);

        myViewHolder.tvSlcId.setText(String.valueOf(objList.getSlcNo()));
        myViewHolder.tvslcName.setText(objList.getName());

        String dim =  mContext.getResources().getString(R.string.dim)+"<font color=#0072BC> " + objList.getDimPer() + "</font>";
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            myViewHolder.tvDimPer.setText(Html.fromHtml(dim, Html.FROM_HTML_MODE_LEGACY));
        } else
            myViewHolder.tvDimPer.setText(Html.fromHtml(dim));


        if (!objList.getDateTime().equals("")) {

            //if (objList.getDateTime().contains("T")) {
            //String[] ary = objList.getDateTime().split("T");
            //String finalStr = ary[0] + " " + ary[1];

            String date = objList.getDateTime();
            String[] ary = date.split(" ");

            String formated = ary[0] + " <font color=#A2A2A2>" + ary[1] + "</font>";

            myViewHolder.tvLastUpdate.setText(Html.fromHtml(formated));
            myViewHolder.ivTempArrow.setVisibility(View.VISIBLE);

            myViewHolder.llList.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    try {
                        if (mList.get(i).getLlId() != R.id.ll7) {
                            objMyCallbackForControl.onClickStatusUI(i, mList.get(i));
                            //Log.i(AppConstants.TAG, "SLCID: " + mList.get(i).getId());
                        }
                    } catch (Exception e) {
                    }
                }
            });
            //}
        } else {
            myViewHolder.tvLastUpdate.setText("N/A");
            myViewHolder.ivTempArrow.setVisibility(View.INVISIBLE);
        }

       /* if (isFromDetails) {
            myViewHolder.cbStatus.setVisibility(View.GONE);
            myViewHolder.llstatusLine2.setVisibility(View.GONE);
            if (isFromNever)
                myViewHolder.ivTempArrow.setVisibility(View.GONE);
            else
                myViewHolder.ivTempArrow.setVisibility(View.VISIBLE);
        } else {
            myViewHolder.cbStatus.setVisibility(View.VISIBLE);
            myViewHolder.llstatusLine2.setVisibility(View.GONE);
            myViewHolder.ivTempArrow.setVisibility(View.GONE);
        }*/
        myViewHolder.cbStatus.setVisibility(View.VISIBLE);
        myViewHolder.llstatusLine2.setVisibility(View.GONE);

        if (i % 2 == 0) {
            myViewHolder.llList.setBackgroundColor(mContext.getResources().getColor(R.color.colorWhite));
        } else {
            myViewHolder.llList.setBackgroundColor(mContext.getResources().getColor(R.color.colorSkyBlueList));
        }

        if (objList.getLlId() == R.id.ll5)
            myViewHolder.tvDimPer.setVisibility(View.VISIBLE);
        else
            myViewHolder.tvDimPer.setVisibility(View.GONE);


        String jsonString = mList.get(i).getJsonString();
        try {
            JSONObject jsonObject = new JSONObject(jsonString);

        } catch (JSONException e) {
            e.printStackTrace();
        }

        myViewHolder.tvBhs.setText(String.valueOf(objList.getA5BurnHrs()));
        myViewHolder.tvVolt.setText(String.valueOf(objList.getA1Voltage()));
        myViewHolder.tvCur.setText(String.valueOf(objList.getA2Current()));

        //in some cases, it will prevent unwanted situations
        myViewHolder.cbStatus.setOnCheckedChangeListener(null);
        myViewHolder.cbStatus.setChecked(mList.get(i).isChecked());

        myViewHolder.cbStatus.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                mList.get(i).setChecked(isChecked);
            }
        });
    }

    boolean isSecondViewVisible(String jsonString) {
        boolean flag = false;
        try {

            JSONObject jsonObject = new JSONObject(jsonString);
            Iterator iter = jsonObject.keys();

            while (iter.hasNext()) {
                CommonResponseStatusDetailsDialog obj = new CommonResponseStatusDetailsDialog();
                String key = (String) iter.next();
                String value = jsonObject.getString(key);
                String from = "";

                if (key.contains("@")) {
                    String[] splitAry = key.split("@");
                    if (key.startsWith("d") || key.startsWith("D")) {
                        from = "d";
                    } else if (key.startsWith("a") || key.startsWith("A")) {
                        from = "a";
                    }

                    if (value.equals("0.0"))
                        value = "0";

                    //if (value.contains(".0")) {
                    value = value.indexOf(".") < 0 ? value : value.replaceAll("0*$", "").replaceAll("\\.$", "");
                    //value.replace(".0","");
                    // }

                    key = splitAry[1];
                    obj.setId(key);
                    obj.setValue(value);
                    obj.setFrom(from);

                    if (key.equalsIgnoreCase("Communication")) {
                        if (value.equals("0")) {
                            flag = false;
                        } else {
                            flag = true;
                        }
                        break;
                    }
                }
                //Log.d("keywithvalues", "print    " + key + "   " + value);
            }


        } catch (JSONException e) {
            e.printStackTrace();
        }
        return flag;

    }

    @Override
    public int getItemCount() {
        return mList.size();
    }

    class MyViewHolderList extends RecyclerView.ViewHolder {

        @BindView(R.id.llList)
        LinearLayout llList;

        @BindView(R.id.tvSlcId)
        TextView tvSlcId;

        @BindView(R.id.tvslcName)
        TextView tvslcName;

        @BindView(R.id.tvLastUpdate)
        TextView tvLastUpdate;

        @BindView(R.id.cbStatus)
        CheckBox cbStatus;

        /*@BindView(R.id.ivLC)
        ImageView ivLC;*/

        @BindView(R.id.tvBhs)
        TextView tvBhs;

        @BindView(R.id.tvVolt)
        TextView tvVolt;

        @BindView(R.id.ivTempArrow)
        ImageView ivTempArrow;

        @BindView(R.id.llstatusLine2)
        LinearLayout llstatusLine2;

        @BindView(R.id.tvCur)
        TextView tvCur;

        @BindView(R.id.ivLS)
        ImageView ivLS;

        @BindView(R.id.ivD)
        ImageView ivD;

        @BindView(R.id.ivArrow)
        ImageView ivArrow;

        @BindView(R.id.tvDimPer)
        TextView tvDimPer;

        public MyViewHolderList(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

}

//String date = objList.getDateTime();
       /* if (!objList.getDateTime().toString().equalsIgnoreCase("")) {
            if (date.contains("T")) {
                String[] ary = date.split("T");
                String finalStr = ary[0] + " " + ary[1];
                myViewHolder.tvLastUpdate.setText(finalStr);
                myViewHolder.ivArrow.setVisibility(View.VISIBLE);

                myViewHolder.llList.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        objMyCallbackForControl.onClickStatusUI(i, mList.get(i));
                        //Log.i(AppConstants.TAG, "SLCID: " + mList.get(i).getId());
                    }
                });

            } else {
                myViewHolder.ivArrow.setVisibility(View.GONE);
            }


        } else {
            myViewHolder.tvLastUpdate.setText("N/A");
            myViewHolder.ivArrow.setVisibility(View.GONE);
        }
*/
       /*if (objList.getD1LampStatus().equals("0"))
            myViewHolder.ivLS.setImageDrawable(mContext.getResources().getDrawable(R.drawable.custom_round_red));
        else
            myViewHolder.ivLS.setImageDrawable(mContext.getResources().getDrawable(R.drawable.custom_round_green));

        if (objList.getD9Driver().equals("0"))
            myViewHolder.ivD.setImageDrawable(mContext.getResources().getDrawable(R.drawable.custom_round_green));
        else
            myViewHolder.ivD.setImageDrawable(mContext.getResources().getDrawable(R.drawable.custom_round_red));*/



       /* if(objList.getLlId()==R.id.ll1){
            myViewHolder.llstatusLine2.setVisibility(View.GONE);
        }*/

       /* if (isSecondViewVisible(mList.get(i).getJsonString())) {
            myViewHolder.llstatusLine2.setVisibility(View.VISIBLE);
        } else {
            myViewHolder.llstatusLine2.setVisibility(View.GONE);
        }*/