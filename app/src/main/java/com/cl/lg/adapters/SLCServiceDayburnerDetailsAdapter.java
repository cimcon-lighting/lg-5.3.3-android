package com.cl.lg.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.cl.lg.R;
import com.cl.lg.pojo.Daybuner.DayburnerOutages;
import com.cl.lg.pojo.FaultySLCMaster.SlcList;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class SLCServiceDayburnerDetailsAdapter extends RecyclerView.Adapter<SLCServiceDayburnerDetailsAdapter.MyViewHolderList> {

    Context mContext;
    List<com.cl.lg.pojo.Daybuner.List> mList;

    private MyCallbackForControl objMyCallbackForControl;

    public interface MyCallbackForControl {
        void onClickStatusUI(int position, com.cl.lg.pojo.Daybuner.List mList);
    }

    public SLCServiceDayburnerDetailsAdapter(Context mContext, List<com.cl.lg.pojo.Daybuner.List> ojbSLCList, MyCallbackForControl objMyCallbackForControl) {
        mList = ojbSLCList;
        this.objMyCallbackForControl = objMyCallbackForControl;
        this.mContext = mContext;

    }

    @NonNull
    @Override
    public MyViewHolderList onCreateViewHolder(@NonNull ViewGroup viewGroup, int viewType) {
        View itemView = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.raw_dashboard_details, viewGroup, false);
        MyViewHolderList viewHolder = new MyViewHolderList(itemView);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull final MyViewHolderList myViewHolder, final int i) {

        if (i % 2 == 0)
            myViewHolder.llList.setBackgroundColor(mContext.getResources().getColor(R.color.colorWhite));
        else
            myViewHolder.llList.setBackgroundColor(mContext.getResources().getColor(R.color.colorSkyBlueList));

        final com.cl.lg.pojo.Daybuner.List objList = mList.get(i);

        String finalStr = "";
        String date = objList.getDate();
        if (!date.equals("")) {
            if (date.contains("T")) {
                String[] ary = date.split("T");
                finalStr = ary[0] + " " + ary[1];
            } else
                finalStr = date;

        } else
            finalStr = "N/A";

        myViewHolder.cbDetail.setChecked(mList.get(i).isChecked());

        myViewHolder.cbDetail.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                mList.get(i).setChecked(isChecked);
            }
        });

        myViewHolder.llnonDayBurner.setVisibility(View.GONE);
        myViewHolder.llDayburner.setVisibility(View.VISIBLE);
        myViewHolder.tvLastUpdateDayBurner.setText(finalStr);
        myViewHolder.tvAddressDaybruner.setText(objList.getSlcAddress());
        myViewHolder.tvslcNameDayBurner.setText(objList.getSlcName());
        myViewHolder.ivTempArrowDayBurner.setVisibility(View.VISIBLE);
        myViewHolder.cbDetail.setVisibility(View.VISIBLE);

        myViewHolder.llDayburner.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                objMyCallbackForControl.onClickStatusUI(i, mList.get(i));
            }
        });

    }

    @Override
    public int getItemCount() {
        return mList.size();
    }

    class MyViewHolderList extends RecyclerView.ViewHolder {
        @BindView(R.id.cbDetail)
        CheckBox cbDetail;

        @BindView(R.id.llList)
        LinearLayout llList;

        @BindView(R.id.llDayburner)
        LinearLayout llDayburner;

        @BindView(R.id.llnonDayBurner)
        LinearLayout llnonDayBurner;

        @BindView(R.id.tvslcNameDayBurner)
        TextView tvslcNameDayBurner;

        @BindView(R.id.tvLastUpdateDayBurner)
        TextView tvLastUpdateDayBurner;

        @BindView(R.id.tvAddressDaybruner)
        TextView tvAddressDaybruner;

        @BindView(R.id.ivTempArrowDayBurner)
        ImageView ivTempArrowDayBurner;

        public MyViewHolderList(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}