package com.cl.lg.adapters;

import android.app.Activity;
import android.graphics.drawable.Drawable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.cl.lg.R;
import com.cl.lg.pojo.GridItem;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class InfoGridAdapter extends RecyclerView.Adapter<InfoGridAdapter.ViewHolder> {

    ArrayList<GridItem> mList;
    Activity activity;

    public InfoGridAdapter(ArrayList<GridItem> mList, Activity activity) {
        this.mList = mList;
        this.activity = activity;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.raw_grid_info, parent, false);
        return new InfoGridAdapter.ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(InfoGridAdapter.ViewHolder holder, int position) {
        GridItem objGridItem = mList.get(position);
        holder.ivImageName.setImageDrawable(objGridItem.getmDrawable());
        holder.tvTextname.setText(objGridItem.getmText());
    }

    @Override
    public int getItemCount() {
        return mList.size();
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.ivImageName)
        ImageView ivImageName;

        @BindView(R.id.tvTextname)
        TextView tvTextname;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}


