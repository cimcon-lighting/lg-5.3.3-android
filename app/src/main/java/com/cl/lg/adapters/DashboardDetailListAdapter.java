package com.cl.lg.adapters;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.cl.lg.R;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class DashboardDetailListAdapter extends RecyclerView.Adapter<DashboardDetailListAdapter.MyViewHolderList> {

    Context mContext;
    List<com.cl.lg.pojo.SLCStatus2.List> mList;


    private MyCallbackForControl objMyCallbackForControl;
    boolean isInstaller = false;
    boolean isFromDetails = false;
    boolean isFromNever = false;

    private int selectedPosition = -1;// no selection by default

    public interface MyCallbackForControl {
        void onClickStatusUI(int position, com.cl.lg.pojo.SLCStatus2.List objListDetail);
    }

    public DashboardDetailListAdapter(Context mContext, List<com.cl.lg.pojo.SLCStatus2.List> objRecentlyInstalleds, MyCallbackForControl objMyCallbackForControl, boolean isFromNever) {
        mList = objRecentlyInstalleds;
        this.objMyCallbackForControl = objMyCallbackForControl;
        this.mContext = mContext;
        isInstaller = false;
        this.isFromNever = isFromNever;
    }

    @NonNull
    @Override
    public MyViewHolderList onCreateViewHolder(@NonNull ViewGroup viewGroup, int viewType) {
        View itemView = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.raw_dashboard_details, viewGroup, false);
        MyViewHolderList viewHolder = new MyViewHolderList(itemView);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull final MyViewHolderList myViewHolder, final int i) {
        myViewHolder.llDayburner.setVisibility(View.GONE);
        final com.cl.lg.pojo.SLCStatus2.List objList = mList.get(i);

        myViewHolder.tvSlcId.setText(String.valueOf(objList.getSlcNo()));
        myViewHolder.tvslcName.setText(objList.getName());

        String date = objList.getDateTime();
        if (!objList.getDateTime().toString().equalsIgnoreCase("")) {
            if (date.contains("T")) {
                String[] ary = date.split("T");
                String finalStr = ary[0] + " " + ary[1];
                myViewHolder.tvLastUpdate.setText(finalStr);
                myViewHolder.ivTempArrow.setVisibility(View.VISIBLE);

                myViewHolder.llList.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        objMyCallbackForControl.onClickStatusUI(i, mList.get(i));
                        //Log.i(AppConstants.TAG, "SLCID: " + mList.get(i).getId());
                    }
                });

            } else {
                myViewHolder.ivTempArrow.setVisibility(View.GONE);
            }

        } else {
            myViewHolder.tvLastUpdate.setText("N/A");
            myViewHolder.ivTempArrow.setVisibility(View.GONE);
        }

        myViewHolder.tvAddress.setVisibility(View.GONE);

      /*  if (objList.getDateTime().equalsIgnoreCase("")) {
            myViewHolder.tvLastUpdate.setText("NA");
            myViewHolder.ivArrow.setVisibility(View.GONE);

        }else
            myViewHolder.ivArrow.setVisibility(View.VISIBLE);
*/

       if (isFromNever)
            myViewHolder.ivTempArrow.setVisibility(View.GONE);
        else
            myViewHolder.ivTempArrow.setVisibility(View.VISIBLE);

        if (i % 2 == 0) {
            myViewHolder.llList.setBackgroundColor(mContext.getResources().getColor(R.color.colorWhite));
        } else {
            myViewHolder.llList.setBackgroundColor(mContext.getResources().getColor(R.color.colorSkyBlueList));
        }

    }

    @Override
    public int getItemCount() {
        return mList.size();
    }

    class MyViewHolderList extends RecyclerView.ViewHolder {

        @BindView(R.id.llList)
        LinearLayout llList;

        @BindView(R.id.tvSlcId)
        TextView tvSlcId;

        @BindView(R.id.tvslcName)
        TextView tvslcName;

        @BindView(R.id.tvLastUpdate)
        TextView tvLastUpdate;

        @BindView(R.id.ivTempArrow)
        ImageView ivTempArrow;

        @BindView(R.id.tvAddress)
        TextView tvAddress;

        @BindView(R.id.llDayburner)
        LinearLayout llDayburner;

        public MyViewHolderList(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}