package com.cl.lg.adapters;

import android.app.Activity;
import android.content.Context;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.cl.lg.R;
import com.cl.lg.pojo.CommonDialogResponse;
import com.cl.lg.pojo.SetMode.DatumSetMode;
import com.cl.lg.utils.AppConstants;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ListDialogSetModeCommandAdapter extends RecyclerView.Adapter<ListDialogSetModeCommandAdapter.MyViewHolderList> {

    Context mContext;
    List<DatumSetMode> mList;

    List<CommonDialogResponse> mStringFilterList;

    private MyCallbackForControlDialog objMyCallbackForControl;

    public ListDialogSetModeCommandAdapter(Activity activity, ArrayList<DatumSetMode> filterList, ListDialogSetModeCommandAdapter.MyCallbackForControlDialog myCallbackForControlDialog) {
        this.mList = filterList;
        this.objMyCallbackForControl = myCallbackForControlDialog;
        this.mContext = activity;
    }

    public interface MyCallbackForControlDialog {
        void onClickForControl(int position, DatumSetMode response);
    }

    @NonNull
    @Override
    public MyViewHolderList onCreateViewHolder(@NonNull ViewGroup viewGroup, int viewType) {
        View itemView = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.raw_dialog_list_sent_command, viewGroup, false);
        MyViewHolderList viewHolder = new MyViewHolderList(itemView);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolderList myViewHolder, final int i) {

        myViewHolder.tvAssetKey.setText(mList.get(i).getModeName());
        //myViewHolder.tvId.setText(mList.get(i).getId());

        if (i % 2 == 0) {
            myViewHolder.llTitles.setBackgroundColor(mContext.getResources().getColor(R.color.colorLightGray));
        } else {
            myViewHolder.llTitles.setBackgroundColor(mContext.getResources().getColor(R.color.colorWhite));
        }

        myViewHolder.llTitles.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                objMyCallbackForControl.onClickForControl(i,mList.get(i));
                Log.i(AppConstants.TAG, "List: " + mList.get(i));
            }
        });

    }

    @Override
    public int getItemCount() {
        return mList.size();
    }

    class MyViewHolderList extends RecyclerView.ViewHolder {

        @BindView(R.id.llTitles)
        LinearLayout llTitles;

        @BindView(R.id.tvSLC)
        TextView tvAssetKey;

        @BindView(R.id.tvId1)
        TextView tvId;

        public MyViewHolderList(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}