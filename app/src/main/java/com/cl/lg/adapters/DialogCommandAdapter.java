package com.cl.lg.adapters;

import android.app.Activity;
import android.content.Context;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.cl.lg.R;
import com.cl.lg.pojo.Command;
import com.cl.lg.utils.AppConstants;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class DialogCommandAdapter extends RecyclerView.Adapter<DialogCommandAdapter.MyViewHolderList> {

    Context mContext;
    List<Command> mList;
    private MyCallbackForControlDialog objMyCallbackForControl;

    public DialogCommandAdapter(Activity activity, ArrayList<Command> filterList, DialogCommandAdapter.MyCallbackForControlDialog myCallbackForControlDialog) {
        this.mList = filterList;
        this.objMyCallbackForControl = myCallbackForControlDialog;
        this.mContext = activity;
    }

    public interface MyCallbackForControlDialog {
        void onClickForControl(int position, String slcNo);
    }

    @NonNull
    @Override
    public MyViewHolderList onCreateViewHolder(@NonNull ViewGroup viewGroup, int viewType) {
        View itemView = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.raw_commads, viewGroup, false);
        MyViewHolderList viewHolder = new MyViewHolderList(itemView);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolderList myViewHolder, final int i) {

        myViewHolder.tvCommands.setText(mList.get(i).getText());
        myViewHolder.tvCommands.setBackground(mList.get(i).getDrawable());
        myViewHolder.ivCommand.setImageDrawable(mList.get(i).getIcon());

        myViewHolder.llRawCommads.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                objMyCallbackForControl.onClickForControl(i, mList.get(i).getText());
                Log.i(AppConstants.TAG, "List: " + mList.get(i).getText());
            }
        });
    }

    @Override
    public int getItemCount() {
        return mList.size();
    }

    class MyViewHolderList extends RecyclerView.ViewHolder {

        @BindView(R.id.ivCommand)
        ImageView ivCommand;

        @BindView(R.id.llRawCommads)
        FrameLayout llRawCommads;

        @BindView(R.id.tvCommands)
        TextView tvCommands;

        public MyViewHolderList(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}