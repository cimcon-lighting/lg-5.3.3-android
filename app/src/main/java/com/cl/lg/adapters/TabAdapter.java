package com.cl.lg.adapters;

import android.app.Activity;

import android.graphics.drawable.Drawable;
import androidx.annotation.Nullable;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.cl.lg.R;

import java.util.ArrayList;
import java.util.List;

public class TabAdapter extends FragmentStatePagerAdapter {
    private final List<Fragment> mFragmentList = new ArrayList<>();
    private final List<Drawable> mFragmentTitleList = new ArrayList<>();

    Activity activity;

    public TabAdapter(FragmentManager fm, Activity activity) {
        super(fm);
        this.activity=activity;
    }


    @Override
    public Fragment getItem(int position) {
        return mFragmentList.get(position);
    }

    public void addFragment(Fragment fragment,Drawable drawable) {
        mFragmentList.add(fragment);
        mFragmentTitleList.add(drawable);
    }

    @Nullable
    @Override
    public CharSequence getPageTitle(int position) {
        return null;
    }

    @Override
    public int getCount() {
        return mFragmentList.size();
    }

    public View getTabView(int position) {
        View view = LayoutInflater.from(activity).inflate(R.layout.custom_tab, null);
        ImageView iv=view.findViewById(R.id.ivTab);
        iv.setImageDrawable(mFragmentTitleList.get(position));
        return view;
    }

    public View getSelectedTabView(int position) {
        View view = LayoutInflater.from(activity).inflate(R.layout.custom_tab, null);
        LinearLayout tabTextView = view.findViewById(R.id.llCustomTab);
        tabTextView.setBackgroundColor(activity.getResources().getColor(R.color.colorGreenGraph));
        return view;
    }
} 