package com.cl.lg.adapters;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.cl.lg.R;
import com.cl.lg.utils.AppConstants;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class GatwayDetailsAdapter extends RecyclerView.Adapter<GatwayDetailsAdapter.MyViewHolderList> {

    Context mContext;
    List<com.cl.lg.pojo.GatewayDetails.List> mList;
    String type;

    private MyCallbackForControl objMyCallbackForControl;

    public interface MyCallbackForControl {
        void onClickStatusUI(int position, com.cl.lg.pojo.GatewayDetails.List objListDetail);
        void onClickSLC(String slcId);
    }


    public GatwayDetailsAdapter(Context mContext, List<com.cl.lg.pojo.GatewayDetails.List> objRecentlyInstalleds, MyCallbackForControl objMyCallbackForControl,String type) {
        mList = objRecentlyInstalleds;
        this.objMyCallbackForControl = objMyCallbackForControl;
        this.mContext = mContext;
        this.type=type;
    }

    @NonNull
    @Override
    public MyViewHolderList onCreateViewHolder(@NonNull ViewGroup viewGroup, int viewType) {
        View itemView = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.raw_gatway_list, viewGroup, false);
        MyViewHolderList viewHolder = new MyViewHolderList(itemView);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull final MyViewHolderList myViewHolder, final int position) {

        final com.cl.lg.pojo.GatewayDetails.List objList = mList.get(position);
        myViewHolder.tvGatewayname.setText(objList.getGatewayName());
        myViewHolder.tvAddress.setText(objList.getDeviceAddress());
        myViewHolder.tvCommType.setText(objList.getDetails().getCommunicationType());
        myViewHolder.tvslc.setText(objList.getCommCount()+"/"+objList.getSlcCount());

        if(type.equalsIgnoreCase(AppConstants.G_CONNECTED))
            myViewHolder.ivGatwayType.setImageDrawable(mContext.getDrawable(R.drawable.g_active_on_4_12));
        else if(type.equalsIgnoreCase(AppConstants.G_DISCONNECTED))
            myViewHolder.ivGatwayType.setImageDrawable(mContext.getDrawable(R.drawable.gateway_off));
        else if(type.equalsIgnoreCase(AppConstants.G_POWERLOSS))
            myViewHolder.ivGatwayType.setImageDrawable(mContext.getDrawable(R.drawable.gateway_powerloss));

        myViewHolder.llList.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //objMyCallbackForControl.onClickStatusUI(position,objList);
            }
        });

        myViewHolder.llAssignedSLC.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                objMyCallbackForControl.onClickSLC(objList.getRtuGroupId());
            }
        });
        myViewHolder.ivSLC.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                objMyCallbackForControl.onClickSLC(objList.getRtuGroupId());
            }
        });


        myViewHolder.llGatway.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                objMyCallbackForControl.onClickStatusUI(position,objList);
            }
        });
        myViewHolder.ivGateway.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                objMyCallbackForControl.onClickStatusUI(position,objList);
            }
        });

        if(position%2==0){
            myViewHolder.llList.setBackgroundColor(mContext.getResources().getColor(R.color.colorWhite));
        }else
            myViewHolder.llList.setBackgroundColor(mContext.getResources().getColor(R.color.colorSkyBlueList));

    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @Override
    public int getItemCount() {
        return mList.size();
    }

    class MyViewHolderList extends RecyclerView.ViewHolder {

        @BindView(R.id.llList)
        LinearLayout llList;

        @BindView(R.id.ivGatwayType)
        ImageView ivGatwayType;

        @BindView(R.id.tvGatewayname)
        TextView tvGatewayname;

        @BindView(R.id.tvslc)
        TextView tvslc;

        @BindView(R.id.tvCommType)
        TextView tvCommType;

        @BindView(R.id.tvAddress)
        TextView tvAddress;

        @BindView(R.id.ivGateway)
        ImageView ivGateway;

        @BindView(R.id.ivSLC)
        ImageView ivSLC;

        @BindView(R.id.llAssignedSLC)
        LinearLayout llAssignedSLC;

        @BindView(R.id.llGatway)
        LinearLayout llGatway;

        public MyViewHolderList(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}