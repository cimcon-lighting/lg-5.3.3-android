package com.cl.lg.networking;

import com.cl.lg.pojo.AssignedSLC.AssignedMaster;
import com.cl.lg.pojo.ClientType.ClientType;
import com.cl.lg.pojo.CommandResponse.CommandResponse;
import com.cl.lg.pojo.DashboardCounts.DashboardCounts;
import com.cl.lg.pojo.Daybuner.DayburnerOutages;
import com.cl.lg.pojo.FaultySLCMaster.SLCFaulty;
import com.cl.lg.pojo.ForgetPass;
import com.cl.lg.pojo.GatewayDetails.GatewayMaster;
import com.cl.lg.pojo.GatwayCount.GatewayCount;
import com.cl.lg.pojo.GetCommands.GetCommand;
import com.cl.lg.pojo.GetFaultySLCCount.GetFaultySLCCount;
import com.cl.lg.pojo.GetGatwaylist.GetGatway;
import com.cl.lg.pojo.GetStatus.GetStatus;
import com.cl.lg.pojo.GroupName.GroupName;
import com.cl.lg.pojo.LoginMaster.LoginMaster;
import com.cl.lg.pojo.Map.MapMaster;
import com.cl.lg.pojo.Parameters.ParametersMasters;
import com.cl.lg.pojo.SAMLDetail;
import com.cl.lg.pojo.SLCName.SLCName;
import com.cl.lg.pojo.SLCStatus2.SLCStatusMaster;
import com.cl.lg.pojo.SecurityData;
import com.cl.lg.pojo.SentCmdDetails.SentCmdDetails;
import com.cl.lg.pojo.SentCommand.SentCommand;
import com.cl.lg.pojo.SetMode.SetmodeCmd;
import com.cl.lg.pojo.TrackIds;
import com.cl.lg.pojo.UpgradeResponse;
import com.cl.lg.pojo.saml_details.SAMLDetails;
import com.cl.lg.utils.AppConstants;
import com.google.gson.JsonObject;

import java.util.HashMap;

import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Headers;
import retrofit2.http.POST;

/**
 * Created by vrajesh on 1/4/2018.
 */

public interface API {

    @FormUrlEncoded
    @POST(AppConstants.API_SCODE)
    Call<SecurityData> checkUniqueCode(
            @Field("code") String code,
            @Field("source") String source
    );

    @POST(AppConstants.API_LOGIN)
    Call<LoginMaster> getToken1(
            @Header("grant_type") String grant_type,
            @Header("username") String username,
            @Header("ClientID") String ClientID,
            @Header("password") String password,
            @Header("IsSecured") String IsSecured,
            @Header("Origins") String origin,
            @Header("Version") String version,
            @Header("Source") String source
    );


    @POST(AppConstants.API_LOGIN)
    Call<LoginMaster> getToken(
            @Header("grant_type") String grant_type,
            @Header("username") String username,
            @Header("ClientID") String ClientID,
            @Header("password") String password,
            @Header("IsSecured") String IsSecured
    );


    @GET(AppConstants.API_GET_COMMANDS)
    @Headers("Content-Type:application/json")
    Call<GetCommand> getCommannd(
            @Header("Authorization") String authorization,
            @Header("IsSecured") String IsSecured

    );

    @GET(AppConstants.API_GET_STATUS)
    @Headers("Content-Type:application/json")
    Call<GetStatus> getStatus(
            @Header("Authorization") String authorization,
            @Header("IsSecured") String IsSecured
    );

    @GET(AppConstants.API_GET_GATEWAY)
    @Headers("Content-Type:application/json")
    Call<GetGatway> getGatwayList(
            @Header("Authorization") String authorization,
            @Header("GatewayType") String gatwaytype,
            @Header("IsSecured") String IsSecured

    );

    @GET(AppConstants.API_GET_TRACKID)
    @Headers("Content-Type:application/json")
    Call<TrackIds> getTrackId(
            @Header("Authorization") String authorization,
            @Header("TrackID") String trackid,
            @Header("IsSecured") String IsSecured
    );

    /*@POST(AppConstants.API_SENT_COMMAND)
    @Headers("Content-Type:application/json")
    Call<SentCommand> getSentCommand(
            @Header("Authorization") String authorization,
            @Header("PageStartIndex") String startIndex,
            @Body RequestBody params
    ); //  @Header("PageSize") String pageSizze,*/

    @POST(AppConstants.API_SENT_COMMAND)
    @Headers("Content-Type:application/json")
    Call<SentCommand> getSentCommand(
            @Header("Authorization") String authorization,
            @Header("PageStartIndex") String startIndex,
            @Body JsonObject params,
            @Header("IsSecured") String IsSecured,
            @Header("PageSize") String pageSize
    );

    @POST(AppConstants.API_SLC_STATUS_LIST)
    @Headers("Content-Type:application/json")
    Call<SLCStatusMaster> getSLCStatusList(
            @Header("Authorization") String authorization,
            @Header("pageno") String pageno,
            @Header("PageSize") String pageSize,
            @Header("lamptypeid") int lamptypeid,
            @Body RequestBody params,
            @Header("IsSecured") String IsSecured
    );

    @POST(AppConstants.API_SLC_STATUS_LIST)
    @Headers("Content-Type:application/json")
    Call<JsonObject> getSLCStatusListTemp(
            @Header("Authorization") String authorization,
            @Header("pageno") String pageno,
            @Header("PageSize") String pageSize,
            @Header("lamptypeid") String lamptypeid,
            @Body RequestBody params,
            @Header("IsSecured") String IsSecured
    );

    @POST(AppConstants.API_GET_MAP_DATA)
    @Headers("Content-Type:application/json")
    Call<MapMaster> getMap(
            @Header("Authorization") String authorization,
            @Header("lamptypeid") String lamptypid,
            @Body RequestBody params,
            @Header("IsSecured") String IsSecured
    );

    @POST(AppConstants.API_FORGOT_PASSWORD)
    @Headers("Content-Type:application/json")
    Call<ForgetPass> getForgetPass(@Body RequestBody rb, @Header("IsSecured") String IsSecured);

    @GET(AppConstants.API_GET_DASHBOARD_COUNT)
    @Headers("Content-Type:application/json")
    Call<DashboardCounts> getDashboarCounts(
            @Header("Authorization") String authorization,
            @Header("pType") String pType,
            @Header("IsSecured") String IsSecured
    );

    @GET(AppConstants.API_GET_MODE_CMD_LIST)
    @Headers("Content-Type:application/json")
    Call<SetmodeCmd> getSetMode(
            @Header("Authorization") String authorization,
            @Header("IsSecured") String IsSecured
            );

    @GET(AppConstants.API_GET_SLC_NAME)
    @Headers("Content-Type:application/json")
    Call<SLCName> getSLCName(
            @Header("Authorization") String authorization,
            @Header("SLCNO") String slcNo,
            @Header("Gatewayid") String gatewayid,
            @Header("lamptypeid") String lamptypId,
            @Header("IsSecured") String IsSecured,
            @Header("NodeType") String nodetypId
    );

    @GET(AppConstants.API_GET_SLC_GP)
    @Headers("Content-Type:application/json")
    Call<GroupName> getSLCGp(
            @Header("Authorization") String authorization,
            @Header("Group") String gid,
            @Header("lamptypeid") String lamptypId,
            @Header("IsSecured") String IsSecured,
            @Header("NodeType") String nodetypId
    );

    //commands
    @POST(AppConstants.API_CMD_RESETTRIP)
    @Headers("Content-Type:application/json")
    Call<CommandResponse> resetTrip(
            @Header("Authorization") String authorization,
            @Body RequestBody params,
            @Header("IsSecured") String IsSecured
    );

    @POST(AppConstants.API_CMD_SENDREADDATACOMMAND)
    @Headers("Content-Type:application/json")
    Call<CommandResponse> ReadData(
            @Header("Authorization") String authorization,
            @Body RequestBody params,
            @Header("IsSecured") String IsSecured
    );

    @POST(AppConstants.API_CMD_SWITCHONOFFSLC)
    @Headers("Content-Type:application/json")
    Call<CommandResponse> switchOnOffSlc(
            @Header("Authorization") String authorization,
            @Body RequestBody params,
            @Header("IsSecured") String IsSecured
    );

    @POST(AppConstants.API_CMD_DIMSLC)
    @Headers("Content-Type:application/json")
    Call<CommandResponse> dimSlc(
            @Header("Authorization") String authorization,
            @Body RequestBody params,
            @Header("IsSecured") String IsSecured
    );

    @POST(AppConstants.API_CMD_SETMODE)
    @Headers("Content-Type:application/json")
    Call<CommandResponse> setMode(
            @Header("Authorization") String authorization,
            @Body RequestBody params,
            @Header("IsSecured") String IsSecured
    );

    @POST(AppConstants.API_CMD_GETMODE)
    @Headers("Content-Type:application/json")
    Call<CommandResponse> getMode(
            @Header("Authorization") String authorization,
            @Body RequestBody params,
            @Header("IsSecured") String IsSecured
    );

    @GET(AppConstants.API_SEND_CMD_DETAILS)
    @Headers("Content-Type:application/json")
    Call<SentCmdDetails> getSendCmdDetails(
            @Header("Authorization") String authorization,
            @Header("PageSize") String pageSize,
            @Header("TrackID") String trackId,
            @Header("pageno") String pgStartIndex,
            @Header("IsSecured") String IsSecured
    );

    @GET(AppConstants.API_GET_FAULTY_SLC_COUNT_)
    @Headers("Content-Type:application/json")
    Call<GetFaultySLCCount> getFaultySLCCount(
            @Header("Authorization") String authorization,
            @Header("IsSecured") String IsSecured
    );

    @GET(AppConstants.API_GATWAY_COUNT)
    @Headers("Content-Type:application/json")
    Call<GatewayCount> getGatwayCount(
            @Header("Authorization") String authorization,
            @Header("IsSecured") String IsSecured
    );

    @GET(AppConstants.API_GATWAYLIST)
    @Headers("Content-Type:application/json")
    Call<GatewayMaster> getGatwayListDetails(
            @Header("Authorization") String authorization,
            @Header("pageNo") int pageNo,
            @Header("pageSize") int pageSize,
            @Header("GatewayType") String GatewayType,
            @Header("GatewayName") String GatewayName,
            @Header("Gatewayid") String Gatewayid,
            @Header("IsSecured") String IsSecured
    );

    @POST(AppConstants.API_ASSIGNED_SLC)
    @Headers("Content-Type:application/json")
    Call<AssignedMaster> getAssignedSLC(
            @Header("Authorization") String authorization,
            @Header("pageNo") int pageNo,
            @Header("pageSize") int pageSize,
            @Body HashMap<String, String> body,
            @Header("IsSecured") String IsSecured
    );

    @GET(AppConstants.API_GET_FAULTY_SLC_DETAILS_)
    @Headers("Content-Type:application/json")
    Call<SLCFaulty> getFaultSLCService(
            @Header("Authorization") String authorization,
            @Header("pageNo") int pageNo,
            @Header("pageSize") int pageSize,
            @Header("FaultyPrameter") int FaultyPrameter,
            @Header("lamptypeid") String lamptypeid,
            @Header("IsSecured") String IsSecured
    );


    @GET(AppConstants.API_GET_FAULTY_SLC_DETAILS_)
    @Headers("Content-Type:application/json")
    Call<JsonObject> getFaultSLCService2(
            @Header("Authorization") String authorization,
            @Header("pageNo") int pageNo,
            @Header("pageSize") int pageSize,
            @Header("FaultyPrameter") int FaultyPrameter,
            @Header("SlcId") String slcid,
            @Header("lamptypeid") String lampType,
            @Header("IsSecured") String IsSecured

    );

    @GET(AppConstants.API_DAYBURNER_OUTAGES)
    @Headers("Content-Type:application/json")
    Call<DayburnerOutages> getDayBurnerOutages(
            @Header("Authorization") String authorization,
            @Header("ReportType") String ReportType,
            @Header("PageIndex") int pageNo,
            @Header("StartDate") String StartDate,
            @Header("EndDate") String EndDate,
            @Header("SLCName") String SLCName,
            @Header("lamptypeid") String lampType,
            @Header("IsSecured") String IsSecured
    );

    @GET(AppConstants.API_DAYBURNER_OUTAGES)
    @Headers("Content-Type:application/json")
    Call<JsonObject> getDayBurnerOutages2(
            @Header("Authorization") String authorization,
            @Header("ReportType") String ReportType,
            @Header("PageIndex") int pageNo,
            @Header("StartDate") String StartDate,
            @Header("EndDate") String EndDate,
            @Header("SLCName") String SLCName,
            @Header("lamptypeid") String lampType,
            @Header("IsSecured") String IsSecured
            );

    @GET(AppConstants.API_STATUS_TOTAL)
    @Headers("Content-Type:application/json")
    Call<JsonObject> getStatusTotal(
            @Header("Authorization") String authorization,
            @Header("pageNo") int pageNo,
            @Header("pageSize") int pageSize,
            @Header("SlcId") String slcid,
            @Header("lamptypeid") String lampType,
            @Header("IsSecured") String IsSecured,
            @Header("NodeType") String nodeType
    );

    @GET(AppConstants.API_HISTORICAL_DATA)
    @Headers("Content-Type:application/json")
    Call<JsonObject> getHistoricalData(
            @Header("Authorization") String authorization,
            @Header("pageNo") int pageNo,
            @Header("pageSize") int pageSize,
            @Header("SlcId") String SlcId,
            @Header("gId") String gId,
            @Header("FromDate") String FromDate,
            @Header("ToDate") String ToDate,
            @Header("FromTime") String FromTime,
            @Header("ToTime") String ToTime,
            @Header("IsSecured") String IsSecured
    );

    @GET(AppConstants.API_PARAMETERS)
    @Headers("Content-Type:application/json")
    Call<ParametersMasters> getParametersApi(
            @Header("Authorization") String authorization,
            @Header("lamptypeid") String lampType,
            @Header("IsSecured") String IsSecured
    );

    @GET(AppConstants.API_GET_SLC_UNMAPPED)
    @Headers("Content-Type:application/json")
    Call<SLCName> getSLCUnmapped(
            @Header("Authorization") String authorization,
            @Header("SLCNO") String slcNo,
            @Header("lamptypeid") String lamptypId,
            @Header("IsSecured") String IsSecured,
            @Header("NodeType") String nodeType
    );

    @FormUrlEncoded
    @POST(AppConstants.API_UPGRADE)
    Call<UpgradeResponse> getUpgradeConfirmation(
            @Field("Email") String Email,
            @Field("Name") String Name,
            @Field("Clientid") String Clientid,
            @Header("IsSecured") String IsSecured
    );

    @GET(AppConstants.API_GET_CLIENT_TYPE)
    @Headers("Content-Type:application/json")
    Call<ClientType> getClientType(
            @Header("Authorization") String authorization,
            @Header("IsSecured") String IsSecured
    );


    @GET(AppConstants.API_GET_SAML_DETAIL)
    @Headers("Content-Type:application/json")
    Call<SAMLDetails> getSAMLDetails(
      @Header("Clientid") String Clientid,
      @Header("isSecured") String isSecured,
      @Header("source") String source
    );

    @POST(AppConstants.API_GET_SAML_TOKEN)
    Call<LoginMaster> getTokenByWindowsLogin(
            @Header("grant_type") String grant_type,
            @Header("username") String username,
            @Header("IsSecured") String IsSecured,
            @Header("Origins") String origin,
            @Header("Version") String version,
            @Header("Source") String source,
            @Header("clientid") String clientid
    );

}