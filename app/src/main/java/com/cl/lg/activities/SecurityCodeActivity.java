package com.cl.lg.activities;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.os.Build;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.appcompat.app.AppCompatActivity;

import android.os.Environment;
import android.util.Base64;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.VolleyError;
import com.bumptech.glide.Glide;
import com.cl.lg.LG;
import com.cl.lg.R;
import com.cl.lg.networking.API;
import com.cl.lg.pojo.LoginMaster.LoginMaster;
import com.cl.lg.pojo.SAMLDetail;
import com.cl.lg.pojo.SecurityData;
import com.cl.lg.pojo.saml_details.SAMLDetails;
import com.cl.lg.utils.AppConstants;
import com.cl.lg.utils.MSGraphRequestWrapper;
import com.cl.lg.utils.Utils;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.microsoft.identity.client.AuthenticationCallback;
import com.microsoft.identity.client.IAccount;
import com.microsoft.identity.client.IAuthenticationResult;
import com.microsoft.identity.client.IPublicClientApplication;
import com.microsoft.identity.client.ISingleAccountPublicClientApplication;
import com.microsoft.identity.client.PublicClientApplication;
import com.microsoft.identity.client.exception.MsalClientException;
import com.microsoft.identity.client.exception.MsalException;
import com.microsoft.identity.client.exception.MsalServiceException;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.http.Url;

import static com.cl.lg.utils.AppConstants.GRANT_TYPE_VALUE;
import static com.cl.lg.utils.AppConstants.IS_SECURED;

public class SecurityCodeActivity extends AppCompatActivity {

    @BindView(R.id.ivBGSCode)
    ImageView ivBGSCode;

    @BindView(R.id.btSend)
    TextView btSend;

    @BindView(R.id.etCode)
    EditText etCode;

    boolean isPermissionGranted = false;
    ProgressDialog progressSecurityCode;

    SharedPreferences spf, spf_login;
    API objApi;
    API objApi2;
    boolean isRemeber = false;

    Utils objUtils;
    private String[] PERMISSIONS = {
            android.Manifest.permission.ACCESS_NETWORK_STATE,
            Manifest.permission.WRITE_EXTERNAL_STORAGE,
            Manifest.permission.READ_EXTERNAL_STORAGE,
    };
    private static final int PERMISSION_ALL = 1;

    private FirebaseAnalytics mFirebaseAnalytics;

    /* Azure AD Variables */
    private ISingleAccountPublicClientApplication mSingleAccountApp;
    private IAccount mAccount;
    String version;
    String clientId;

    String sha1Singature;

    String packageName;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activtiy_security_code);
        init();
    }

    void init() {
        ButterKnife.bind(this);
        Glide.with(this).load(R.drawable.bg_new).into(ivBGSCode);

        objUtils = new Utils();
        mFirebaseAnalytics = FirebaseAnalytics.getInstance(this);

        objApi = new LG().networkCall(SecurityCodeActivity.this, true);

        spf = getSharedPreferences(AppConstants.SPF, MODE_PRIVATE);
        spf_login = getSharedPreferences(AppConstants.SPF_LOGIN, MODE_PRIVATE);

        //forcefully logout
        spf.edit().clear().apply();

        btSend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                SendSecurityCode();
            }
        });

        etCode.setText("");
        progressSecurityCode = new ProgressDialog(SecurityCodeActivity.this);
        progressSecurityCode.setMessage(getResources().getString(R.string.loading));
        progressSecurityCode.setCancelable(false);

        etCode.setOnEditorActionListener(new EditText.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int actionId, KeyEvent keyEvent) {
                boolean handled = false;
                if (actionId == EditorInfo.IME_ACTION_DONE) {
                    // TODO do something
                    handled = true;
                    SendSecurityCode();
                }
                return handled;
            }
        });

        isRemeber = spf_login.getBoolean(AppConstants.IS_REMMEBER, false);
        if (isRemeber) {
            etCode.setText(spf_login.getString(AppConstants.SECURITY_CODE, ""));
        }

        if (AppConstants.isLogDisplay) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                request6plus();
            } else isPermissionGranted = true;
        }

        mFirebaseAnalytics.setCurrentScreen(this, "securityCode", null);
        /*Bundle bundle = new Bundle();
        bundle.putString(FirebaseAnalytics.Param.ITEM_ID, "test ID");
        bundle.putString(FirebaseAnalytics.Param.ITEM_NAME, "test Item");
        bundle.putString(FirebaseAnalytics.Param.CONTENT_TYPE, "image");
        mFirebaseAnalytics.logEvent(FirebaseAnalytics.Event.SELECT_CONTENT, bundle);*/

        packageName = getApplicationContext().getPackageName();
        try {
            PackageInfo info = getPackageManager().getPackageInfo(packageName, PackageManager.GET_SIGNATURES);
            for (Signature signature : info.signatures) {
                MessageDigest md;
                md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());
                sha1Singature = new String(Base64.encode(md.digest(), 0));
                //String something = new String(Base64.encodeBytes(md.digest()));
                Log.e(AppConstants.TAG, sha1Singature);
            }
        } catch (PackageManager.NameNotFoundException e1) {
            Log.e("name not found", e1.toString());
        } catch (NoSuchAlgorithmException e) {
            Log.e("no such an algorithm", e.toString());
        } catch (Exception e) {
            Log.e("exception", e.toString());
        }

        try {
            PackageInfo pInfo = getPackageManager().getPackageInfo(getPackageName(), 0);
            version = pInfo.versionName;

        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
            version = "";
        }
        //samlCheckedPreviousLogin();
    }


    void deviceInfo() {
        try {
            PackageInfo pInfo = getPackageManager().getPackageInfo(getPackageName(), 0);
            String version = getResources().getString(R.string.version) + pInfo.versionName;
            Utils.addLog("Device Info: " + Build.MANUFACTURER +
                    " | " + Build.MODEL +
                    " | OS " + Build.VERSION.RELEASE +
                    " | API: " + Build.VERSION.SDK_INT
                    + " | " + version);
        } catch (Exception e) {
        }
    }

    private void request6plus() {
        if (!Utils.hasPermissions(SecurityCodeActivity.this, PERMISSIONS)) {
            ActivityCompat.requestPermissions(SecurityCodeActivity.this, PERMISSIONS, PERMISSION_ALL);
            return;
        } else {
            isPermissionGranted = true;
            deviceInfo();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {
        switch (requestCode) {
            case PERMISSION_ALL: {
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED
                ) {
                    isPermissionGranted = true;
                    deviceInfo();
                } else {
                    isPermissionGranted = false;
                    Toast.makeText(SecurityCodeActivity.this, getResources().getString(R.string.all_permission_granted), Toast.LENGTH_SHORT).show();
                }
                return;
            }
        }
    }

    void SendSecurityCode() {
        if (etCode.getText().toString().trim().equals("")) {
            //etCode.setError(getResources().getString(R.string.please_enter_unique_code));
            Utils.dialogForMessage(SecurityCodeActivity.this, getResources().getString(R.string.please_enter_unique_code));
            return;
        }

        //API
        if (Utils.isInternetAvailable(SecurityCodeActivity.this)) {
            sendUniqueCodeToServer(etCode.getText().toString());
        } else {
            Utils.dialogForMessage(SecurityCodeActivity.this, getResources().getString(R.string.no_internet_connection));
        }

        //Crashlytics.getInstance().crash(); // Force a crash for testing purpose
    }

    void sendUniqueCodeToServer(final String uniqueCode) {
        progressSecurityCode.show();
        //if (isPermissionGranted)
        Utils.addLog("Security API S-Code: " + uniqueCode);
        Utils.addLog("Security API Call Started....");
        final Call<SecurityData> objCheckUnieCall = objApi.checkUniqueCode(uniqueCode, "Android");

        objCheckUnieCall.enqueue(new Callback<SecurityData>() {
            @Override
            public void onResponse(Call<SecurityData> call, Response<SecurityData> response) {
                try {
                   /* if (progressSecurityCode.isShowing())
                        progressSecurityCode.dismiss();*/

                    if (response.code() == 200) {

                        //if(isRemeber)
                        spf_login.edit().putString(AppConstants.SECURITY_CODE, uniqueCode).apply();

                        Utils.addLog("Security API 200 Response.");
                        SecurityData objCheckUniqueCode = response.body();
                        if (objCheckUniqueCode.getStatus().toString().equals("1")) {


                       /* Bundle params = new Bundle();
                        params.putString("screen_name", "Security Code");
                        mFirebaseAnalytics.logEvent("share_image", params);
                        Bundle bundle = new Bundle();
                        bundle.putString(FirebaseAnalytics.Param.METHOD, "Security Code");
                        mFirebaseAnalytics.logEvent(FirebaseAnalytics.Event.LOGIN, bundle);*/

                            SharedPreferences.Editor editor = spf.edit();
                            Log.i(AppConstants.TAG, objCheckUniqueCode.toString());

                            editor.putString(AppConstants.USER_ID, objCheckUniqueCode.getUserid());

                            //editor.putString(AppConstants.LGVERSION,objCheckUniqueCode.getLGVersion());

                            //live
                            editor.putString(AppConstants.API_URL, objCheckUniqueCode.getAPIURL());
                            editor.putString(AppConstants.CLIENT_ID, objCheckUniqueCode.getClientID());
                            editor.putString(AppConstants.APP_TYPE, objCheckUniqueCode.getApptype());
                            editor.putString(AppConstants.APP_TYPE_MSG, objCheckUniqueCode.getApptypemessage());

                            //Rimple:
                            //editor.putString(AppConstants.API_URL, "http://199.199.50.114/WebAPI/");
                            //editor.putString(AppConstants.CLIENT_ID, "");

                            editor.putString(AppConstants.MESSAGE, objCheckUniqueCode.getMessage());

                            editor.putString(AppConstants.MESSAGE_NEAR_ME, objCheckUniqueCode.getNearmedistancemessage());
                            editor.putString(AppConstants.MILES, objCheckUniqueCode.getNearmedistance());

                            editor.putString(AppConstants.CLIENT_NAME, objCheckUniqueCode.getClientName());

                            //UI visibility of POLE EDIT - DISPLAY , Camera preview,
                            editor.apply();

                            String event_name = objCheckUniqueCode.getClientName() + "_" + "securityCode";
                            Bundle bundle = new Bundle();
                            bundle.putString(FirebaseAnalytics.Param.ITEM_NAME, event_name);
                            mFirebaseAnalytics.logEvent(FirebaseAnalytics.Event.SELECT_CONTENT, bundle);


                          /*  startActivity(new Intent(SecurityCodeActivity.this, LoginActivity.class));
                            finish();*/
                            clientId = objCheckUniqueCode.getClientID();
                            objApi2 = new LG().networkCall(SecurityCodeActivity.this, false);

                            getSAMLDetail(objCheckUniqueCode.getClientID());

                            Utils.addLog("Security API Status :1");
                            Utils.addLog("Security API : ClientID: " + objCheckUniqueCode.getClientID() + " USER ID: " + objCheckUniqueCode.getUserid()
                                    + " API _URL: " + objCheckUniqueCode.getAPIURL().toString()
                            );


                        } else if (objCheckUniqueCode.getStatus().toString().equals("0")) {
                            objUtils.dismissProgressDialog(progressSecurityCode);
                            Utils.addLog("Security API Status :0");
                            Utils.dialogForMessage(SecurityCodeActivity.this, objUtils.getStringResourceByName(SecurityCodeActivity.this, objCheckUniqueCode.getMessage().toString()));
                        }
                    } else {
                        Utils.addLog("Security API Code: " + response.code() + " Error Msg: " + response.errorBody());
                        objUtils.responseHandle(SecurityCodeActivity.this, response.code(), response.errorBody());
                    }
                    btSend.setClickable(true);

                } catch (Exception e) {
                    objUtils.dismissProgressDialog(progressSecurityCode);
                    Utils.dialogForMessage(SecurityCodeActivity.this, getResources().getString(R.string.http_error_msg));
                }
            }

            @Override
            public void onFailure(Call<SecurityData> call, Throwable t) {
                Utils.addLog("Security API OnFailure called: " + t.getMessage().toString());
                objUtils.dismissProgressDialog(progressSecurityCode);

                Utils.dialogForMessage(SecurityCodeActivity.this, getResources().getString(R.string.http_error_msg));
                btSend.setClickable(true);
            }
        });
    }


    /**
     * Extracts a scope array from a text field,
     * i.e. from "User.Read User.ReadWrite" to ["user.read", "user.readwrite"]
     */
    private String[] getScopes() {
        String[] scope = {"user.read"};
        return scope;
    }

    /**
     * Load the currently signed-in account, if there's any.
     */
    private void loadAccount() {
        if (mSingleAccountApp == null) {
            objUtils.dismissProgressDialog(progressSecurityCode);
            return;
        }

        mSingleAccountApp.getCurrentAccountAsync(new ISingleAccountPublicClientApplication.CurrentAccountCallback() {
            @Override
            public void onAccountLoaded(@Nullable IAccount activeAccount) {
                // You can use the account data to update your UI or your app database.
                mAccount = activeAccount;

                if (mAccount != null) {
                    signOut();//temporary sign out
                } else {
                    //continue login
                    mSingleAccountApp.signIn(SecurityCodeActivity.this, null, getScopes(), getAuthInteractiveCallback());
                }
            }

            @Override
            public void onAccountChanged(@Nullable IAccount priorAccount, @Nullable IAccount currentAccount) {
                if (currentAccount == null) {
                    // Perform a cleanup task as the signed-in account changed.
                    showToastOnSignOut();
                    objUtils.dismissProgressDialog(progressSecurityCode);
                }
            }

            @Override
            public void onError(@NonNull MsalException exception) {
                objUtils.dismissProgressDialog(progressSecurityCode);
                displayError(exception);

            }
        });
    }

    /**
     * Updates UI when app sign out succeeds
     */
    private void showToastOnSignOut() {
        final String signOutText = "Signed Out.";
        Toast.makeText(SecurityCodeActivity.this, signOutText, Toast.LENGTH_SHORT)
                .show();
    }

    /**
     * Callback used for interactive request.
     * If succeeds we use the access token to call the Microsoft Graph.
     * Does not check cache.
     */
    private AuthenticationCallback getAuthInteractiveCallback() {
        progressSecurityCode.show();
        return new AuthenticationCallback() {

            @Override
            public void onSuccess(IAuthenticationResult authenticationResult) {
                /* Successfully got a token, use it to call a protected resource - MSGraph */

                String accessToken = authenticationResult.getAccessToken().toString();
                //String idToken=authenticationResult.getAccount().getClaims().get("id_token").toString();

                Log.d(AppConstants.TAG, "Successfully authenticated");
                Log.i(AppConstants.TAG, "AccessTokne:: " + accessToken);

                /* Update account */
                mAccount = authenticationResult.getAccount();

                /* call graph */
                callGraphAPI(authenticationResult);

                //Get email from access token
                /*String paratmers = null;
                try {
                    paratmers = JWTUtils.decoded(accessToken);
                    String result[] = paratmers.split(":");
                    String windowsEmail = result[0];
                    getTokenApiForWindow(windowsEmail, false);
                } catch (Exception e) {
                    e.printStackTrace();
                    Log.e(AppConstants.TAG, e.getMessage());
                    Toast.makeText(SecurityCodeActivity.this, "Windows token not found", Toast.LENGTH_SHORT).show();
                    objUtils.dismissProgressDialog(progressSecurityCode);
                }*/


            }

            @Override
            public void onError(MsalException exception) {
                /* Failed to acquireToken */
                objUtils.dismissProgressDialog(progressSecurityCode);
                Log.d(AppConstants.TAG, "Authentication failed: " + exception.toString());
                displayError(exception);

                if (exception instanceof MsalClientException) {
                    /* Exception inside MSAL, more info inside MsalError.java */
                } else if (exception instanceof MsalServiceException) {
                    /* Exception when communicating with the STS, likely config issue */
                }
            }

            @Override
            public void onCancel() {
                /* User canceled the authentication */
                Log.d(AppConstants.TAG, "User cancelled login.");
                objUtils.dismissProgressDialog(progressSecurityCode);
            }
        };
    }

    /**
     * Make an HTTP request to obtain MSGraph data
     */
    private void callGraphAPI(final IAuthenticationResult authenticationResult) {

        Log.i(AppConstants.TAG, authenticationResult.getAccessToken());
        Log.i(AppConstants.TAG, authenticationResult.toString());

        final String defaultGraphResourceUrl = MSGraphRequestWrapper.MS_GRAPH_ROOT_ENDPOINT + "v1.0/me";

        MSGraphRequestWrapper.callGraphAPIUsingVolley(
                SecurityCodeActivity.this,
                defaultGraphResourceUrl,
                authenticationResult.getAccessToken(),
                new com.android.volley.Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        /* Successfully called graph, process data and send to UI */
                        Log.d(AppConstants.TAG, "Response: " + response.toString());

                        String email = "";
                        try {
                            email = response.getString("userPrincipalName");
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        getTokenApiForWindow(email, false);
                    }
                },
                new com.android.volley.Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.d(AppConstants.TAG, "Error: " + error.toString());
                        displayError(error);
                        objUtils.dismissProgressDialog(progressSecurityCode);
                    }
                });
    }

    void displayError(@NonNull final Exception exception) {
        Log.e(AppConstants.TAG, exception.toString());
        Toast.makeText(SecurityCodeActivity.this, exception.toString(), Toast.LENGTH_LONG).show();
    }

    void signOut() {
        mSingleAccountApp.signOut(new ISingleAccountPublicClientApplication.SignOutCallback() {
            @Override
            public void onSignOut() {
                mAccount = null;
                //showToastOnSignOut();
                mSingleAccountApp.signIn(SecurityCodeActivity.this, null, getScopes(), getAuthInteractiveCallback());
            }

            @Override
            public void onError(@NonNull MsalException exception) {
                displayError(exception);
            }
        });
    }

    void samlCheckedPreviousLogin(String clientid, String tenantId, String audiance_type) {

        String key = null;
        try {
            key = URLEncoder.encode(sha1Singature.trim(), "utf-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }

        String redirectUri = "msauth://" + packageName + "/" + key;
        Log.i(AppConstants.TAG, "Redirect Url:" + redirectUri);

        //json file create
        String jsonString = getJsonStuffs(clientid, redirectUri, tenantId, audiance_type);
        //String jsonString= getJsonStuffs2(clientid,redirectUri);
        Log.i(AppConstants.TAG, jsonString);

        File folder = new File(String.valueOf(SecurityCodeActivity.this.getExternalFilesDir(Environment.DIRECTORY_DOCUMENTS)));
        folder.mkdirs();
        File configFile = new File(folder, "auth_config_single.json");

        if (configFile.exists()) {
            configFile.delete();
        }
        try {
            configFile.createNewFile();
        } catch (IOException e) {
            e.printStackTrace();
        }

        FileWriter writer = null;
        try {
            writer = new FileWriter(configFile);
            writer.write(jsonString);
            writer.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

        if (configFile.length() > 0) {
            PublicClientApplication.createSingleAccountPublicClientApplication(
                    SecurityCodeActivity.this, configFile
                    , new IPublicClientApplication.ISingleAccountApplicationCreatedListener() {
                        @Override
                        public void onCreated(ISingleAccountPublicClientApplication application) {
                            mSingleAccountApp = application;
                            loadAccount();
                        }

                        @Override
                        public void onError(MsalException exception) {
                            objUtils.dismissProgressDialog(progressSecurityCode);
                            Log.e(AppConstants.TAG, exception.toString());
                            Toast.makeText(SecurityCodeActivity.this, exception.toString(), Toast.LENGTH_LONG).show();
                        }
                    }
            );
        } else
            Toast.makeText(SecurityCodeActivity.this, "Unable to login, Please try again", Toast.LENGTH_LONG).show();


        /*PublicClientApplication.createSingleAccountPublicClientApplication(SecurityCodeActivity.this,
                R.raw.auth_config_single_account,
                new IPublicClientApplication.ISingleAccountApplicationCreatedListener() {
                    @Override
                    public void onCreated(ISingleAccountPublicClientApplication application) {
                        mSingleAccountApp = application;
                        loadAccount();
                    }
                    @Override
                    public void onError(MsalException exception) {
                        Log.e(AppConstants.TAG, exception.toString());
                        Toast.makeText(SecurityCodeActivity.this, exception.toString(), Toast.LENGTH_LONG).show();
                    }
                });*/
    }

    String getJsonStuffs(String clientId, String redirectUrl, String tenant_id, String audiance_type) {
        JSONObject json = new JSONObject();
        try {
            json.put("client_id", clientId);
            json.put("authorization_user_agent", "DEFAULT");
            json.put("redirect_uri", redirectUrl);
            json.put("account_mode", "SINGLE");
            json.put("broker_redirect_uri_registered", true);

            JSONArray ary = new JSONArray();

            JSONObject aryJsonObject = new JSONObject();
            aryJsonObject.put("type", "AAD");

            JSONObject audiance = new JSONObject();
            audiance.put("tenant_id", tenant_id);
            audiance.put("type", audiance_type);

            //aryJsonObject.put("authority_url", "https://login.microsoftonline.com/"+tenant_id+"/adminconsent?client_id="+clientId);
            //aryJsonObject.put("authority_url", "https://login.microsoftonline.com/"+tenant_id);
            aryJsonObject.put("audience", audiance);

            ary.put(aryJsonObject);

            json.put("authorities", ary);

            return json.toString();

        } catch (JSONException e) {
            e.printStackTrace();
            return null;
        }
    }

    String getJsonStuffs2(String jsonString, String redirectUrl) {

        /*com.cl.cimconlgandroid:host="com.cl.cimconlg"{
            "client_id" : "67e7a32f-f206-401f-9083-13e313caaf87",
                    "authorization_user_agent" : "DEFAULT",
                    "redirect_uri" : "msauth://com.cl.cimconlg/EAghE%2F9t6W144CWlfW9sEe405JM%3D",
                    "authorities" : [
            {
                "type": "AAD",
                    "audience": {
                "type": "AzureADMyOrg",
                        "tenant_id": "c6e54ef3-679a-432d-8d28-b10219f2b7db"
            }
            }
  ]*/

        String[] splitString = jsonString.split("=");
        splitString[1].replace("\"com.cl.cimconlg\"", "");

        Log.i(AppConstants.TAG, "splited json :: " + splitString[1].toString());


        try {
            JSONObject json = new JSONObject(splitString[1]);
            json.put("account_mode", "SINGLE");

            //json.put("client_id", clientId);
            json.put("redirect_uri", redirectUrl);

            return json.toString();

        } catch (JSONException e) {
            e.printStackTrace();
            return null;
        }
    }


    void getSAMLDetail(String clientId) {
        objApi2.getSAMLDetails(clientId, "1", AppConstants.SOURCE).enqueue(new Callback<SAMLDetails>() {
            @Override
            public void onResponse(Call<SAMLDetails> call, Response<SAMLDetails> response) {

                if (response.code() == 200) {

                    SAMLDetails objSamlDetail = response.body();

                    if (objSamlDetail.getStatus().equals("1")) {

                        if (objSamlDetail.getData().getIsSamlSupport().toString().equalsIgnoreCase("true")) {

                            try {
                                String tenantId = objSamlDetail.getData().getTenant_id();
                                String clientId = objSamlDetail.getData().getkClientID();
                                String audiance_type = objSamlDetail.getData().getAudiance_type();
                                Log.i(AppConstants.TAG, "ClientId:" + clientId + "  Tenant uri:" + tenantId);
                                samlCheckedPreviousLogin(clientId, tenantId, audiance_type);
                            } catch (Exception e) {
                                Toast.makeText(SecurityCodeActivity.this, "Wrong response", Toast.LENGTH_SHORT).show();
                            }
                        } else {
                            startActivity(new Intent(SecurityCodeActivity.this, LoginActivity.class));
                            finish();
                            objUtils.dismissProgressDialog(progressSecurityCode);
                        }
                    } else {
                        objUtils.dismissProgressDialog(progressSecurityCode);
                    }
                } else if (response.code() == 404) {
                    startActivity(new Intent(SecurityCodeActivity.this, LoginActivity.class));
                    finish();
                    objUtils.dismissProgressDialog(progressSecurityCode);
                } else {
                    objUtils.dismissProgressDialog(progressSecurityCode);
                    objUtils.responseHandleLogin(SecurityCodeActivity.this, response.code(), response.errorBody());
                }
            }

            @Override
            public void onFailure(Call<SAMLDetails> call, Throwable t) {
                objUtils.dismissProgressDialog(progressSecurityCode);
            }
        });
    }

    void getTokenApiForWindow(final String username, final boolean isTocuhId) {

        progressSecurityCode.show();
        Utils.addLog("Login API username: " + username + " Grant_type: " + GRANT_TYPE_VALUE + " Client_id: " + clientId);
        Utils.addLog("Login API Call Started....");
        objApi2.getTokenByWindowsLogin(GRANT_TYPE_VALUE, username, IS_SECURED, AppConstants.ORIGINS, version, AppConstants.SOURCE, clientId).enqueue(new Callback<LoginMaster>() {
            @Override
            public void onResponse(Call<LoginMaster> call, Response<LoginMaster> response) {

                objUtils.dismissProgressDialog(progressSecurityCode);
                if (response.code() == 200) {
                    Utils.addLog("Login API 200 Response.");
                    if (response.body().getStatus().equalsIgnoreCase("1")) {
                        spf_login.edit().putString(AppConstants.EMAIL, username).apply();

                        LoginMaster objMaster = response.body();

                        SharedPreferences.Editor editor = spf.edit();
                        editor.putString(AppConstants.ACCESS_TOKEN, objMaster.getData().getTokenType() + " " + objMaster.getData().getAccessToken());
                        editor.putString(AppConstants.USERNAME, objMaster.getData().getUserName());
                        editor.putString(AppConstants.PHONE_NUMBER, objMaster.getData().getPhoneNumber());
                        editor.putString(AppConstants.CLIENT_TYPE, objMaster.getData().getClient_type());
                        editor.putString(AppConstants.NAME, objMaster.getData().getUserfullname());
                        editor.putString(AppConstants.DATE_FORMAT, objMaster.getData().getClientDateFormate());
                        editor.putString(AppConstants.GRANT_TYPE, GRANT_TYPE_VALUE);
                        editor.putBoolean(AppConstants.ISLOGGEDIN, true);

                        editor.putString(AppConstants.MINX, String.valueOf(objMaster.getData().getMinx()));
                        editor.putString(AppConstants.MINY, String.valueOf(objMaster.getData().getMiny()));

                        //Client name will display on dashboard
                        editor.putString(AppConstants.CLIENT_NAME_LG,objMaster.getData().getClientName());

                        //spf_login.edit().putString(AppConstants.TEMP_PASS, password).apply();

                        if (isTocuhId) {
                            spf_login.edit().putBoolean(AppConstants.IS_REMMEBER, true).apply();
                        }

                        //temp_pass = password;

                        String event_name = spf.getString(AppConstants.CLIENT_NAME, "") + "_" + objMaster.getData().getUserName() + "_";

                        editor.putString(AppConstants.GLOBAL_EVENT_NAME, event_name);
                        editor.commit();

                        Utils.addLog("Login API Status :1");
                        Utils.addLog(
                                "Login API: ACCESS TOKEN: " + objMaster.getData().getAccessToken() +
                                        " \nUSER NAME: " + objMaster.getData().getUserName() +
                                        " NAME: " + objMaster.getData().getUserfullname() +
                                        " Client typ:" + objMaster.getData().getClient_type() +
                                        " \nDate Format: " + objMaster.getData().getClientDateFormate() +
                                        " MinX: " + objMaster.getData().getMinx() +
                                        " Min Y: " + objMaster.getData().getMiny()
                        );

                        Log.i(AppConstants.TAG, objMaster.getData().getAccessToken());
                        //startActivity(new Intent(LoginActivity.this, MainActivity.class));
                        //finish();

                        Bundle bundle = new Bundle();
                        bundle.putString(FirebaseAnalytics.Param.ITEM_NAME, event_name + "login");
                        mFirebaseAnalytics.logEvent(FirebaseAnalytics.Event.SELECT_CONTENT, bundle);
                        Log.i(AppConstants.TAG, "Event:" + event_name + "login");
                        Intent intent = new Intent(SecurityCodeActivity.this, MainActivity.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP |
                                Intent.FLAG_ACTIVITY_CLEAR_TASK |
                                Intent.FLAG_ACTIVITY_NEW_TASK);
                        startActivity(intent);
                        finish();

                    } else if (response.body().getStatus().equals("-1")) {
                        Utils.dialogForMessagePlayStoreNavigate(SecurityCodeActivity.this, response.body().getMessage().toString());
                    } else {
                        Utils.addLog("Login API Status :0");
                        LoginMaster objMaster = response.body();
                        Utils.dialogForMessage(SecurityCodeActivity.this, objUtils.getStringResourceByName(SecurityCodeActivity.this, objMaster.getMessage()));
                    }
                } else {
                    Utils.addLog("Login API Code: " + response.code() + " Error Msg: " + response.errorBody());
                    objUtils.responseHandleLogin(SecurityCodeActivity.this, response.code(), response.errorBody());
                }
            }

            @Override
            public void onFailure(Call<LoginMaster> call, Throwable t) {
                objUtils.dismissProgressDialog(progressSecurityCode);
                Utils.addLog("LOGIN API OnFailure called: " + t.getMessage().toString());
            }
        });
    }

}