package com.cl.lg.fragment;

import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.cl.lg.LG;
import com.cl.lg.R;
import com.cl.lg.activities.MainActivity;
import com.cl.lg.networking.API;
import com.cl.lg.pojo.GatwayCount.GatewayCount;
import com.cl.lg.pojo.GetFaultySLCCount.Data;
import com.cl.lg.pojo.GetFaultySLCCount.GetFaultySLCCount;
import com.cl.lg.utils.AppConstants;
import com.cl.lg.utils.Utils;
import com.google.firebase.analytics.FirebaseAnalytics;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class GatewayStatus extends Fragment {

    @BindView(R.id.llBackProfile)
    LinearLayout llBackProfile;

    @BindView(R.id.tvConnectedCount)
    TextView tvConnectedCount;

    @BindView(R.id.tvDisConnectedCount)
    TextView tvDisConnectedCount;

    @BindView(R.id.llDisconnected)
    LinearLayout llDisconnected;

    @BindView(R.id.llConnected)
    LinearLayout llConnected;

    @BindView(R.id.llPowerLoss)
    LinearLayout llPowerLoss;

    @BindView(R.id.tvPowerloss)
    TextView tvPowerloss;

    Utils objUtils;

    View view;

    API objApi;
    String token;

    ProgressDialog dialog_wait;
    SharedPreferences spf;

    private FirebaseAnalytics mFirebaseAnalytics;

    String type;
    String typeMessage;

    int searchvisibility = View.GONE;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_gatway_status, null);
        init();
        return view;
    }

    void init() {
        ButterKnife.bind(this, view);
        objUtils = new Utils();

        spf = getActivity().getSharedPreferences(AppConstants.SPF, Context.MODE_PRIVATE);

        mFirebaseAnalytics = FirebaseAnalytics.getInstance(getActivity());
        mFirebaseAnalytics.setCurrentScreen(getActivity(), "gatewayStatus", null /* class override */);

        dialog_wait = new ProgressDialog(getActivity());
        dialog_wait.setMessage(getResources().getString(R.string.please_wait));
        dialog_wait.setCancelable(false);

        type = spf.getString(AppConstants.APP_TYPE, "");
        //typeMessage = spf.getString(AppConstants.APP_TYPE_MSG, "");
        typeMessage = objUtils.getStringResourceByName(getActivity(), spf.getString(AppConstants.APP_TYPE_MSG, ""));

        token = spf.getString(AppConstants.ACCESS_TOKEN, "");
        objApi = new LG().networkCall(getActivity(), false);

        llBackProfile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                objUtils.loadFragment(new MoreFragment(), getActivity());
            }
        });

        getStatusCount();

        ((MainActivity) getActivity()).selectDashboard(false);
        ((MainActivity) getActivity()).selectStatus(false);
        ((MainActivity) getActivity()).selectMap(false);
        ((MainActivity) getActivity()).selectMenu(true);
    }

    void getStatusCount() {
        dialog_wait.show();
        objApi.getGatwayCount(token, AppConstants.IS_SECURED).enqueue(new Callback<GatewayCount>() {
            @Override
            public void onResponse(Call<GatewayCount> call, Response<GatewayCount> response) {
                try {
                    objUtils.dismissProgressDialog(dialog_wait);
                    if (response.code() == 200) {


                        GatewayCount count = response.body();
                        if (count.getStatus().equals("1")) {
                            com.cl.lg.pojo.GatwayCount.Data objData = count.getData();

                            if(objData.getConnected()!=null) {
                                tvConnectedCount.setText(objData.getConnected());
                                if (!objData.getConnected().toString().equals("0"))
                                    llConnected.setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View view) {
                                            if (type.toString().equalsIgnoreCase(AppConstants.APP_TYPE_PAID))
                                                navigate(AppConstants.G_CONNECTED);
                                            else
                                                objUtils.dialogForApptype(getActivity(), typeMessage);
                                        }
                                    });
                            }

                            if(objData.getDisconnected()!=null) {
                                tvDisConnectedCount.setText(objData.getDisconnected());
                                if (!objData.getDisconnected().toString().equals("0"))
                                    llDisconnected.setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View view) {
                                            if (type.toString().equalsIgnoreCase(AppConstants.APP_TYPE_PAID))
                                                navigate(AppConstants.G_DISCONNECTED);
                                            else
                                                objUtils.dialogForApptype(getActivity(), typeMessage);
                                        }
                                    });
                            }

                            if(objData.getPowerLoss()!=null) {
                                tvPowerloss.setText(objData.getPowerLoss());
                                if (!objData.getPowerLoss().toString().equals("0"))
                                    llPowerLoss.setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View view) {
                                            if (type.toString().equalsIgnoreCase(AppConstants.APP_TYPE_PAID))
                                                navigate(AppConstants.G_POWERLOSS);
                                            else
                                                objUtils.dialogForApptype(getActivity(), typeMessage);
                                        }
                                    });
                            }
                        }
                    } else {
                        objUtils.responseHandle(getActivity(), response.code(), response.errorBody());
                    }
                } catch (Exception e) {
                    Utils.dialogForMessage(getActivity(), getResources().getString(R.string.http_error_msg));
                }
            }

            @Override
            public void onFailure(Call<GatewayCount> call, Throwable t) {
                objUtils.dismissProgressDialog(dialog_wait);
                Utils.dialogForMessage(getActivity(), getResources().getString(R.string.http_error_msg));
            }
        });
    }

/*    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.llDisconnected:
                navigate(AppConstants.G_DISCONNECTED);
                break;
            case R.id.llConnected:
                navigate(AppConstants.G_CONNECTED);
                break;
        }
    }*/

    void navigate(String type) {


        String event_name = "";
        if (type.toString().equals(AppConstants.G_CONNECTED)) {
            event_name = spf.getString(AppConstants.GLOBAL_EVENT_NAME, "") + "gatewayConnected";
        } else if (type.toString().equals(AppConstants.G_DISCONNECTED)) {
            event_name = spf.getString(AppConstants.GLOBAL_EVENT_NAME, "") + "gatwayDisconnected";
        }
        Bundle bundleAnalytics = new Bundle();
        bundleAnalytics.putString(FirebaseAnalytics.Param.ITEM_NAME, event_name);
        mFirebaseAnalytics.logEvent(FirebaseAnalytics.Event.SELECT_CONTENT, bundleAnalytics);
        Log.d(AppConstants.TAG, "Event:" + event_name);

        GatewayInventory inventory = new GatewayInventory();
        Bundle objBundle = new Bundle();
        objBundle.putString(AppConstants.GATWAY_TYPE, type);
        objBundle.putInt(AppConstants.IS_SEARCH_ON, searchvisibility);
        inventory.setArguments(objBundle);

        try {
            if (getActivity() != null) {
                // create a FragmentManager
                FragmentManager fm = getActivity().getFragmentManager();
                // create a FragmentTransaction to begin the transaction and replace the Fragment
                FragmentTransaction fragmentTransaction = fm.beginTransaction();
                // replace the FrameLayout with new Fragment
                fragmentTransaction.replace(R.id.frm1, inventory);
                fragmentTransaction.addToBackStack(null);
                //fragmentTransaction.commit(); // save the changes
                fragmentTransaction.commitAllowingStateLoss();

            }
        } catch (Exception e) {
        }
    }
}