package com.cl.lg.fragment;

import android.app.Dialog;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.app.SearchManager;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Rect;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.cl.lg.activities.MainActivity;
import com.google.android.material.textfield.TextInputEditText;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.appcompat.widget.SearchView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.cl.lg.R;
import com.cl.lg.adapters.ListDialogAdapter;
import com.cl.lg.adapters.SlcCommandAdapter;
import com.cl.lg.pojo.DialogList;
import com.cl.lg.pojo.ListResponse.List;
import com.cl.lg.utils.AppConstants;
import com.cl.lg.utils.EndlessRecyclerViewScrollListener;
import com.cl.lg.utils.Utils;
import com.getbase.floatingactionbutton.FloatingActionButton;
import com.google.firebase.analytics.FirebaseAnalytics;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class SLCCommandsFragment extends Fragment implements SlcCommandAdapter.MyCallbackForControl, View.OnClickListener {

    @BindView(R.id.tvNorecordsList)
    TextView tvNorecordsList;

    @BindView(R.id.rvSlcCommand)
    RecyclerView rvSlcCommand;

    @BindView(R.id.btnRefresh)
    ImageView btnRefresh;

    @BindView(R.id.swpRefresshPole)
    SwipeRefreshLayout swpRefresshPole;

    @BindView(R.id.ivFilter)
    ImageView ivFilter;

    @BindView(R.id.llSlcCommands)
    LinearLayout llSlcCommands;

    @BindView(R.id.edtSLCName)
    TextInputEditText edtSLCName;

    @BindView(R.id.edtCommanddname)
    TextInputEditText edtCommanddname;

    @BindView(R.id.edtGroup)
    TextInputEditText edtGroup;

    @BindView(R.id.edtNearMe)
    TextInputEditText edtNearMe;

    @BindView(R.id.edtAddress)
    TextInputEditText edtAddress;

    @BindView(R.id.fabOn)
    FloatingActionButton fabOn;
    @BindView(R.id.fabOff)
    FloatingActionButton fabOff;
    @BindView(R.id.fabDim)
    FloatingActionButton fabDim;

    java.util.List<List> mList;
    SlcCommandAdapter adapter;

    EndlessRecyclerViewScrollListener scrollListener;

    View view;

    Utils objUtils;
    ArrayList<DialogList> filterList;
    FirebaseAnalytics mFirebaseAnalytics;
    SharedPreferences spf;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragement_commands, null);
        init();
        return view;
    }

    void init() {

        ButterKnife.bind(this, view);
        objUtils = new Utils();
        mList = new ArrayList<>();
        filterList = new ArrayList<>();

        spf=getActivity().getSharedPreferences(AppConstants.SPF,Context.MODE_PRIVATE);

        rvSlcCommand.setHasFixedSize(true);
        rvSlcCommand.addItemDecoration(new DividerItemDecoration(getActivity(), LinearLayoutManager.VERTICAL));
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
        rvSlcCommand.setLayoutManager(linearLayoutManager);
        adapter = new SlcCommandAdapter(getActivity(), mList, this);
        rvSlcCommand.addItemDecoration(new DividerItemDecoration(getActivity(), 0));

        scrollListener = new EndlessRecyclerViewScrollListener(linearLayoutManager) {
            @Override
            public void onLoadMore(int page, int totalItemsCount, RecyclerView view) {
                // Triggered only when new data needs to be appended to the list
                // Add whatever code is needed to append new items to the bottom of the list
                Log.d(AppConstants.TAG, "scroll listner call");
                //dummyData();
                Log.i(AppConstants.TAG, "Pg:" + page);
            }
        };
        rvSlcCommand.addOnScrollListener(scrollListener);
        rvSlcCommand.setAdapter(adapter);

        llSlcCommands.setVisibility(View.GONE);

        ivFilter.setOnClickListener(this);
        dummyData();
        edtSLCName.setOnClickListener(this);
        edtAddress.setOnClickListener(this);
        edtNearMe.setOnClickListener(this);
        edtGroup.setOnClickListener(this);
        edtCommanddname.setOnClickListener(this);

        mFirebaseAnalytics = FirebaseAnalytics.getInstance(getActivity());
        mFirebaseAnalytics.setCurrentScreen(getActivity(), "slclist", null /* class override */);

        ((MainActivity)getActivity()).selectDashboard(false);
        ((MainActivity)getActivity()).selectStatus(true);
        ((MainActivity)getActivity()).selectMap(false);
        ((MainActivity)getActivity()).selectMenu(true);

    }

    void dialog_search_list(final ArrayList<DialogList> mlistDialog, final TextInputEditText editText) {
        filterList = mlistDialog;
        final Dialog dialog = new Dialog(getActivity());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);

        dialog.setContentView(R.layout.dialog_serach_list);
        dialog.setCancelable(false);

        TextView dialogButton = dialog.findViewById(R.id.btCancel);
        dialogButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        RecyclerView objRecyclerView = dialog.findViewById(R.id.rvDialog);

        objRecyclerView.setHasFixedSize(true);
        objRecyclerView.addItemDecoration(new DividerItemDecoration(getActivity(), LinearLayoutManager.VERTICAL));
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
        objRecyclerView.setLayoutManager(linearLayoutManager);
        final ListDialogAdapter adapterDialog = new ListDialogAdapter(getActivity(), filterList, new ListDialogAdapter.MyCallbackForControlDialog() {
            @Override
            public void onClickForControl(int position, String slcNo) {
                editText.setText(slcNo);
                dialog.dismiss();
            }
        });
        objRecyclerView.addItemDecoration(new DividerItemDecoration(getActivity(), 0));
        objRecyclerView.setAdapter(adapterDialog);

        androidx.appcompat.widget.SearchView objSearchView = dialog.findViewById(R.id.svSLCs);
        SearchManager searchManager = (SearchManager) getActivity().getSystemService(Context.SEARCH_SERVICE);
        objSearchView.setSearchableInfo(searchManager
                .getSearchableInfo(getActivity().getComponentName()));

        objSearchView.setQueryHint(getResources().getString(R.string.search));
        objSearchView.setMaxWidth(Integer.MAX_VALUE);

        objSearchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String s) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String s) {
                if (s.toString().trim().equalsIgnoreCase("")) {
                    filterList = mlistDialog;
                    adapterDialog.notifyDataSetChanged();
                } else {
                    ArrayList<DialogList> temp = new ArrayList();
                    for (DialogList d : filterList) {
                        //or use .equal(text) with you want equal match
                        //use .toLowerCase() for better matches
                        if (d.getName().contains(s.toString())) {
                            temp.add(d);
                        }
                    }
                    //update recyclerview
                    filterList = temp;
                    adapterDialog.notifyDataSetChanged();
                }
                return false;
            }
        });

        // dialogButton.setOnTouchListener(Util.colorFilter());

        Rect displayRectangle = new Rect();
        Window window = getActivity().getWindow();

        window.getDecorView().getWindowVisibleDisplayFrame(displayRectangle);
        dialog.getWindow().setLayout((int) (displayRectangle.width() * 0.9f), (int) (displayRectangle.height() * 0.9f));
        dialog.show();
    }

    void dummyData() {

        mList.clear();
        for (int i = 0; i < 15; i++) {
            com.cl.lg.pojo.ListResponse.List objList = new com.cl.lg.pojo.ListResponse.List();
            objList.setMacAddress("" + i);
            objList.setCreated("24/3/2019");
            objList.setSlcId("SLC " + i);

            mList.add(objList);
        }

        tvNorecordsList.setVisibility(View.GONE);
        rvSlcCommand.setVisibility(View.VISIBLE);
        adapter.notifyDataSetChanged();

    }


    @Override
    public void onClick(View view) {

        switch (view.getId()) {
            case R.id.edtCommanddname:
                dialog_search_list(dummyData2(), edtCommanddname);
                break;
            case R.id.edtAddress:
                dialog_search_list(dummyData2(), edtAddress);
                break;
            case R.id.edtNearMe:
                dialog_search_list(dummyData2(), edtNearMe);
                break;
            case R.id.edtSLCName:
                dialog_search_list(dummyData2(), edtSLCName);
                break;
            case R.id.edtGroup:
                dialog_search_list(dummyData2(), edtGroup);
                break;
            case R.id.ivFilter:


                Bundle bundle = new Bundle();
                bundle.putString(FirebaseAnalytics.Param.ITEM_ID, "searchStatus");
                mFirebaseAnalytics.logEvent(FirebaseAnalytics.Event.SELECT_CONTENT, bundle);

                Bundle bundle1 = new Bundle();
                bundle1.putString("search_custom", "searchStatus");
                mFirebaseAnalytics.logEvent(FirebaseAnalytics.Event.SELECT_CONTENT, bundle1);

                if (llSlcCommands.getVisibility() == View.GONE) {
                    llSlcCommands.setVisibility(View.VISIBLE);
                    edtSLCName.setText("");
                    edtGroup.setText("");
                    edtNearMe.setText("");
                    edtCommanddname.setText("");
                    edtAddress.setText("");

                } else {
                    llSlcCommands.setVisibility(View.GONE);
                }
                break;
        }
    }

    @Override
    public void onClickForControl(int position, String slcNo) {

        //objUtils.loadFragment(new DetailListFragment(), getActivity());

        try {
            if (getActivity() != null) {
                // create a FragmentManager
                FragmentManager fm = getActivity().getFragmentManager();
                // create a FragmentTransaction to begin the transaction and replace the Fragment
                FragmentTransaction fragmentTransaction = fm.beginTransaction();
                // replace the FrameLayout with new Fragment

                Bundle objBundle = new Bundle();
                objBundle.putString("From", "SLCCommand");

                DetailListFragment fragment = new DetailListFragment();
                fragment.setArguments(objBundle);

                fragmentTransaction.replace(R.id.frm1, fragment);
                fragmentTransaction.addToBackStack(null);
                //fragmentTransaction.commit(); // save the changes
                fragmentTransaction.commitAllowingStateLoss();
            }
        } catch (Exception e) {
        }
    }


    ArrayList<DialogList> dummyData2() {
        filterList.clear();
        for (int i = 0; i < 5; i++) {
            com.cl.lg.pojo.DialogList objList = new com.cl.lg.pojo.DialogList();
            objList.setName("Test" + i);

            filterList.add(objList);
        }
        return filterList;

    }
}
