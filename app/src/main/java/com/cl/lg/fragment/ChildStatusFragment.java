package com.cl.lg.fragment;

import android.app.ProgressDialog;
import android.content.SharedPreferences;
import android.os.Bundle;
import com.google.android.material.textfield.TextInputEditText;
import androidx.fragment.app.Fragment;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.cl.lg.R;
import com.cl.lg.adapters.StatusListAdapter;
import com.cl.lg.networking.API;
import com.cl.lg.pojo.CommonDialogResponse;
import com.cl.lg.pojo.CommonResponseStatusDetailsDialog;
import com.cl.lg.pojo.ListResponse.List;
import com.cl.lg.utils.EndlessRecyclerViewScrollListener;
import com.cl.lg.utils.Utils;

import java.util.ArrayList;

import butterknife.BindView;

public class ChildStatusFragment  extends Fragment {

    @BindView(R.id.tvNorecordsList)
    TextView tvNorecordsList;

    @BindView(R.id.rvStatus)
    RecyclerView rvStatus;

    @BindView(R.id.btnRefresh)
    ImageView btnRefresh;

    @BindView(R.id.swpRefresshPole)
    SwipeRefreshLayout swpRefresshPole;

    @BindView(R.id.ivFilter)
    ImageView ivFilter;

    @BindView(R.id.llStatus)
    LinearLayout llStatus;

    @BindView(R.id.viewStatus)
    View viewStatus;

    @BindView(R.id.ivCommand)
    ImageView ivCommand;

    @BindView(R.id.edtSLCName)
    TextInputEditText edtSLCName;

    @BindView(R.id.edtCommanddname)
    TextInputEditText edtCommanddname;

    @BindView(R.id.edtGroup)
    TextInputEditText edtGroup;

    @BindView(R.id.edtNearMe)
    TextInputEditText edtNearMe;

    @BindView(R.id.edtAddress)
    TextInputEditText edtAddress;

    @BindView(R.id.llSlcCommands)
    LinearLayout llSlcCommands;

    @BindView(R.id.ivBgStatus)
    ImageView ivBgSt;

    java.util.List<List> mList;
    StatusListAdapter adapter;
    EndlessRecyclerViewScrollListener scrollListener;
    View view;


    ArrayList<CommonDialogResponse> mListDialog;
    ArrayList<CommonDialogResponse> filterList;

    ArrayList<com.cl.lg.pojo.SLCStatus.List> mListFinal;
    ArrayList<CommonResponseStatusDetailsDialog> mListDialogStatus;

    Utils objUtils;

    API objApi;
    String token;
    SharedPreferences spf;
    ProgressDialog dialog_wait;

    Double lat = 23.023527;
    Double lng = 72.571478;

    ArrayList<com.cl.lg.pojo.SLCStatus.List> mFinalStatusList;
    String statusParameter;
    String powerPara;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_off, null);
        init();
        return view;
    }

    void init(){

    }
}