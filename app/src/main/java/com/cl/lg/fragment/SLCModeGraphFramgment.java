package com.cl.lg.fragment;

import android.app.Fragment;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Canvas;
import android.os.Bundle;

import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.load.engine.Resource;
import com.cl.lg.R;
import com.cl.lg.activities.MainActivity;
import com.cl.lg.adapters.PieLegendAdapter;
import com.cl.lg.pojo.Legends;
import com.cl.lg.utils.AppConstants;
import com.cl.lg.utils.Utils;
import com.github.mikephil.charting.charts.PieChart;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.PieData;
import com.github.mikephil.charting.data.PieDataSet;
import com.github.mikephil.charting.data.PieEntry;
import com.github.mikephil.charting.formatter.PercentFormatter;
import com.github.mikephil.charting.highlight.Highlight;
import com.github.mikephil.charting.listener.OnChartValueSelectedListener;
import com.google.firebase.analytics.FirebaseAnalytics;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class SLCModeGraphFramgment extends Fragment implements PieLegendAdapter.MyCallbackForControlDialog {

    @BindView(R.id.ivBackLogin)
    ImageView ivBackLogin;

    Utils objUtils;
    View view;

    @BindView(R.id.rvLegends)
    RecyclerView rvLegends;

    @BindView(R.id.pieChart)
    PieChart pieChart1;

    @BindView(R.id.tvTitleGraph)
    TextView tvTitleGraph;

    PieLegendAdapter mAdapter;
    List<Legends> mList;

    SharedPreferences spf;

    float total = 0.0f;
    int photocell, manual, mixed_mode, astro_clock, schedule, astro_clock_with_over;
    int off, on, dim;

    private FirebaseAnalytics mFirebaseAnalytics;

    Bundle bundleAnaltics;
    Bundle objBundle;
    String dashboardType;
    int yes, no, never;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_graph, null);
        init();
        return view;
    }

    void init() {
        ButterKnife.bind(this, view);
        objUtils = new Utils();
        mList = new ArrayList<>();
        objBundle = getArguments();

        if (objBundle != null) {
            dashboardType = objBundle.getString(AppConstants.DASHOBARD_TYPE);
        }

        spf = getActivity().getSharedPreferences(AppConstants.SPF, Context.MODE_PRIVATE);

        mFirebaseAnalytics = FirebaseAnalytics.getInstance(getActivity());
        mFirebaseAnalytics.setCurrentScreen(getActivity(), "graphSLCMode", null /* class override */);

        photocell = Integer.parseInt(spf.getString(AppConstants.photocell, ""));
        manual = Integer.parseInt(spf.getString(AppConstants.Manual, ""));
        mixed_mode = Integer.parseInt(spf.getString(AppConstants.MixedMode, ""));
        astro_clock = Integer.parseInt(spf.getString(AppConstants.AstroClock, ""));
        schedule = Integer.parseInt(spf.getString(AppConstants.Schedule, ""));
        astro_clock_with_over = Integer.parseInt(spf.getString(AppConstants.AstroClockWithOver, ""));

        on = Integer.parseInt(spf.getString(AppConstants.ON, ""));
        off = Integer.parseInt(spf.getString(AppConstants.OFF, ""));
        dim = Integer.parseInt(spf.getString(AppConstants.DIM, ""));

        yes = Integer.parseInt(spf.getString(AppConstants.YES, ""));
        no = Integer.parseInt(spf.getString(AppConstants.NO, ""));
        never = Integer.parseInt(spf.getString(AppConstants.NEVER, ""));


        if (dashboardType.toString().equals(AppConstants.MODE)) {
            total = photocell + manual + mixed_mode + astro_clock + schedule + astro_clock_with_over;
            tvTitleGraph.setText(getResources().getString(R.string.slc_modes));
        } else if (dashboardType.toString().equals(AppConstants.LIGHT_STATUS)) {
            total = on + off + dim;
            tvTitleGraph.setText(getResources().getString(R.string.lamp_status).toUpperCase());
        } else if (dashboardType.toString().equals(AppConstants.SLC_COMM)) {
            total = yes + no + never;
            tvTitleGraph.setText(getResources().getString(R.string.slccomm).toUpperCase());
        }

        rvLegends.setHasFixedSize(true);
        rvLegends.addItemDecoration(new DividerItemDecoration(getActivity(), LinearLayoutManager.VERTICAL) {
            @Override
            public void onDraw(Canvas canvas, RecyclerView parent, RecyclerView.State state) {
                // Do not draw the divider
            }
        });
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
        rvLegends.setLayoutManager(linearLayoutManager);
        mAdapter = new PieLegendAdapter(getActivity(), mList, this, true);
        rvLegends.setAdapter(mAdapter);

        drawGraph();

        ivBackLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                objUtils.loadFragment(new DashboardFragment(), getActivity());
                //getActivity().onBackPressed();
            }
        });

        ((MainActivity) getActivity()).selectDashboard(true);
        ((MainActivity) getActivity()).selectStatus(false);
        ((MainActivity) getActivity()).selectMap(false);
        ((MainActivity) getActivity()).selectMenu(false);
    }

    void chartDataSet(PieChart objPieChart, List<Integer> colors, List<PieEntry> entries) {

        PieDataSet set = new PieDataSet(entries, "");
        set.setSliceSpace(2f);
        set.setDrawValues(true);
        set.setYValuePosition(PieDataSet.ValuePosition.OUTSIDE_SLICE);
        set.setValueLineColor(getResources().getColor(R.color.colorWhite));
        set.setValueLinePart1Length(1.0f);
        set.setValueLinePart2Length(1.35f);
        set.setAutomaticallyDisableSliceSpacing(true);

        set.setValueTextSize(10.0f);
        set.setValueTextColor(getResources().getColor(R.color.colorWhite));
        set.setColors(colors);


        PieData data = new PieData(set);

        //PercentFormatter obj= new PercentFormatter(objPieChart);
        data.setValueFormatter(new PercentFormatter(objPieChart));
        objPieChart.setUsePercentValues(true);
        //data.setValueFormatter(new MyValueFormatter());

        objPieChart.setHoleRadius(0f);
        objPieChart.setCenterTextSize(16);
        objPieChart.setTransparentCircleAlpha(0);

        objPieChart.setCenterTextColor(getResources().getColor(R.color.colorGray));
        objPieChart.setDrawEntryLabels(false);
        objPieChart.setDrawSliceText(false);

        objPieChart.setData(data);
        objPieChart.setHighlightPerTapEnabled(true);

        objPieChart.getLegend().setEnabled(false);
        objPieChart.animateY(700);
        objPieChart.getDescription().setEnabled(false);
        objPieChart.invalidate();
        objPieChart.setClickable(false);
        objPieChart.setRotationEnabled(true);

        objPieChart.setOnChartValueSelectedListener(new OnChartValueSelectedListener() {
            @Override
            public void onValueSelected(Entry e, Highlight h) {
                Log.i("VAL SELECTED", "Value: " + e.getY() + ", index: " + h.getX() + ", DataSet index: ");
                int i = (int) h.getX();
                System.out.println("Position" + i);

                PieEntry objdata = (PieEntry) e;
                String type = objdata.getLabel();
                Log.i("***", type);

                if (dashboardType.toString().equals(AppConstants.MODE)) {
                    if (type.equalsIgnoreCase(AppConstants.AstroClockWithOver)) {
                        objUtils.openDetailFragment(getActivity(), getResources().getString(R.string.astro_clock_with_override), "8&1", "9&=&4", "0", "", true, R.id.tvAstroClockWithOver, false, false);
                    } else if (type.equalsIgnoreCase(AppConstants.Manual)) {
                        objUtils.openDetailFragment(getActivity(), getResources().getString(R.string.manual), "8&1", "9&=&0", "0", "", true, R.id.tvManual, false, false);
                    } else if (type.equalsIgnoreCase(AppConstants.MixedMode)) {
                        objUtils.openDetailFragment(getActivity(), getResources().getString(R.string.mix_mode), "8&1", "9&=&7", "0", "", true, R.id.tvMixedMode, false, false);
                    } else if (type.equalsIgnoreCase(AppConstants.AstroClock)) {
                        objUtils.openDetailFragment(getActivity(), getResources().getString(R.string.astro_clock), "8&1", "9&=&3", "0", "", true, R.id.tvAstroClock, false, false);
                    } else if (type.equalsIgnoreCase(AppConstants.Schedule)) {
                        objUtils.openDetailFragment(getActivity(), getResources().getString(R.string.schedule), "8&1", "9&=&2", "0", "", true, R.id.tvSchedule, false, false);
                    } else if (type.equalsIgnoreCase(AppConstants.photocell)) {
                        objUtils.openDetailFragment(getActivity(), getResources().getString(R.string.photocell), "8&1", "9&=&1", "0", "", true, R.id.tvphotcell, false, false);
                    }
                } else if (dashboardType.toString().equals(AppConstants.LIGHT_STATUS)) {
                    //getResources().getString(R.string.lamp_status)+" - "+
                    if (type.equalsIgnoreCase(AppConstants.ON)) {
                        objUtils.openDetailFragment(getActivity(), getResources().getString(R.string.on), "1&1,8&1", "6&=&0,|6&=&100", "0", "", false, R.id.llon, true, false);
                    } else if (type.equalsIgnoreCase(AppConstants.DIM)) {
                        objUtils.openDetailFragment(getActivity(), getResources().getString(R.string.dim), "1&1,8&1", "6&>&0,6&<&100", "0", "", false, R.id.lldim, true, false);
                    } else if (type.equalsIgnoreCase(AppConstants.OFF)) {
                        objUtils.openDetailFragment(getActivity(), getResources().getString(R.string.off), "1&0,8&1", "", "0", "", false, R.id.lloff, true, false);
                    }
                } else if (dashboardType.toString().equals(AppConstants.SLC_COMM)) {
                    if (type.equalsIgnoreCase(AppConstants.YES)) {
                        objUtils.openDetailFragment(getActivity(), getResources().getString(R.string.slccomm) + " - " + getResources().getString(R.string.yes),
                                "8&1", "", "0", "",
                                true, R.id.llyes, false, false);
                    } else if (type.equalsIgnoreCase(AppConstants.NO)) {
                        objUtils.openDetailFragment(getActivity(), getResources().getString(R.string.slccomm) + " - " + getResources().getString(R.string.no),
                                "8&0", "", "0", "",
                                true, R.id.llno, false, false);
                    } else if (type.equalsIgnoreCase(AppConstants.NEVER)) {
                        objUtils.openDetailFragment(getActivity(), getResources().getString(R.string.slccomm) + " - " + getResources().getString(R.string.never), "", "", "1", "", true, R.id.llnever, false, false);
                    }
                }
            }

            @Override
            public void onNothingSelected() {
            }
        });
    }

    void drawGraph() {
        mList.clear();
        List<Integer> colorList = new ArrayList<>();
        List<PieEntry> pieEntry = new ArrayList<>();

        if (dashboardType.toString().equals(AppConstants.MODE)) {

            Legends objLegends = new Legends();
            objLegends.setName(AppConstants.photocell);
            objLegends.setGraphName(getResources().getString(R.string.photocell));
            objLegends.setDisplayName(getResources().getString(R.string.photocell));
            objLegends.setPer(String.valueOf(photocell));
            objLegends.setPerF(getPer(photocell, total));
            objLegends.setColorCode(getResources().getColor(R.color.colorBlueGraph));
            objLegends.setDrawable(getActivity().getResources().getDrawable(R.drawable.photocell_4_12));
            mList.add(objLegends);

            Legends objLegends4 = new Legends();
            objLegends4.setName(AppConstants.Schedule);
            objLegends4.setGraphName(getResources().getString(R.string.schedule));
            objLegends4.setDisplayName(getResources().getString(R.string.schedule));
            objLegends4.setPer(String.valueOf(schedule));
            objLegends4.setPerF(getPer(schedule, total));
            objLegends4.setColorCode(getActivity().getResources().getColor(R.color.colorGreenGraph));
            objLegends4.setDrawable(getActivity().getResources().getDrawable(R.drawable.shcedule_4_12));
            mList.add(objLegends4);


            Legends objLegends3 = new Legends();
            objLegends3.setName(AppConstants.AstroClockWithOver);
            objLegends3.setGraphName(getResources().getString(R.string.astro_clock_with_override));
            objLegends3.setDisplayName(getResources().getString(R.string.astro_clock_with_override));
            objLegends3.setPer(String.valueOf(astro_clock_with_over));
            objLegends3.setPerF(getPer(astro_clock_with_over, total));
            objLegends3.setColorCode(getActivity().getResources().getColor(R.color.colorOrangeGraph));
            objLegends3.setDrawable(getActivity().getResources().getDrawable(R.drawable.astro_clock_with_override_4_12));
            mList.add(objLegends3);

            Legends objLegends5 = new Legends();
            objLegends5.setName(AppConstants.AstroClock);
            objLegends5.setGraphName(getResources().getString(R.string.astro_clock));
            objLegends5.setDisplayName(getResources().getString(R.string.astro_clock));
            objLegends5.setPerF(getPer(astro_clock, total));
            objLegends5.setPer(String.valueOf(astro_clock));
            objLegends5.setColorCode(getActivity().getResources().getColor(R.color.colorYellowGraph));
            objLegends5.setDrawable(getResources().getDrawable(R.drawable.astro_clock_4_12));
            mList.add(objLegends5);

            Legends objLegends2 = new Legends();
            objLegends2.setName(AppConstants.MixedMode);
            objLegends2.setGraphName(getResources().getString(R.string.mix_mode));
            objLegends2.setDisplayName(getResources().getString(R.string.mix_mode));
            objLegends2.setPer(String.valueOf(mixed_mode));
            objLegends2.setPerF(getPer(mixed_mode, total));
            objLegends2.setColorCode(getResources().getColor(R.color.colorPurpleGraph));
            objLegends2.setDrawable(getActivity().getResources().getDrawable(R.drawable.mix_mode_4_12));
            mList.add(objLegends2);

            Legends objLegends1 = new Legends();
            objLegends1.setName(AppConstants.Manual);
            objLegends1.setDisplayName(getResources().getString(R.string.manual));
            objLegends1.setGraphName(getResources().getString(R.string.manual));
            objLegends1.setPer(String.valueOf(manual));
            objLegends1.setPerF(getPer(manual, total));
            objLegends1.setColorCode(getResources().getColor(R.color.colorGraphManual));
            objLegends1.setDrawable(getActivity().getResources().getDrawable(R.drawable.manual_14_12));
            mList.add(objLegends1);


        } else if (dashboardType.toString().equals(AppConstants.LIGHT_STATUS)) {
            //
            Legends objLegends3 = new Legends();
            objLegends3.setName(AppConstants.ON);
            objLegends3.setDisplayName(getResources().getString(R.string.on));
            objLegends3.setGraphName(getResources().getString(R.string.on));
            objLegends3.setPer(String.valueOf(on));
            objLegends3.setPerF(getPer(on, total));
            objLegends3.setColorCode(getActivity().getResources().getColor(R.color.colorGreenDashboard));
            objLegends3.setDrawable(getActivity().getResources().getDrawable(R.drawable.lamp_on_4_12));
            mList.add(objLegends3);

            Legends objLegends2 = new Legends();
            objLegends2.setName(AppConstants.OFF);
            objLegends2.setDisplayName(getResources().getString(R.string.off));
            objLegends2.setGraphName(getResources().getString(R.string.off));
            objLegends2.setPer(String.valueOf(off));
            objLegends2.setPerF(getPer(off, total));
            objLegends2.setColorCode(getResources().getColor(R.color.colorGrayDashboard));
            objLegends2.setDrawable(getActivity().getResources().getDrawable(R.drawable.lamp_off_marker_29_12_));//lamp_off16_12
            mList.add(objLegends2);

            Legends objLegends1 = new Legends();
            objLegends1.setName(AppConstants.DIM);
            objLegends1.setDisplayName(getResources().getString(R.string.dim));
            objLegends1.setGraphName(getResources().getString(R.string.dim));
            objLegends1.setPer(String.valueOf(dim));
            objLegends1.setPerF(getPer(dim, total));
            objLegends1.setColorCode(getResources().getColor(R.color.colorOrangeDashboard));
            objLegends1.setDrawable(getActivity().getResources().getDrawable(R.drawable.lamp_dim_4_12));
            mList.add(objLegends1);

        }else if (dashboardType.toString().equals(AppConstants.SLC_COMM)) {
            Legends objLegends3 = new Legends();
            objLegends3.setName(AppConstants.YES);
            objLegends3.setDisplayName(getResources().getString(R.string.slccomm)+" - "+getResources().getString(R.string.yes));
            objLegends3.setGraphName(getResources().getString(R.string.yes));
            objLegends3.setPer(String.valueOf(yes));
            objLegends3.setPerF(getPer(yes, total));
            objLegends3.setColorCode(getActivity().getResources().getColor(R.color.colorGreenGraph));
            objLegends3.setDrawable(getActivity().getResources().getDrawable(R.drawable.slc_yes));
            mList.add(objLegends3);

            Legends objLegends2 = new Legends();
            objLegends2.setName(AppConstants.NO);
            objLegends2.setDisplayName(getResources().getString(R.string.slccomm)+" - "+getResources().getString(R.string.no));
            objLegends2.setGraphName(getResources().getString(R.string.no));
            objLegends2.setPer(String.valueOf(no));
            objLegends2.setPerF(getPer(no, total));
            objLegends2.setColorCode(getResources().getColor(R.color.colorGray));
            objLegends2.setDrawable(getActivity().getResources().getDrawable(R.drawable.no_comm_graph_14_12));
            mList.add(objLegends2);

            Legends objLegends1 = new Legends();
            objLegends1.setName(AppConstants.NEVER);
            objLegends1.setDisplayName(getResources().getString(R.string.slccomm)+" - "+getResources().getString(R.string.never));
            objLegends1.setGraphName(getResources().getString(R.string.never));
            objLegends1.setPer(String.valueOf(never));
            objLegends1.setPerF(getPer(never, total));
            objLegends1.setColorCode(getResources().getColor(R.color.colorGrayDashboard));
            objLegends1.setDrawable(getActivity().getResources().getDrawable(R.drawable.never_comm_graph_14_12));
            mList.add(objLegends1);
        }

        mAdapter.notifyDataSetChanged();

        for (int i = 0; i < mList.size(); i++) {

            if (!mList.get(i).getPer().equals("0")) {

                PieEntry entry = new PieEntry(mList.get(i).getPerF(), mList.get(i).getName());
                pieEntry.add(entry);
                colorList.add(mList.get(i).getColorCode());
            }

        }

        chartDataSet(pieChart1, colorList, pieEntry);
    }

    float getPer(int objained, float total) {
        if (total != 0 || total != 0.0) {
            float per = (objained * 100) / total;
            return per;
        }
        return 0.0f;

    }

    @Override
    public void onClickForControl(int position, String slcNo, String displayName) {
        String event_name = "";
        if (slcNo.equalsIgnoreCase(AppConstants.photocell)) {
            event_name = spf.getString(AppConstants.GLOBAL_EVENT_NAME, "") + "Photocell";
            objUtils.openDetailFragment(getActivity(), displayName, "8&1", "9&=&1", "0", "", true, R.id.tvphotcell, false, false);
        } else if (slcNo.equalsIgnoreCase(AppConstants.Manual)) {
            event_name = spf.getString(AppConstants.GLOBAL_EVENT_NAME, "") + "manual";
            objUtils.openDetailFragment(getActivity(), displayName, "8&1", "9&=&0", "0", "", true, R.id.tvManual, false, false);
        } else if (slcNo.equalsIgnoreCase(AppConstants.MixedMode)) {
            event_name = spf.getString(AppConstants.GLOBAL_EVENT_NAME, "") + "mixedMode";
            objUtils.openDetailFragment(getActivity(), displayName, "8&1", "9&=&7", "0", "", true, R.id.tvMixedMode, false, false);
        } else if (slcNo.equalsIgnoreCase(AppConstants.AstroClock)) {
            event_name = spf.getString(AppConstants.GLOBAL_EVENT_NAME, "") + "astroClockOverride";
            objUtils.openDetailFragment(getActivity(), displayName, "8&1", "9&=&3", "0", "", true, R.id.tvAstroClock, false, false);
        } else if (slcNo.equalsIgnoreCase(AppConstants.Schedule)) {
            event_name = spf.getString(AppConstants.GLOBAL_EVENT_NAME, "") + "schedule";
            objUtils.openDetailFragment(getActivity(), displayName, "8&1", "9&=&2", "0", "", true, R.id.tvSchedule, false, false);
        } else if (slcNo.equalsIgnoreCase(AppConstants.AstroClockWithOver)) {
            event_name = spf.getString(AppConstants.GLOBAL_EVENT_NAME, "") + "astroClockOverride";
            objUtils.openDetailFragment(getActivity(), displayName, "8&1", "9&=&4", "0", "", true, R.id.tvAstroClockWithOver, false, false);
        } else if (slcNo.equalsIgnoreCase(AppConstants.ON)) {
            event_name = spf.getString(AppConstants.GLOBAL_EVENT_NAME, "") + "lightOn";
            objUtils.openDetailFragment(getActivity(), displayName, "1&1,8&1", "6&=&0,|6&=&100", "0", "", false, R.id.llon, true, false);
        } else if (slcNo.equalsIgnoreCase(AppConstants.OFF)) {
            event_name = spf.getString(AppConstants.GLOBAL_EVENT_NAME, "") + "lightOff";
            objUtils.openDetailFragment(getActivity(), displayName, "1&0,8&1", "", "0", "", false, R.id.lloff, true, false);
        } else if (slcNo.equalsIgnoreCase(AppConstants.DIM)) {
            event_name = spf.getString(AppConstants.GLOBAL_EVENT_NAME, "") + "lightDim";
            objUtils.openDetailFragment(getActivity(), displayName, "1&1,8&1", "6&>&0,6&<&100", "0", "", false, R.id.lldim, true, false);
        } else if (slcNo.equalsIgnoreCase(AppConstants.YES)) {
            event_name = spf.getString(AppConstants.GLOBAL_EVENT_NAME, "") + "statusYes";
            objUtils.openDetailFragment(getActivity(), displayName, "8&1", "", "0", "", true, R.id.llyes, false, false);
        }else if (slcNo.equalsIgnoreCase(AppConstants.NO)) {
            event_name = spf.getString(AppConstants.GLOBAL_EVENT_NAME, "") + "statusNo";
            objUtils.openDetailFragment(getActivity(), displayName, "8&0", "", "0", "", true, R.id.llno, false, false);
        }else if (slcNo.equalsIgnoreCase(AppConstants.NEVER)) {
            event_name = spf.getString(AppConstants.GLOBAL_EVENT_NAME, "") + "statusNever";
            objUtils.openDetailFragment(getActivity(), displayName, "", "", "1", "", true, R.id.llnever, false, false);
        }
        bundleAnaltics = new Bundle();
        bundleAnaltics.putString(FirebaseAnalytics.Param.ITEM_NAME, event_name);
        mFirebaseAnalytics.logEvent(FirebaseAnalytics.Event.SELECT_CONTENT, bundleAnaltics);
        Log.d(AppConstants.TAG, "Event:" + event_name);
    }
}