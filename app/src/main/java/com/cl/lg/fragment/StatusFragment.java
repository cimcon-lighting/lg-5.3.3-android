package com.cl.lg.fragment;

import android.Manifest;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.Dialog;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.app.KeyguardManager;
import android.app.ProgressDialog;
import android.app.SearchManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Canvas;
import android.graphics.Rect;
import android.graphics.drawable.ColorDrawable;
import android.hardware.fingerprint.FingerprintManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.security.keystore.KeyGenParameterSpec;
import android.security.keystore.KeyPermanentlyInvalidatedException;
import android.security.keystore.KeyProperties;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;

import com.cl.lg.activities.LoginActivity;
import com.cl.lg.activities.MainActivity;
import com.cl.lg.activities.SecurityCodeActivity;
import com.cl.lg.pojo.ClientType.ClientType;
import com.cl.lg.pojo.ClientType.Datum;
import com.cl.lg.pojo.DashboardCounts.LampType;
import com.cl.lg.pojo.Parameters.ParametersMasters;
import com.cl.lg.pojo.SetMode.DatumSetMode;
import com.google.android.material.textfield.TextInputEditText;

import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import androidx.appcompat.widget.AppCompatCheckBox;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.appcompat.widget.SearchView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.TranslateAnimation;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.cl.lg.LG;
import com.cl.lg.R;
import com.cl.lg.adapters.DialogCommandAdapter;
import com.cl.lg.adapters.ListDialogSentCommandAdapter;
import com.cl.lg.adapters.ListDialogSetModeCommandAdapter;
import com.cl.lg.adapters.ListDialogStatusAdapter;
import com.cl.lg.adapters.StatusListAdapterDynamic;
import com.cl.lg.adapters.TabAdapter;
import com.cl.lg.networking.API;
import com.cl.lg.pojo.Command;
import com.cl.lg.pojo.CommandResponse.CommandResponse;
import com.cl.lg.pojo.CommonDialogResponse;
import com.cl.lg.pojo.CommonResponseStatusDetailsDialog;
import com.cl.lg.pojo.GetGatwaylist.GetGatway;
import com.cl.lg.pojo.GroupName.GroupName;
import com.cl.lg.pojo.ListResponse.List;
import com.cl.lg.pojo.LoginMaster.LoginMaster;
import com.cl.lg.pojo.SLCName.SLCName;
import com.cl.lg.pojo.SLCStatus2.SLCStatusMaster;
import com.cl.lg.pojo.SetMode.SetmodeCmd;
import com.cl.lg.pojo.StatusResponse2;
import com.cl.lg.utils.AppConstants;
import com.cl.lg.utils.DBHelper;
import com.cl.lg.utils.EndlessRecyclerViewScrollListener;
import com.cl.lg.utils.FingerprintHandler;
import com.cl.lg.utils.GPSTracker;
import com.cl.lg.utils.Utils;
import com.getbase.floatingactionbutton.FloatingActionButton;
import com.google.android.gms.maps.model.LatLng;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.math.BigInteger;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.UnrecoverableKeyException;
import java.security.cert.CertificateException;
import java.util.ArrayList;
import java.util.Locale;

import javax.crypto.Cipher;
import javax.crypto.KeyGenerator;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;

import butterknife.BindView;
import butterknife.ButterKnife;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.content.Context.FINGERPRINT_SERVICE;
import static android.content.Context.KEYGUARD_SERVICE;

public class StatusFragment extends Fragment implements
        StatusListAdapterDynamic.MyCallbackForControl,
        View.OnClickListener, FingerprintHandler.OnSuccessAPI {

    @BindView(R.id.tvNorecordsList)
    TextView tvNorecordsList;

    @BindView(R.id.rvStatus)
    RecyclerView rvStatus;

    @BindView(R.id.swpRefresshPole)
    SwipeRefreshLayout swpRefresshPole;

    @BindView(R.id.ivFilter)
    ImageView ivFilter;

    @BindView(R.id.llStatus)
    LinearLayout llStatus;

    @BindView(R.id.viewStatus)
    View viewStatus;

    @BindView(R.id.ivCommand)
    ImageView ivCommand;

    @BindView(R.id.edtSLCName)
    TextInputEditText edtSLCName;

    @BindView(R.id.edtGatwayName)
    TextInputEditText edtGatwayName;

    @BindView(R.id.edtGroup)
    TextInputEditText edtGroup;

    @BindView(R.id.edtNearMe)
    TextInputEditText edtNearMe;

    @BindView(R.id.edtAddress)
    TextInputEditText edtAddress;

    @BindView(R.id.llSlcCommands)
    LinearLayout llSlcCommands;

    @BindView(R.id.ivNearme)
    ImageView ivNearme;

    @BindView(R.id.ll0)
    LinearLayout ll0;
    @BindView(R.id.ll1)
    LinearLayout ll1;
    @BindView(R.id.ll2)
    LinearLayout ll2;
    @BindView(R.id.ll3)
    LinearLayout ll3;
    @BindView(R.id.ll4)
    LinearLayout ll4;
    @BindView(R.id.ll5)
    LinearLayout ll5;
    @BindView(R.id.ll6)
    LinearLayout ll6;
    @BindView(R.id.ll7)
    LinearLayout ll7;

    @BindView(R.id.view0)
    View view0;
    @BindView(R.id.view1)
    View view1;
    @BindView(R.id.view2)
    View view2;
    @BindView(R.id.view3)
    View view3;
    @BindView(R.id.view4)
    View view4;
    @BindView(R.id.view5)
    View view5;
    @BindView(R.id.view6)
    View view6;
    @BindView(R.id.view7)
    View view7;

    @BindView(R.id.tv0)
    TextView tv0;
    @BindView(R.id.tv1)
    TextView tv1;
    @BindView(R.id.tv2)
    TextView tv2;
    @BindView(R.id.tv3)
    TextView tv3;
    @BindView(R.id.tv4)
    TextView tv4;
    @BindView(R.id.tv5)
    TextView tv5;
    @BindView(R.id.tv6)
    TextView tv6;
    @BindView(R.id.tv7)
    TextView tv7;

    @BindView(R.id.btnClear)
    Button btnClear;

    @BindView(R.id.btnSearch)
    Button btnSearch;

    @BindView(R.id.fabMore)
    FloatingActionButton fabMore;

    @BindView(R.id.fabReset)
    FloatingActionButton fabReset;

    @BindView(R.id.fabReadData)
    FloatingActionButton fabReadData;

    @BindView(R.id.fabOn)
    FloatingActionButton fabOn;

    @BindView(R.id.fabOff)
    FloatingActionButton fabOff;

    @BindView(R.id.fabDim)
    FloatingActionButton fabDim;

    @BindView(R.id.fabRoute)
    FloatingActionButton fabRoute;

    @BindView(R.id.llGatway)
    LinearLayout llGatway;

    @BindView(R.id.tvLable)
    TextView tvLable;

    @BindView(R.id.btnNearMe)
    Button btnNearMe;

    @BindView(R.id.right_labels)
    com.getbase.floatingactionbutton.FloatingActionsMenu right_labels;

    @BindView(R.id.cbSelectAll)
    AppCompatCheckBox cbSelectAll;

    @BindView(R.id.tvType)
    TextView tvType;

    @BindView(R.id.frmLampType)
    FrameLayout frmLampType;

    @BindView(R.id.tvNodeType)
    TextView tvNodeType;

    @BindView(R.id.frmNode)
    FrameLayout frmNode;


    @BindView(R.id.btnSentCommand)
    ImageView btnSentCommand;

    @BindView(R.id.progressBarStatus)
    ProgressBar progressBarStatus;

    @BindView(R.id.tvMessage)
    TextView tvMessage;

    @BindView(R.id.tvNodeTypeTitle)
    TextView tvNodeTypeTitle;

    @BindView(R.id.tvLampTypeTitle)
    TextView tvLampTypeTitle;

    java.util.List<List> mList;
    StatusListAdapterDynamic adapter;
    EndlessRecyclerViewScrollListener scrollListener;

    View view;
    boolean isUp;

    ArrayList<CommonDialogResponse> mListDialog;
    ArrayList<CommonDialogResponse> filterList;

    ArrayList<com.cl.lg.pojo.SLCStatus.List> mListFinal;

    ArrayList<StatusResponse2> mListFinal2;
    ArrayList<CommonResponseStatusDetailsDialog> mListDialogStatus;

    Utils objUtils;

    API objApi;
    String token;
    SharedPreferences spf;
    SharedPreferences spf_login;
    ProgressDialog dialog_wait;

    Double lat = 0.0;
    Double lng = 0.0;

    ArrayList<com.cl.lg.pojo.SLCStatus2.List> mFinalStatusList;
    String statusParameter;
    String powerPara;

    String cliet_typ;

    private TabAdapter tabAdapter;
    int totalcount = 0;
    ArrayList<DatumSetMode> mListSetMode = new ArrayList<>();

    final ArrayList<CommonDialogResponse> mListSearch = new ArrayList<>();

    String distance = "";
    //Double distance;

    String slc_id, gatway_id, gp_id2;
    int id = 0;
    Bundle bundle;

    private String[] PERMISSIONS = {
            android.Manifest.permission.ACCESS_FINE_LOCATION,
            android.Manifest.permission.ACCESS_COARSE_LOCATION,
            android.Manifest.permission.ACCESS_NETWORK_STATE
    };
    private static final int PERMISSION_ALL = 1;
    ProgressDialog dialogForLatlong;
    DBHelper db;

    int SearchVisbilitySent = View.GONE;
    int SearchVisibillity = View.GONE;
    boolean isNearMePressNav = false;
    //boolean isFromNearMe = false;

    FingerprintManager fingerprintManager;
    KeyguardManager keyguardManager;

    private KeyStore keyStore;
    // Variable used for storing the key in the Android Keystore container
    private static final String KEY_NAME = "androidLG";
    private Cipher cipher;
    private FirebaseAnalytics mFirebaseAnalytics;

    Bundle bundleAnalytics;
    ArrayList<LampType> mLampType;
    ArrayList<Datum> mClientType;

    String LampTypeId;
    Utils.ClickLampType objClickLampType;
    String lamptypid;
    String nodeTypeid;

    String type;
    String typeMessage;
    String nearmeMsg;
    boolean isZigbeeContains;
    boolean isSingleNodeType;
    boolean isSingleZigbee;

    private static StatusFragment instance = null;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        instance = this;
    }

    public static StatusFragment getInstance() {
        return instance;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        Log.i(AppConstants.TAG, "onCreateView");
        view = inflater.inflate(R.layout.fragement_status, null);
        init();
        return view;
    }

    void init() {

        ButterKnife.bind(this, view);
        //Glide.with(this).load(R.drawable.bg).into(ivBgStatus);
        objUtils = new Utils();

        db = new DBHelper(getActivity());

        objApi = new LG().networkCall(getActivity(), false);
        spf = getActivity().getSharedPreferences(AppConstants.SPF, Context.MODE_PRIVATE);
        spf_login = getActivity().getSharedPreferences(AppConstants.SPF_LOGIN, Context.MODE_PRIVATE);

        type = spf.getString(AppConstants.APP_TYPE, "");
        //typeMessage = spf.getString(AppConstants.APP_TYPE_MSG, "");
        typeMessage = objUtils.getStringResourceByName(getActivity(), spf.getString(AppConstants.APP_TYPE_MSG, ""));

        token = spf.getString(AppConstants.TOKEN_TYPE, "") + " " + spf.getString(AppConstants.ACCESS_TOKEN, "");
        isSingleNodeType = spf.getBoolean(AppConstants.isSingleNodeType, false);
        mLampType = Utils.getLampType(spf);
        mClientType = Utils.getClientTypeList(spf);

        if (isSingleNodeType) {
            frmNode.setVisibility(View.GONE);
            tvNodeTypeTitle.setVisibility(View.GONE);
            tvLampTypeTitle.setText(getString(R.string.lamp_type) + ":");
        } else {
            frmNode.setVisibility(View.VISIBLE);
            tvNodeTypeTitle.setVisibility(View.VISIBLE);
            tvLampTypeTitle.setText(getString(R.string.lamp_type_break));
        }

        lamptypid = spf.getString(AppConstants.SELECTED_LAMP_TYPE_ID, "");

        LampTypeId = spf.getString(AppConstants.SELECTED_LAMP_TYPE_ID, "");
        tvType.setText(spf.getString(AppConstants.SELECTED_LAMP_TYPE, ""));

        String nodeType = spf.getString(AppConstants.SELECTED_NODE_TYPE, getActivity().getResources().getString(R.string.status_all));
        if (nodeType.toString().equalsIgnoreCase("All") || nodeType.toString().equalsIgnoreCase("Todo") || nodeType.toString().equalsIgnoreCase("Todos")) {
            tvNodeType.setText(getActivity().getResources().getString(R.string.status_all));
            spf.edit().putString(AppConstants.SELECTED_NODE_TYPE, getString(R.string.status_all)).apply();
        } else {
            tvNodeType.setText(nodeType);
        }

        mFirebaseAnalytics = FirebaseAnalytics.getInstance(getActivity());
        mFirebaseAnalytics.setCurrentScreen(getActivity(), "status", null);

        cliet_typ = spf.getString(AppConstants.CLIENT_TYPE, "");
        isZigbeeContains = spf.getBoolean(AppConstants.isZigbeeContains, false);
        isSingleZigbee = spf.getBoolean(AppConstants.IS_SINGLE_ZIGBEE, false);
        filterList = new ArrayList<>();
        mFinalStatusList = new ArrayList<>();

        mListFinal = new ArrayList<>();
        mListFinal2 = new ArrayList<>();
        mListDialogStatus = new ArrayList<>();
        mList = new ArrayList<>();
        mListDialog = new ArrayList<>();
        mListSetMode = new ArrayList<DatumSetMode>();
        bundle = getArguments();
        dialogForLatlong = new ProgressDialog(getActivity());
        dialogForLatlong.setMessage(getResources().getString(R.string.please_wait));
        dialogForLatlong.setCancelable(false);

        keyguardManager = (KeyguardManager) getActivity().getSystemService(KEYGUARD_SERVICE);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            fingerprintManager = (FingerprintManager) getActivity().getSystemService(FINGERPRINT_SERVICE);
        } else {
            fingerprintManager = null;
        }

        if (bundle != null) {
            id = bundle.getInt(AppConstants.UI_ID, R.id.ll0);
            SearchVisibillity = bundle.getInt(AppConstants.IS_SEARCH_ON, View.GONE);
            isNearMePressNav = bundle.getBoolean(AppConstants.IS_NEAR_ME, false);
            SearchVisbilitySent = bundle.getInt(AppConstants.IS_SEARCH_ON_SENT_CMD, View.GONE);
        }

        if (SearchVisibillity == View.GONE) {
            llSlcCommands.setVisibility(View.GONE);
            //resetSearchFields();
        } else {
            llSlcCommands.setVisibility(View.VISIBLE);
            edtGatwayName.setText(spf.getString(AppConstants.G_ID_VALUE_NAVE, ""));
            edtSLCName.setText(spf.getString(AppConstants.SLC_ID_VALUE_NAVE, ""));
            edtGroup.setText(spf.getString(AppConstants.GP_ID_VALUE_NAVE, ""));

            slc_id = spf.getString(AppConstants.SLC_ID_NAV, "");
            gatway_id = spf.getString(AppConstants.G_ID_NAV, "");
            gp_id2 = spf.getString(AppConstants.GP_ID_NAV, "");
        }

        nearmeMsg = objUtils.getStringResourceByName(getActivity(), spf.getString(AppConstants.MESSAGE_NEAR_ME, ""));
        Log.i(AppConstants.TAG, "**" + nearmeMsg);

        rvStatus.setHasFixedSize(true);
        rvStatus.addItemDecoration(new DividerItemDecoration(getActivity(), LinearLayoutManager.VERTICAL));
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
        rvStatus.setLayoutManager(linearLayoutManager);
        adapter = new StatusListAdapterDynamic(getActivity(), mListFinal2, this, false, false);
        rvStatus.addItemDecoration(new DividerItemDecoration(getActivity(), 0));

        if (isZigbeeContains) {
            llGatway.setVisibility(View.VISIBLE);
            if (isSingleZigbee)
                tvMessage.setText(getResources().getString(R.string.msg_zigbee));
            else
                tvMessage.setText(getResources().getString(R.string.msg_non_zigbee));
        } else {
            llGatway.setVisibility(View.GONE);
            tvMessage.setText(getResources().getString(R.string.msg_non_zigbee));
        }

        scrollListener = new EndlessRecyclerViewScrollListener(linearLayoutManager) {
            @Override
            public void onLoadMore(int page, int totalItemsCount, RecyclerView view) {
                // Triggered only when new data needs to be appended to the list
                // Add whatever code is needed to append new items to the bottom of the list
                if (mListFinal2.size() < totalcount) {
                    Log.i(AppConstants.TAG, "Pg:" + page);
                    getDataStatus2(page + 1, id, false);
                }
            }
        };

        cbSelectAll.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (b) {
                    for (int i = 0; i < mListFinal2.size(); i++) {
                        mListFinal2.get(i).setChecked(true);
                    }
                } else {
                    for (int i = 0; i < mListFinal2.size(); i++) {
                        mListFinal2.get(i).setChecked(false);
                    }
                }
                adapter.notifyDataSetChanged();
            }
        });

        frmLampType.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String event_name = spf.getString(AppConstants.GLOBAL_EVENT_NAME, "") + "selectLampType";
                Log.d(AppConstants.TAG, "Event:" + event_name);
                Bundle bundleAnalytics = new Bundle();
                bundleAnalytics.putString(FirebaseAnalytics.Param.ITEM_NAME, event_name);
                mFirebaseAnalytics.logEvent(FirebaseAnalytics.Event.SELECT_CONTENT, bundleAnalytics);

                objUtils.ShowLampTypeDialog(mLampType, getResources().getString(R.string.strLampTypTitle), getActivity(), spf, new Utils.ClickLampType() {
                    @Override
                    public void setOnClickLampType(com.cl.lg.pojo.DashboardCounts.LampType objOnClickLampType) {
                        getParamters(objOnClickLampType.getLampTypeID());
                    }
                });
            }
        });


        frmNode.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String event_name = spf.getString(AppConstants.GLOBAL_EVENT_NAME, "") + "selectNodeType";
                Log.d(AppConstants.TAG, "Event:" + event_name);
                Bundle bundleAnalytics = new Bundle();
                bundleAnalytics.putString(FirebaseAnalytics.Param.ITEM_NAME, event_name);
                mFirebaseAnalytics.logEvent(FirebaseAnalytics.Event.SELECT_CONTENT, bundleAnalytics);

                objUtils.ShowNodeTypeDialog(mClientType, getResources().getString(R.string.strNodeTypTitle), getActivity(), spf, new Utils.ClickNodeType() {
                    @Override
                    public void setOnClickNodeType(Datum objOnClickLampType) {
                        getDataStatus2(1, id, false);
                    }
                });
            }
        });

        btnSentCommand.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (type.toString().equalsIgnoreCase(AppConstants.APP_TYPE_PAID)) {
                    //openSentCommand();
                    objUtils.loadFragment(new SentCommandFragment(), getActivity());
                } else
                    objUtils.dialogForApptype(getActivity(), typeMessage);
            }
        });

        rvStatus.addOnScrollListener(scrollListener);
        rvStatus.setAdapter(adapter);

        ivCommand.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //dialog_command(objUtils.getCommands(getActivity()));
                openCommand();
            }
        });

        dialog_wait = new ProgressDialog(getActivity());
        dialog_wait.setMessage(getResources().getString(R.string.please_wait));
        dialog_wait.setCancelable(false);

        if (id == R.id.ll0)
            selectll0();
        else if (id == R.id.ll1)
            selectll1(true);
        else if (id == R.id.ll2)
            selectll2(true);
        else if (id == R.id.ll3)
            selectll3(true);
        else if (id == R.id.ll4)
            selectll4(true);
        else if (id == R.id.ll5)
            selectll5(true);
        else if (id == R.id.ll6)
            selectll6(true);
        else if (id == R.id.ll7)
            selectll7(true);
        else
            selectll0();

        //getDataStatus2(1);
        //swpRefresshPole.setRefreshing(true);

        swpRefresshPole.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        //Do something after 3000ms
                        swpRefresshPole.setRefreshing(false);
                    }
                }, 2500);
                cbSelectAll.setChecked(false);
                resetSearchFields();
                resetNearMe();
                getDataStatus2(1, id, false);

                if (right_labels.isExpanded())
                    right_labels.collapse();

            }
        });

        //dummyData();

        edtSLCName.setOnClickListener(this);
        edtAddress.setOnClickListener(this);
        //edtNearMe.setOnClickListener(this);
        edtGroup.setOnClickListener(this);
        edtGatwayName.setOnClickListener(this);
        ivFilter.setOnClickListener(this);
        ivNearme.setOnClickListener(this);

        ll0.setOnClickListener(this);
        ll1.setOnClickListener(this);
        ll2.setOnClickListener(this);
        ll3.setOnClickListener(this);
        ll4.setOnClickListener(this);
        ll5.setOnClickListener(this);
        ll6.setOnClickListener(this);

        //its a neverCommunicated and we are not to display any details
        ll7.setOnClickListener(this);

        btnClear.setOnClickListener(this);
        btnSearch.setOnClickListener(this);
        btnNearMe.setOnClickListener(this);

        fabRoute.setOnClickListener(this);
        fabMore.setOnClickListener(this);
        fabReset.setOnClickListener(this);
        fabDim.setOnClickListener(this);
        fabOff.setOnClickListener(this);
        fabOn.setOnClickListener(this);
        fabReadData.setOnClickListener(this);

        /*tabAdapter = new TabAdapter(getActivity().getfr(), getActivity());
        tabAdapter.addFragment(new ChildStatusFragment(),getResources().getDrawable(R.drawable.light_off));
        tabAdapter.addFragment(new ChildStatusFragment(),getResources().getDrawable(R.drawable.light_on));
        tabAdapter.addFragment(new ChildStatusFragment(),getResources().getDrawable(R.drawable.off));
        viewPager.setAdapter(tabAdapter);*/

       /* for (int i = 0; i < tabLayout.getTabCount(); i++) {
            TabLayout.Tab tab = tabLayout.getTabAt(i);
            assert tab != null;
            tab.setCustomView(null);
            tab.setCustomView(tabAdapter.getTabView(i));
        }
        tabLayout.setupWithViewPager(viewPager);*/

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            request6plus();
        } else {
            getLocation();
        }

       /* if (isNearMePressNav) {
            nearMe();
        } else {
            resetNearMe();
            getDataStatus2(1, id, false);
        }*/

        ((MainActivity) getActivity()).selectDashboard(false);
        ((MainActivity) getActivity()).selectStatus(true);
        ((MainActivity) getActivity()).selectMap(false);
        ((MainActivity) getActivity()).selectMenu(false);
    }

    void openSentCommand() {
        Bundle mBundle = new Bundle();
        mBundle.putInt(AppConstants.UI_ID, id);
        mBundle.putInt(AppConstants.IS_SEARCH_ON, llSlcCommands.getVisibility());
        mBundle.putBoolean(AppConstants.IS_NEAR_ME, isNearMePressNav);
        mBundle.putInt(AppConstants.IS_SEARCH_ON_SENT_CMD, View.GONE);
        SentCommandFragment fragment = new SentCommandFragment();
        fragment.setArguments(mBundle);
        objUtils.loadFragment(fragment, getActivity());
    }

    void getParamters(String lampTypeId) {
        dialog_wait.show();
        objApi.getParametersApi(token, lampTypeId, AppConstants.IS_SECURED).enqueue(new Callback<ParametersMasters>() {
            @Override
            public void onResponse(Call<ParametersMasters> call, Response<ParametersMasters> response) {
                //objUtils.dismissProgressDialog(dialog_wait);
                Log.i(AppConstants.TAG, "---*-");
                if (response.code() == 200) {
                    Log.i(AppConstants.TAG, "---*-");
                    ParametersMasters objParametersMasters = response.body();
                    java.util.List<com.cl.lg.pojo.Parameters.Datum> objData = objParametersMasters.getData();

                    if (objParametersMasters.getStatus().equalsIgnoreCase("1")) {
                        try {
                            Log.i(AppConstants.TAG, "---*-");
                            //Log.i(AppConstants.TAG, "parameters:" + objData.getParameters().toString());
                            Utils.SaveArraylistInSPF(spf, objData);
                        } catch (Exception e) {
                            Utils.SaveArraylistInSPF(spf, new ArrayList<>());
                            Log.i(AppConstants.TAG, "EEEE");
                        }

                        getDataStatus2(1, id, true);
                    }
                } else {
                    objUtils.responseHandle(getActivity(), response.code(), response.errorBody());
                    objUtils.dismissProgressDialog(dialog_wait);
                }
            }

            @Override
            public void onFailure(Call<ParametersMasters> call, Throwable t) {
                objUtils.dismissProgressDialog(dialog_wait);
                Log.i(AppConstants.TAG, "EEEEFail");

            }
        });

    }

    @Override
    public void onResume() {
        super.onResume();
        Log.i(AppConstants.TAG, "onResume");
        if (right_labels.isExpanded())
            right_labels.collapse();

        if (cbSelectAll.isChecked())
            cbSelectAll.setChecked(false);
        ((MainActivity) getActivity()).selectStatus(true);
    }

    void dialog_password(final int cmdType, boolean isFromSetMode) {
        try {
            final Dialog dialog = new Dialog(getActivity());
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.getWindow().clearFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);
            dialog.setContentView(R.layout.dialog_password);
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));

            final EditText edtPassword = dialog.findViewById(R.id.edtPassword);

            Button btnOk = dialog.findViewById(R.id.btnOk);
            btnOk.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (edtPassword.getText().toString().equals("")) {
                        //edtPassword.setError(getResources().getString(R.string.empty_password));
                        Utils.dialogForMessage(getActivity(), getResources().getString(R.string.empty_password));
                    } else {
                        if (Utils.isInternetAvailable(getActivity())) {
                            getTokenApi(edtPassword.getText().toString(), cmdType, dialog, isFromSetMode);
                        } else {
                            Utils.dialogForMessage(getActivity(), getResources().getString(R.string.no_internet_connection));
                        }
                    }
                }
            });

            Button btnCancel = dialog.findViewById(R.id.btnCancel);
            btnCancel.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    dialog.dismiss();
                }
            });


            Rect displayRectangle = new Rect();
            Window window = getActivity().getWindow();
            window.getDecorView().getWindowVisibleDisplayFrame(displayRectangle);
            dialog.getWindow().setLayout((int) (displayRectangle.width() * 0.9f), (int) (displayRectangle.height() * 0.8f));

            dialog.show();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    void getTokenApi(final String password, final int cmdType, final Dialog dialog, boolean isfromSetMode) {

        String username = spf_login.getString(AppConstants.EMAIL, "");
        String grant_type = spf.getString(AppConstants.GRANT_TYPE, "");
        String clientId = spf.getString(AppConstants.CLIENT_ID, "");

        dialog_wait.show();
        Utils.addLog("Login API username: " + username + " Password: " + password + " Grant_type: " + grant_type + "Client_id: " + clientId);
        Utils.addLog("Login API Call Started....");
        objApi.getToken(grant_type, username, clientId, password, AppConstants.IS_SECURED).enqueue(new Callback<LoginMaster>() {
            @Override
            public void onResponse(Call<LoginMaster> call, Response<LoginMaster> response) {

                objUtils.dismissProgressDialog(dialog_wait);

                if (response.code() == 200) {
                    Utils.addLog("Login API 200 Response.");
                    if (response.body().getStatus().equalsIgnoreCase("1")) {

                        dialog.dismiss();
                        LoginMaster objMaster = response.body();

                        SharedPreferences.Editor editor = spf.edit();
                        editor.putString(AppConstants.ACCESS_TOKEN, objMaster.getData().getTokenType() + " " + objMaster.getData().getAccessToken());
                        editor.putString(AppConstants.USERNAME, objMaster.getData().getUserName());
                        editor.putString(AppConstants.PHONE_NUMBER, objMaster.getData().getPhoneNumber());
                        editor.putString(AppConstants.CLIENT_TYPE, objMaster.getData().getClient_type());
                        editor.putString(AppConstants.NAME, objMaster.getData().getUserfullname());
                        editor.putString(AppConstants.DATE_FORMAT, objMaster.getData().getClientDateFormate());

                        editor.putString(AppConstants.MINX, String.valueOf(objMaster.getData().getMinx()));
                        editor.putString(AppConstants.MINY, String.valueOf(objMaster.getData().getMiny()));
                        editor.apply();

                        Log.i(AppConstants.TAG, objMaster.getData().getAccessToken());

                        Utils.addLog("Login API - from CMD Status :1");

                        Utils.addLog("Login API  - from CMD : ACCESS TOKEN: " + objMaster.getData().getAccessToken() +
                                " \nUSER NAME: " + objMaster.getData().getUserName() +
                                " NAME: " + objMaster.getData().getUserfullname() +
                                " Client typ:" + objMaster.getData().getClient_type() +
                                " \nDate Format: " + objMaster.getData().getClientDateFormate() +
                                " MinX: " + objMaster.getData().getMinx() +
                                " Min Y: " + objMaster.getData().getMiny()
                        );
                        //fire cmd

                        if (cmdType == R.id.fabDim) {
                            dialog_dim(0);
                        } else if (cmdType == R.id.fabOff) {
                            fireCommand(AppConstants.SWITCH_OFF, getCheckedSLC(), 0, 0);
                        } else if (cmdType == R.id.fabOn) {
                            fireCommand(AppConstants.SWITCH_ON, getCheckedSLC(), 0, 0);
                        } else if (isfromSetMode) {
                            fireCommand(AppConstants.SET_MODE, getCheckedSLC(), 0, cmdType);
                        }

                        //Toast.makeText(getActivity(), "Login Successfully !", Toast.LENGTH_SHORT).show();

                    } else if (response.body().getStatus().toString().equalsIgnoreCase("-1")) {
                        //Util.dialogForMessage(LoginActivity.this, objCommonResponse.getMsg().toString());
                        Utils.dialogForMessagePlayStoreNavigate(getActivity(), response.body().getMessage().toString());

                    } else {
                        Utils.addLog("Login API Status :0");
                        LoginMaster objMaster = response.body();
                        Utils.dialogForMessage(getActivity(), objUtils.getStringResourceByName(getActivity(), objMaster.getMessage()));
                    }
                } else {
                    Utils.addLog("Login API Code: " + response.code() + " Error Msg: " + response.errorBody());
                    objUtils.responseHandle(getActivity(), response.code(), response.errorBody());
                }
            }

            @Override
            public void onFailure(Call<LoginMaster> call, Throwable t) {
                objUtils.dismissProgressDialog(dialog_wait);
                Utils.addLog("LOGIN API OnFailure called: " + t.getMessage().toString());
            }
        });
    }

    void dialogForMessageRefreshData(Activity activity, String message, String title) {
        if (activity != null) {
            androidx.appcompat.app.AlertDialog.Builder builder = new androidx.appcompat.app.AlertDialog.Builder(activity);

            builder.setTitle(title)
                    .setMessage(message)
                    .setCancelable(true)
                    .setNegativeButton(getResources().getString(R.string.ok), new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            dialogInterface.dismiss();
                            //resetSearchFields();
                            //resetNearMe();
                            if (right_labels.isExpanded())
                                right_labels.collapse();

                            if (cbSelectAll.isChecked())
                                cbSelectAll.setChecked(false);

                            //getDataStatus2(1, id);
                        }
                    })
                    .setPositiveButton(getResources().getString(R.string.go), new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            //openSentCommand();
                            objUtils.loadFragment(new SentCommandFragment(), getActivity());
                        }
                    });


            //Creating dialog box
            androidx.appcompat.app.AlertDialog alert = builder.create();
            //Setting the title manually
            alert.requestWindowFeature(Window.FEATURE_NO_TITLE);
            alert.show();
        }
    }

    boolean isChecked() {
        boolean isChecked = false;
        int k = 0;
        dialog_wait.show();
        for (int i = 0; i < mListFinal2.size(); i++) {
            if (mListFinal2.get(i).isChecked())
                k++;
        }

        objUtils.dismissProgressDialog(dialog_wait);
        if (k == 0) {
            isChecked = false;
            Utils.dialogForMessageWithTitle(getActivity(), getResources().getString(R.string.select_at_least_slc), getResources().getString(R.string.app_name));
        } else {
            isChecked = true;
        }
        return isChecked;
    }

    void selectll0() {
        cbSelectAll.setChecked(false);
        view0.setVisibility(View.VISIBLE);
        view1.setVisibility(View.INVISIBLE);
        view2.setVisibility(View.INVISIBLE);
        view3.setVisibility(View.INVISIBLE);
        view4.setVisibility(View.INVISIBLE);
        view5.setVisibility(View.INVISIBLE);
        view6.setVisibility(View.INVISIBLE);
        view7.setVisibility(View.INVISIBLE);
        statusParameter = ""; //first tab
        powerPara = "";
        tvLable.setText(getResources().getString(R.string.status_all).toUpperCase());
        id = R.id.ll0;
    }

    void selectll1(boolean flag) {
        cbSelectAll.setChecked(false);
        view0.setVisibility(View.INVISIBLE);
        view1.setVisibility(View.VISIBLE);
        view2.setVisibility(View.INVISIBLE);
        view3.setVisibility(View.INVISIBLE);
        view4.setVisibility(View.INVISIBLE);
        view5.setVisibility(View.INVISIBLE);
        view6.setVisibility(View.INVISIBLE);
        view7.setVisibility(View.INVISIBLE);
        statusParameter = AppConstants.COMMUNICATION_FAULT; //first tab
        powerPara = "";
        tvLable.setText(getResources().getString(R.string.comm_fault).toUpperCase());
        id = R.id.ll1;
    }

    void selectll2(boolean flag) {
        cbSelectAll.setChecked(false);
        view0.setVisibility(View.INVISIBLE);
        view1.setVisibility(View.INVISIBLE);
        view2.setVisibility(View.VISIBLE);
        view3.setVisibility(View.INVISIBLE);
        view4.setVisibility(View.INVISIBLE);
        view5.setVisibility(View.INVISIBLE);
        view6.setVisibility(View.INVISIBLE);
        view7.setVisibility(View.INVISIBLE);
        statusParameter = AppConstants.DRIVER_FAULT; //first tab
        powerPara = "";
        tvLable.setText(getResources().getString(R.string.driver_fault).toUpperCase());
        id = R.id.ll2;
    }

    void selectll3(boolean flag) {
        cbSelectAll.setChecked(false);
        view0.setVisibility(View.INVISIBLE);
        view1.setVisibility(View.INVISIBLE);
        view2.setVisibility(View.INVISIBLE);
        view3.setVisibility(View.VISIBLE);
        view4.setVisibility(View.INVISIBLE);
        view5.setVisibility(View.INVISIBLE);
        view6.setVisibility(View.INVISIBLE);
        view7.setVisibility(View.INVISIBLE);
        statusParameter = AppConstants.PHOTO_CELL_STATUS_FAULT; //first tab
        powerPara = "";
        tvLable.setText(getResources().getString(R.string.photo_cell).toUpperCase());
        id = R.id.ll3;
    }

    void selectll4(boolean flag) {
        cbSelectAll.setChecked(false);
        view0.setVisibility(View.INVISIBLE);
        view1.setVisibility(View.INVISIBLE);
        view2.setVisibility(View.INVISIBLE);
        view3.setVisibility(View.INVISIBLE);
        view4.setVisibility(View.VISIBLE);
        view5.setVisibility(View.INVISIBLE);
        view6.setVisibility(View.INVISIBLE);
        view7.setVisibility(View.INVISIBLE);
        statusParameter = AppConstants.STATUS_PARA_LAMP_ON; //first tab
        powerPara = AppConstants.LAMP_ON;
        tvLable.setText(getResources().getString(R.string.lamp_on).toUpperCase());
        id = R.id.ll4;
    }

    void selectll5(boolean flag) {
        cbSelectAll.setChecked(false);
        view0.setVisibility(View.INVISIBLE);
        view1.setVisibility(View.INVISIBLE);
        view2.setVisibility(View.INVISIBLE);
        view3.setVisibility(View.INVISIBLE);
        view4.setVisibility(View.INVISIBLE);
        view5.setVisibility(View.VISIBLE);
        view6.setVisibility(View.INVISIBLE);
        view7.setVisibility(View.INVISIBLE);
        powerPara = AppConstants.LAMP_DIM;
        statusParameter = AppConstants.STATUS_PARA_LAMP_DIM;
        tvLable.setText(getResources().getString(R.string.lamp_dim).toUpperCase());
        id = R.id.ll5;
    }

    void selectll6(boolean flag) {
        cbSelectAll.setChecked(false);
        view0.setVisibility(View.INVISIBLE);
        view1.setVisibility(View.INVISIBLE);
        view2.setVisibility(View.INVISIBLE);
        view3.setVisibility(View.INVISIBLE);
        view4.setVisibility(View.INVISIBLE);
        view5.setVisibility(View.INVISIBLE);
        view6.setVisibility(View.VISIBLE);
        view7.setVisibility(View.INVISIBLE);
        statusParameter = AppConstants.LAMP_OFF; //first tab
        powerPara = "";
        tvLable.setText(getResources().getString(R.string.lamp_off).toUpperCase());
        id = R.id.ll6;
    }

    void selectll7(boolean flag) {
        cbSelectAll.setChecked(false);
        view0.setVisibility(View.INVISIBLE);
        view1.setVisibility(View.INVISIBLE);
        view2.setVisibility(View.INVISIBLE);
        view3.setVisibility(View.INVISIBLE);
        view4.setVisibility(View.INVISIBLE);
        view5.setVisibility(View.INVISIBLE);
        view6.setVisibility(View.INVISIBLE);
        view7.setVisibility(View.VISIBLE);
        statusParameter = AppConstants.PHOTO_CELL_STATUS_FAULT;
        statusParameter = "";
        powerPara = "";
        tvLable.setText(getResources().getString(R.string.planning).toUpperCase());
        id = R.id.ll7;
    }

    void resetNearMe() {
        //ivNearme.setImageDrawable(getResources().getDrawable(R.drawable.arrow_white));
        ivNearme.setColorFilter(ContextCompat.getColor(getActivity(), R.color.colorWhite));
        isNearMePressNav = false;

        lat = 0.0;
        lng = 0.0;
        distance = "";
        spf.edit().remove(AppConstants.DISTANCE_NAV).apply();
        spf.edit().putBoolean(AppConstants.IS_NEAR_ME, false).apply();
    }

    void resetSearchFields() {
        edtSLCName.setText("");
        edtGroup.setText("");
        edtGatwayName.setText("");
        edtNearMe.setText("");
        edtAddress.setText("");

        //tvLable.setText(getResources().getString(R.string.comm_fault).toUpperCase());

        slc_id = "";
        gatway_id = "";
        gp_id2 = "";

        SharedPreferences.Editor editor = spf.edit();
        editor.remove(AppConstants.SLC_ID_VALUE_NAVE);
        editor.remove(AppConstants.SLC_ID_NAV);

        editor.remove(AppConstants.G_ID_NAV);
        editor.remove(AppConstants.G_ID_VALUE_NAVE);

        editor.remove(AppConstants.GP_ID_NAV);
        editor.remove(AppConstants.GP_ID_VALUE_NAVE);

        editor.remove(AppConstants.IS_SEARCH_PRESSED_STATUS);
        editor.apply();

    }

    String latStr, lngStr;

    void getDataStatus(int pg) {

        if (Utils.isInternetAvailable(getActivity())) {


            if (pg == 1) {
                mFinalStatusList.clear();
                mListDialogStatus.clear();
                adapter.notifyDataSetChanged();
            }

            String blank = "";
            dialog_wait.show();
            String jsonfinal = null;

            if (lat == 0.0)
                latStr = "";
            else
                latStr = String.format("%.4f", lat);

            if (lng == 0.0)
                lngStr = "";
            else
                lngStr = String.format("%.4f", lng);

            try {
                String jsonStr = "{" +
                        "\"gId\":\"" + gatway_id + "\"," +
                        "\"slcId\":\"" + slc_id + "\"," +
                        "\"slcGroup\":\"" + gp_id2 + "\"," +
                        "\"powerPara\":\"" + powerPara + "\"," +
                        "\"statusPara\":\"" + statusParameter + "\"," +
                        "\"Distance\":\"" + distance + "\"," +
                        "\"Longitude\":\"" + lngStr + "\"," +
                        "\"Latitude\":\"" + latStr + "\"," +

                        "}";

                JSONObject jsonObj = new JSONObject(jsonStr);
                jsonfinal = jsonObj.toString();
                Log.i(AppConstants.TAG, "jsonFinal: " + jsonfinal);

            } catch (Exception e) {
            }

            RequestBody body = RequestBody.create(okhttp3.MediaType.parse("application/json; charset=utf-8"), jsonfinal);

            int lampTypeId;
            lampTypeId = Integer.parseInt(spf.getString(AppConstants.SELECTED_LAMP_TYPE_ID, ""));

            objApi.getSLCStatusList(token, String.valueOf(pg), "20", lampTypeId, body, AppConstants.IS_SECURED).enqueue(new Callback<SLCStatusMaster>() {
                @Override
                public void onResponse(Call<SLCStatusMaster> call, Response<SLCStatusMaster> response) {
                    try {
                        objUtils.dismissProgressDialog(dialog_wait);
                        if (response.code() == 200) {
                            SLCStatusMaster objMaster = response.body();
                            if (objMaster.getStatus().equalsIgnoreCase("1")) {
                                if (objMaster.getData().getList().size() != 0) {
                                    totalcount = Integer.parseInt(objMaster.getData().getTotalRecords());
                                }

                                java.util.List<com.cl.lg.pojo.SLCStatus2.List> objList = objMaster.getData().getList();
                                final com.cl.lg.pojo.SLCStatus2.FilterCounts filterCount = objMaster.getData().getFilterCounts();

                                getActivity().runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        tv0.setText(totalcount);
                                        tv1.setText(String.valueOf(filterCount.getCommFaults()));
                                        tv2.setText(String.valueOf(filterCount.getDriverFault()));
                                        tv3.setText(String.valueOf(filterCount.getPhotoCellFault()));
                                        tv4.setText(String.valueOf(filterCount.getControllerOn()));
                                        tv5.setText(String.valueOf(filterCount.getControllerDim()));
                                        tv6.setText(String.valueOf(filterCount.getControllerOff()));
                                        tv7.setText(String.valueOf(filterCount.getNeverComminucated()));
                                    }
                                });

                                if (objList.size() != 0) {
                                    tvNorecordsList.setVisibility(View.GONE);
                                    rvStatus.setVisibility(View.VISIBLE);
                                    for (int i = 0; i < objList.size(); i++) {
                                        com.cl.lg.pojo.SLCStatus2.List objListInner = objList.get(i);
                                        String jsonInner = new Gson().toJson(objListInner);
                                        objListInner.setJsonString(jsonInner);
                                        //Log.d(AppConstants.TAG, jsonInner);
                                        //mListFinal.add(objListInner);
                                        mFinalStatusList.add(objListInner);
                                    }
                                    adapter.notifyDataSetChanged();
                                } else {
                                    tvNorecordsList.setVisibility(View.VISIBLE);
                                    if (isNearMePressNav) {

                                        tvNorecordsList.setText(nearmeMsg);
                                        //tvNorecordsList.setText(getResources().getString(R.string.no_data_record_near_me));
                                    } else {
                                        tvNorecordsList.setText(getResources().getString(R.string.no_data));
                                    }
                                    rvStatus.setVisibility(View.GONE);
                                }
                            } else
                                Utils.dialogForMessage(getActivity(), objUtils.getStringResourceByName(getActivity(), response.body().getMessage().toString()));
                        } else
                            objUtils.responseHandle(getActivity(), response.code(), response.errorBody());

                    } catch (Exception e) {
                    }
                }

                @Override
                public void onFailure(Call<SLCStatusMaster> call, Throwable t) {
                    objUtils.dismissProgressDialog(dialog_wait);
                }
            });
        } else {
            Utils.dialogForMessage(getActivity(), getResources().getString(R.string.no_internet_connection));
        }
    }

    int totalcount_all = 0;

    void getDataStatus2(int pg, final int ll_id, boolean isFromLampStatus) {

        if (Utils.isInternetAvailable(getActivity())) {

            if (pg == 1) {
                mListFinal2.clear();
                mListDialogStatus.clear();
                adapter.notifyDataSetChanged();
                if (!isFromLampStatus) {
                    dialog_wait.show();
                }
                progressBarStatus.setVisibility(View.GONE);
            } else
                progressBarStatus.setVisibility(View.VISIBLE);

            String blank = "";


            String jsonfinal = null;

            if (lat == 0.0)
                latStr = "";
            else
                latStr = String.format(Locale.ENGLISH,"%.4f", lat);

            if (lng == 0.0)
                lngStr = "";
            else
                lngStr = String.format(Locale.ENGLISH,"%.4f", lng);

            String lampTypeId = spf.getString(AppConstants.SELECTED_LAMP_TYPE_ID, "");

            String nodeTypeId;
            if (isSingleNodeType)
                nodeTypeId = "";
            else
                nodeTypeId = spf.getString(AppConstants.SELECTED_NODE_TYPE_ID, "");

            tvType.setText(spf.getString(AppConstants.SELECTED_LAMP_TYPE, ""));


            String nodeType = spf.getString(AppConstants.SELECTED_NODE_TYPE, getActivity().getResources().getString(R.string.status_all));
            if (nodeType.toString().equalsIgnoreCase("All") || nodeType.toString().equalsIgnoreCase("Todo") || nodeType.toString().equalsIgnoreCase("Todos")) {
                tvNodeType.setText(getActivity().getResources().getString(R.string.status_all));
                spf.edit().putString(AppConstants.SELECTED_NODE_TYPE, tvNodeType.getText().toString()).apply();
            } else {
                tvNodeType.setText(spf.getString(AppConstants.SELECTED_NODE_TYPE, nodeType));
            }


            try {
               /* String jsonStr;
                if (ll_id == R.id.ll7) {
                    jsonStr = "{" +
                            "\"gId\":\"" + gatway_id + "\"," +
                            "\"slcId\":\"" + slc_id + "\"," +
                            "\"slcGroup\":\"" + gp_id2 + "\"," +
                            "\"powerPara\":\"" + powerPara + "\"," +
                            "\"statusPara\":\"" + statusParameter + "\"," +
                            "\"Distance\":\"" + distance + "\"," +
                            "\"Longitude\":\"" + lngStr + "\"," +
                            "\"Latitude\":\"" + latStr + "\"," +
                            "\"NeverCommunication\" :1" +
                            "}";
                } else {
                    jsonStr = "{" +
                            "\"gId\":\"" + gatway_id + "\"," +
                            "\"slcId\":\"" + slc_id + "\"," +
                            "\"slcGroup\":\"" + gp_id2 + "\"," +
                            "\"powerPara\":\"" + powerPara + "\"," +
                            "\"statusPara\":\"" + statusParameter + "\"," +
                            "\"Distance\":\"" + distance + "\"," +
                            "\"Longitude\":\"" + lngStr + "\"," +
                            "\"Latitude\":\"" + latStr + "\"," +
                            "\"NeverCommunication\" :0" +
                            "}";
                }*/

                int neverComm;
                if (ll_id == R.id.ll7)
                    neverComm = 1;
                else
                    neverComm = 0;

                String tempGid = gatway_id;
                String tempSLCid = slc_id;
                String tempGpid = gp_id2;
                boolean isSearchPressed = spf.getBoolean(AppConstants.IS_SEARCH_PRESSED_STATUS, false);
                if (isSearchPressed) {
                    tempGid = gatway_id;
                    tempSLCid = slc_id;
                    tempGpid = gp_id2;
                } else {
                    tempGid = "";
                    tempSLCid = "";
                    tempGpid = "";
                }

                JSONObject jsonObj = new JSONObject();
                jsonObj.put("gId", tempGid);
                jsonObj.put("slcId", tempSLCid);
                jsonObj.put("slcGroup", tempGpid);
                jsonObj.put("powerPara", powerPara);
                jsonObj.put("statusPara", statusParameter);
                jsonObj.put("Distance", distance);
                jsonObj.put("Longitude", lngStr);
                jsonObj.put("Latitude", latStr);
                jsonObj.put("NeverCommunication", neverComm);
                jsonObj.put("LastReceivedMode", "");
                jsonObj.put("NodeType", nodeTypeId);


                //JSONObject jsonObj = new JSONObject(jsonStr);
                jsonfinal = jsonObj.toString();
                Log.i(AppConstants.TAG, "jsonFinal: " + jsonfinal);

            } catch (Exception e) {
            }

            RequestBody body = RequestBody.create(okhttp3.MediaType.parse("application/json; charset=utf-8"), jsonfinal);

            objApi.getSLCStatusListTemp(token, String.valueOf(pg), "20", lampTypeId, body, AppConstants.IS_SECURED).enqueue(new Callback<JsonObject>() {
                @Override
                public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                    try {
                        objUtils.dismissProgressDialog(dialog_wait);
                        progressBarStatus.setVisibility(View.GONE);
                        if (response.code() == 200) {


                            JsonObject jsonObject = response.body();

                            JsonObject objData = jsonObject.getAsJsonObject("data");

                            String status = String.valueOf(jsonObject.get("status").getAsString());
                            //String status=objData2.toString();

                            JsonArray objJsonArray = objData.getAsJsonArray("list");

                            final JsonObject objFilter = objData.getAsJsonObject("filterCounts");

                            if (objJsonArray.size() != 0) {
                                totalcount = objData.get("totalRecords").getAsInt();
                            }
                            try {
                                totalcount_all =
                                        objFilter.get("commFaults").getAsInt() +
                                                objFilter.get("controllerOn").getAsInt() +
                                                objFilter.get("controllerDim").getAsInt() +
                                                objFilter.get("controllerOff").getAsInt() +
                                                objFilter.get("driverFault").getAsInt();
                                //objFilter.get("photoCellFault").getAsInt();
                            } catch (Exception e) {
                            }

                            if (status.equalsIgnoreCase("1")) {

                                getActivity().runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        try {

                                            String total, cf, df, pcf, con, coff, cdim, nComm;

                                            if (totalcount_all > 99)
                                                total = "99+";
                                            else
                                                total = String.valueOf(totalcount_all);


                                            if (objFilter.get("commFaults").getAsInt() > 99)
                                                cf = "99+";
                                            else
                                                cf = objFilter.get("commFaults").getAsString();


                                            if (objFilter.get("driverFault").getAsInt() > 99)
                                                df = "99+";
                                            else
                                                df = objFilter.get("driverFault").getAsString();


                                            if (objFilter.get("photoCellFault").getAsInt() > 99)
                                                pcf = "99+";
                                            else
                                                pcf = objFilter.get("photoCellFault").getAsString();

                                            if (objFilter.get("controllerOn").getAsInt() > 99)
                                                con = "99+";
                                            else
                                                con = objFilter.get("controllerOn").getAsString();

                                            if (objFilter.get("controllerDim").getAsInt() > 99)
                                                cdim = "99+";
                                            else
                                                cdim = objFilter.get("controllerDim").getAsString();

                                            if (objFilter.get("controllerOff").getAsInt() > 99)
                                                coff = "99+";
                                            else
                                                coff = objFilter.get("controllerOff").getAsString();

                                            if (objFilter.get("neverCommunicated").getAsInt() > 99)
                                                nComm = "99+";
                                            else
                                                nComm = objFilter.get("neverCommunicated").getAsString();

                                            tv0.setText(total);
                                            tv0.setVisibility(View.GONE);

                                            tv1.setText(cf);
                                            tv2.setText(df);
                                            tv3.setText(pcf);
                                            tv4.setText(con);
                                            tv5.setText(cdim);
                                            tv6.setText(coff);
                                            tv7.setText(nComm);

                                        } catch (Exception e) {
                                            e.printStackTrace();
                                        }
                                    }
                                });

                                if (objJsonArray.size() != 0) {
                                    tvNorecordsList.setVisibility(View.GONE);
                                    rvStatus.setVisibility(View.VISIBLE);
                                    for (int i = 0; i < objJsonArray.size(); i++) {

                                        //com.cl.lg.pojo.SLCStatus2.List objListInner = objList.get(i);

                                        StatusResponse2 objStatusResponse2 = new StatusResponse2();

                                        JsonObject objData1 = (JsonObject) objJsonArray.get(i);
                                        objStatusResponse2.setSlcNo(objData1.get("slcNo").getAsString());
                                        objStatusResponse2.setName(objData1.get("name").getAsString());

                                       /* objStatusResponse2.setA1Voltage(objData1.get("a1@Voltage").getAsDouble());
                                        objStatusResponse2.setA2Current(objData1.get("a2@Current").getAsDouble());
                                        objStatusResponse2.setA5BurnHrs(objData1.get("a5@Burn Hrs").getAsDouble());*/

                                       /* try {
                                            objStatusResponse2.setD1LampStatus(objData1.get("s1@").getAsString());
                                        } catch (Exception e) {
                                            objStatusResponse2.setD1LampStatus("0");
                                        }*/

                                        try {
                                            objStatusResponse2.setDimPer(objData1.get("@p6").getAsString() + "%");
                                        } catch (Exception e) {
                                            objStatusResponse2.setDimPer("0%");
                                        }

                                        try {
                                            objStatusResponse2.setD9Driver("0");
                                            objStatusResponse2.setLatitude(objData1.get("latitude").getAsString());
                                            objStatusResponse2.setLongitude(objData1.get("longitude").getAsString());
                                        } catch (Exception e) {
                                            objStatusResponse2.setLatitude("");
                                            objStatusResponse2.setLongitude("");
                                        }

                                        try {
                                            objStatusResponse2.setDateTime(objData1.get("datetime").getAsString());
                                        } catch (Exception e) {
                                            objStatusResponse2.setDateTime("");
                                        }

                                        try {
                                            objStatusResponse2.setLastCommunicatedOn(objData1.get("lastCommunicatedOn").getAsString());
                                        } catch (Exception e) {
                                            objStatusResponse2.setLastCommunicatedOn("");
                                        }


                                        //objStatusResponse2.setD9Driver(objData1.get("d9@Driver").getAsString());

                                        objStatusResponse2.setLlId(ll_id);

                                        Gson objGson = new Gson();
                                        String jsonInner = objGson.toJson(objJsonArray.get(i));
                                        objStatusResponse2.setJsonString(jsonInner);

                                        //JsonElement element = new JsonPrimitive(jsonInner);
                                        //JsonObject result = element.getAsJsonObject();
                                        //CommonDialogResponse[] ary=objGson.fromJson(jsonInner,CommonDialogResponse[].class);

                                        /*ArrayList<StatusResponse2> list = objGson.fromJson(jsonInner, new TypeToken<ArrayList<CommonDialogResponse>>() {
                                        }.getType());*/

                                        mListFinal2.add(objStatusResponse2);

                                        //Log.d(AppConstants.TAG, jsonInner);
                                        //mFinalStatusList.add(objListInner);

                                    }
                                    adapter.notifyDataSetChanged();
                                } else {
                                    tvNorecordsList.setVisibility(View.VISIBLE);
                                    if (isNearMePressNav) {
                                        tvNorecordsList.setText(nearmeMsg);
                                        //tvNorecordsList.setText(spf.getString(AppConstants.MESSAGE_NEAR_ME, ""));
                                        //tvNorecordsList.setText(getResources().getString(R.string.no_data_record_near_me));
                                    } else
                                        tvNorecordsList.setText(getResources().getString(R.string.no_data));
                                    rvStatus.setVisibility(View.GONE);
                                }
                            }

                        } else {
                            objUtils.responseHandle(getActivity(), response.code(), response.errorBody());
                            if (isNearMePressNav) {
                                resetNearMe();
                            }
                        }
                    } catch (Exception e) {
                    }
                }

                @Override
                public void onFailure(Call<JsonObject> call, Throwable t) {
                    objUtils.dismissProgressDialog(dialog_wait);
                    progressBarStatus.setVisibility(View.GONE);
                }
            });

        } else {
            progressBarStatus.setVisibility(View.GONE);
            Utils.dialogForMessage(getActivity(), getResources().getString(R.string.no_internet_connection));
        }
    }

    @Override
    public void onClick(View view) {
        String event_name = "";
        switch (view.getId()) {
            case R.id.edtGatwayName:
                if (Utils.isInternetAvailable(getActivity())) {
                    dialog_search_list(edtGatwayName);
                } else {
                    Utils.dialogForMessage(getActivity(), getResources().getString(R.string.no_internet_connection));
                }

                break;
            case R.id.edtAddress:

                if (Utils.isInternetAvailable(getActivity())) {
                    dialog_search_list(edtAddress);
                } else {
                    Utils.dialogForMessage(getActivity(), getResources().getString(R.string.no_internet_connection));
                }

                break;
            case R.id.edtNearMe:
                //dialog_search_list(dummyData2(), edtNearMe);
                break;
            case R.id.edtSLCName:
                if (Utils.isInternetAvailable(getActivity())) {
                    dialog_search_list(edtSLCName);
                } else {
                    Utils.dialogForMessage(getActivity(), getResources().getString(R.string.no_internet_connection));
                }

                break;
            case R.id.edtGroup:

                if (Utils.isInternetAvailable(getActivity())) {
                    dialog_search_list(edtGroup);
                } else {
                    Utils.dialogForMessage(getActivity(), getResources().getString(R.string.no_internet_connection));
                }
                break;
            case R.id.ivFilter:
                event_name = spf.getString(AppConstants.GLOBAL_EVENT_NAME, "") + "searchStatus";
                Log.d(AppConstants.TAG, "Event:" + event_name);
                Bundle bundle = new Bundle();
                bundle.putString(FirebaseAnalytics.Param.ITEM_NAME, event_name);
                mFirebaseAnalytics.logEvent(FirebaseAnalytics.Event.SELECT_CONTENT, bundle);

                if (llSlcCommands.getVisibility() == View.GONE) {
                    llSlcCommands.setVisibility(View.VISIBLE);
                    SearchVisibillity = View.VISIBLE;
                    //resetSearchFields();
                    spf.edit().putInt(AppConstants.IS_SEARCH_ON, View.VISIBLE).apply();
                    //selectll0();
                    //getDataStatus2(1, R.id.ll0);
                } else {
                    spf.edit().putInt(AppConstants.IS_SEARCH_ON, View.GONE).apply();
                    llSlcCommands.setVisibility(View.GONE);
                    SearchVisibillity = View.GONE;
                }
                break;

            case R.id.ll0:
                selectll0();
                getDataStatus2(1, R.id.ll0, false);
                spf.edit().putInt(AppConstants.UI_ID, R.id.ll0).apply();
                break;
            case R.id.ll1:
                event_name = spf.getString(AppConstants.GLOBAL_EVENT_NAME, "") + "communicationFault";
                Log.d(AppConstants.TAG, "Event:" + event_name);
                bundleAnalytics = new Bundle();
                bundleAnalytics.putString(FirebaseAnalytics.Param.ITEM_NAME, event_name);
                mFirebaseAnalytics.logEvent(FirebaseAnalytics.Event.SELECT_CONTENT, bundleAnalytics);
                selectll1(true);
                getDataStatus2(1, R.id.ll1, false);
                spf.edit().putInt(AppConstants.UI_ID, R.id.ll1).apply();
                break;
            case R.id.ll2:
                event_name = spf.getString(AppConstants.GLOBAL_EVENT_NAME, "") + "driverFault";
                Log.d(AppConstants.TAG, "Event:" + event_name);
                bundleAnalytics = new Bundle();
                bundleAnalytics.putString(FirebaseAnalytics.Param.ITEM_NAME, event_name);
                mFirebaseAnalytics.logEvent(FirebaseAnalytics.Event.SELECT_CONTENT, bundleAnalytics);
                selectll2(true);
                getDataStatus2(1, R.id.ll2, false);
                spf.edit().putInt(AppConstants.UI_ID, R.id.ll2).apply();
                break;
            case R.id.ll3:
                selectll3(true);
                getDataStatus2(1, R.id.ll3, false);
                spf.edit().putInt(AppConstants.UI_ID, R.id.ll3).apply();
                break;
            case R.id.ll4:
                event_name = spf.getString(AppConstants.GLOBAL_EVENT_NAME, "") + "lightOn";
                Log.d(AppConstants.TAG, "Event:" + event_name);
                bundleAnalytics = new Bundle();
                bundleAnalytics.putString(FirebaseAnalytics.Param.ITEM_NAME, event_name);
                mFirebaseAnalytics.logEvent(FirebaseAnalytics.Event.SELECT_CONTENT, bundleAnalytics);
                selectll4(true);
                getDataStatus2(1, R.id.ll4, false);
                break;
            case R.id.ll5:
                event_name = spf.getString(AppConstants.GLOBAL_EVENT_NAME, "") + "lightDim";
                Log.d(AppConstants.TAG, "Event:" + event_name);
                bundleAnalytics = new Bundle();
                bundleAnalytics.putString(FirebaseAnalytics.Param.ITEM_NAME, event_name);
                mFirebaseAnalytics.logEvent(FirebaseAnalytics.Event.SELECT_CONTENT, bundleAnalytics);
                selectll5(true);
                getDataStatus2(1, R.id.ll5, false);
                spf.edit().putInt(AppConstants.UI_ID, R.id.ll5).apply();
                break;
            case R.id.ll6:
                event_name = spf.getString(AppConstants.GLOBAL_EVENT_NAME, "") + "lightOff";
                Log.d(AppConstants.TAG, "Event:" + event_name);
                bundleAnalytics = new Bundle();
                bundleAnalytics.putString(FirebaseAnalytics.Param.ITEM_NAME, event_name);
                mFirebaseAnalytics.logEvent(FirebaseAnalytics.Event.SELECT_CONTENT, bundleAnalytics);
                selectll6(true);
                getDataStatus2(1, R.id.ll6, false);
                spf.edit().putInt(AppConstants.UI_ID, R.id.ll6).apply();
                break;

            case R.id.ll7:
                event_name = spf.getString(AppConstants.GLOBAL_EVENT_NAME, "") + "Planning-Photocell";
                Log.d(AppConstants.TAG, "Event:" + event_name);
                bundleAnalytics = new Bundle();
                bundleAnalytics.putString(FirebaseAnalytics.Param.ITEM_NAME, event_name);
                mFirebaseAnalytics.logEvent(FirebaseAnalytics.Event.SELECT_CONTENT, bundleAnalytics);
                selectll7(true);
                getDataStatus2(1, R.id.ll7, false);
                spf.edit().putInt(AppConstants.UI_ID, R.id.ll7).apply();
                break;

            case R.id.btnSearch:
                spf.edit().putBoolean(AppConstants.IS_SEARCH_PRESSED_STATUS, true).apply();
                selectll0();
                getDataStatus2(1, R.id.ll0, false);
                break;

            case R.id.btnClear:
                resetSearchFields();
                resetNearMe();
                getDataStatus2(1, id, false);
                break;

            case R.id.fabDim:
                if (type.toString().equalsIgnoreCase(AppConstants.APP_TYPE_PAID)) {
                    if (Utils.isInternetAvailable(getActivity())) {
                        if (isChecked()) {
                            if (getCheckedSLCCount() > 10) {
                                dialogMulitpleCmdFireConfermation(getActivity(), R.id.fabDim, false);
                            } else
                                dialog_dim(0);
                        }
                    } else {
                        Utils.dialogForMessage(getActivity(), getActivity().getResources().getString(R.string.no_internet_connection));
                    }
                } else
                    objUtils.dialogForApptype(getActivity(), typeMessage);
                break;

            case R.id.fabOff:
                if (type.toString().equalsIgnoreCase(AppConstants.APP_TYPE_PAID)) {
                    if (Utils.isInternetAvailable(getActivity())) {
                        if (isChecked()) {
                            if (getCheckedSLCCount() > 10) {
                                dialogMulitpleCmdFireConfermation(getActivity(), R.id.fabOff, false);
                            } else
                                fireCommand(AppConstants.SWITCH_OFF, getCheckedSLC(), 0, 0);
                        }
                    } else {
                        Utils.dialogForMessage(getActivity(), getActivity().getResources().getString(R.string.no_internet_connection));
                    }
                } else
                    objUtils.dialogForApptype(getActivity(), typeMessage);
                break;
            case R.id.fabOn:
                if (type.toString().equalsIgnoreCase(AppConstants.APP_TYPE_PAID)) {
                    if (Utils.isInternetAvailable(getActivity())) {
                        //if (isChecked()) {
                        if (getCheckedSLCCount() > 10) {
                            dialogMulitpleCmdFireConfermation(getActivity(), R.id.fabOn, false);
                        } else
                            fireCommand(AppConstants.SWITCH_ON, getCheckedSLC(), 0, 0);
                        //}
                    } else
                        Utils.dialogForMessage(getActivity(), getActivity().getResources().getString(R.string.no_internet_connection));
                } else
                    objUtils.dialogForApptype(getActivity(), typeMessage);
                break;
            case R.id.fabReadData:
                if (type.toString().equalsIgnoreCase(AppConstants.APP_TYPE_PAID)) {
                    if (Utils.isInternetAvailable(getActivity())) {
                        if (isChecked()) {
                            /*event_name=spf.getString(AppConstants.GLOBAL_EVENT_NAME,"")+"readData";
                            Log.d(AppConstants.TAG,"Event:"+event_name);
                            bundleAnalytics = new Bundle();
                            bundleAnalytics.putString(FirebaseAnalytics.Param.ITEM_NAME, event_name);
                            mFirebaseAnalytics.logEvent(FirebaseAnalytics.Event.SELECT_CONTENT, bundleAnalytics);*/
                            fireCommand(AppConstants.READ_DATA, getCheckedSLC(), 0, 0);
                        }
                    } else {
                        Utils.dialogForMessage(getActivity(), getActivity().getResources().getString(R.string.no_internet_connection));
                    }
                } else
                    objUtils.dialogForApptype(getActivity(), typeMessage);
                break;
            case R.id.fabReset:
                if (type.toString().equalsIgnoreCase(AppConstants.APP_TYPE_PAID)) {
                    if (Utils.isInternetAvailable(getActivity())) {
                        if (isChecked()) {
                            fireCommand(AppConstants.RESET_SLC_TRIP, getCheckedSLC(), 0, 0);
                        }
                    } else {
                        Utils.dialogForMessage(getActivity(), getActivity().getResources().getString(R.string.no_internet_connection));
                    }
                } else
                    objUtils.dialogForApptype(getActivity(), typeMessage);
                break;
            case R.id.btnNearMe:

                if (type.toString().equalsIgnoreCase(AppConstants.APP_TYPE_PAID)) {
                    event_name = spf.getString(AppConstants.GLOBAL_EVENT_NAME, "") + "nearme";
                    Log.d(AppConstants.TAG, "Event:" + event_name);
                    bundleAnalytics = new Bundle();
                    bundleAnalytics.putString(FirebaseAnalytics.Param.ITEM_NAME, event_name);
                    mFirebaseAnalytics.logEvent(FirebaseAnalytics.Event.SELECT_CONTENT, bundleAnalytics);

                    if (isNearMePressNav) {
                        resetNearMe();
                        getDataStatus2(1, id, false);
                    } else {
                        isNearMePressNav = true;
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                            request6plus();
                        } else {
                            getLocation();
                        }
                    }
                } else
                    objUtils.dialogForApptype(getActivity(), typeMessage);
                break;

            case R.id.ivNearme:
                if (type.toString().equalsIgnoreCase(AppConstants.APP_TYPE_PAID)) {
                    event_name = spf.getString(AppConstants.GLOBAL_EVENT_NAME, "") + "nearme";
                    Log.d(AppConstants.TAG, "Event:" + event_name);
                    bundleAnalytics = new Bundle();
                    bundleAnalytics.putString(FirebaseAnalytics.Param.ITEM_NAME, event_name);
                    mFirebaseAnalytics.logEvent(FirebaseAnalytics.Event.SELECT_CONTENT, bundleAnalytics);

                    resetSearchFields();
                    if (isNearMePressNav) {
                        resetNearMe();
                        getDataStatus2(1, id, false);
                    } else {
                        isNearMePressNav = true;
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                            request6plus();
                        } else {
                            getLocation();
                        }
                    }
                } else
                    objUtils.dialogForApptype(getActivity(), typeMessage);
                break;

            case R.id.fabMore:
                //if (isChecked())
                //dialog_command(objUtils.getCommands(getActivity()));
                openCommand();
                break;
            case R.id.fabRoute:
                if (type.toString().equalsIgnoreCase(AppConstants.APP_TYPE_PAID)) {
                    event_name = spf.getString(AppConstants.GLOBAL_EVENT_NAME, "") + "route";
                    Log.d(AppConstants.TAG, "Event:" + event_name);
                    bundleAnalytics = new Bundle();
                    bundleAnalytics.putString(FirebaseAnalytics.Param.ITEM_NAME, event_name);
                    mFirebaseAnalytics.logEvent(FirebaseAnalytics.Event.SELECT_CONTENT, bundleAnalytics);
                    if (Utils.isInternetAvailable(getActivity())) {
                        if (isChecked()) {
                            if (getCheckedSLCCount() > 10) // 24 map box
                                Utils.dialogForMessageRoute(getActivity(), getResources().getString(R.string.routeValidation));
                            else {
                                //navigate to google map
                                setNavigation();
                                //navigate to map box
                                //setNavigation2(); // navigation UI
                                //navigationRouteMapBox(); //
                            }
                        }
                    } else
                        Utils.dialogForMessage(getActivity(), getActivity().getResources().getString(R.string.no_internet_connection));
                } else
                    objUtils.dialogForApptype(getActivity(), typeMessage);
                break;
        }
    }

    void operationComandFire(String cmdName) {
        if (type.toString().equalsIgnoreCase(AppConstants.APP_TYPE_PAID)) {

            if (cmdName.equalsIgnoreCase(getActivity().getResources().getString(R.string.set_mode))) {
                setModeApi();
            } else {
                if (isChecked()) {

                    if (cmdName.equalsIgnoreCase(getActivity().getResources().getString(R.string.light_dim))) {
                        //per dialog
                        //fireCommand(AppConstants.SWITCH_DIM, getCheckedSLC(), 0, 0);

                        if (getCheckedSLCCount() > 10) {
                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M)
                                dialogOptionTouch(R.id.fabDim, false);
                            else
                                dialog_password(R.id.fabDim, false);
                        } else
                            dialog_dim(0);
                        Log.i("***", "dim");

                    } else if (cmdName.equalsIgnoreCase(getActivity().getResources().getString(R.string.reset_trip))) {
                        fireCommand(AppConstants.RESET_SLC_TRIP, getCheckedSLC(), 0, 0);
                        Log.i("***", "reset");
                    } else if (cmdName.equalsIgnoreCase(getActivity().getResources().getString(R.string.light_off))) {

                        if (getCheckedSLCCount() > 10)
                            dialog_password(R.id.fabOff, false);
                        else
                            fireCommand(AppConstants.SWITCH_OFF, getCheckedSLC(), 0, 0);

                        Log.i("***", "off");

                    } else if (cmdName.equalsIgnoreCase(getActivity().getResources().getString(R.string.light_on))) {

                        if (getCheckedSLCCount() > 10)
                            dialog_password(R.id.fabOn, false);
                        else
                            fireCommand(AppConstants.SWITCH_ON, getCheckedSLC(), 0, 0);
                        Log.i("***", "on");

                    } else if (cmdName.equalsIgnoreCase(getActivity().getResources().getString(R.string.read_data))) {
                        fireCommand(AppConstants.READ_DATA, getCheckedSLC(), 0, 0);

                        Log.i("***", "read");
                    } else if (cmdName.equalsIgnoreCase(getActivity().getResources().getString(R.string.get_mode))) {
                        fireCommand(AppConstants.GET_MODE, getCheckedSLC(), 0, 0);

                        Log.i("***", "get");
                    } else if (cmdName.equalsIgnoreCase(getResources().getString(R.string.route))) {
                        String event_name = spf.getString(AppConstants.GLOBAL_EVENT_NAME, "") + "route";
                        Log.d(AppConstants.TAG, "Event:" + event_name);
                        bundleAnalytics = new Bundle();
                        bundleAnalytics.putString(FirebaseAnalytics.Param.ITEM_NAME, event_name);
                        mFirebaseAnalytics.logEvent(FirebaseAnalytics.Event.SELECT_CONTENT, bundleAnalytics);

                        if (Utils.isInternetAvailable(getActivity())) {
                            if (isChecked()) {
                                if (getCheckedSLCCount() > 10)
                                    Utils.dialogForMessageRoute(getActivity(), getResources().getString(R.string.routeValidation));
                                else {
                                    //navigate to map
                                    setNavigation();
                                }
                            }
                        } else
                            Utils.dialogForMessage(getActivity(), getActivity().getResources().getString(R.string.no_internet_connection));

                    }
                } else {
                }
            }
        } else {
            objUtils.dialogForApptype(getActivity(), typeMessage);
        }
    }

    void openCommand() {
        FragmentManager fm = getActivity().getFragmentManager();
        FragmentTransaction fragmentTransaction = fm.beginTransaction();
        CommandFragment CommandFragment = new CommandFragment();

        Bundle objBundle = new Bundle();
        objBundle.putBoolean("isFromStatus", true);
        CommandFragment.setArguments(objBundle);

        fragmentTransaction.add(R.id.frm1, CommandFragment);
        fragmentTransaction.hide(StatusFragment.this);
        fragmentTransaction.addToBackStack(StatusFragment.class.getName());
        fragmentTransaction.commit();
    }

    int getCheckedSLCCount() {
        int count = 0;
        for (int i = 0; i < mListFinal2.size(); i++) {
            if (mListFinal2.get(i).isChecked()) {
                count++;
            }
        }
        return count;
    }

    void dialogMulitpleCmdFireConfermation(Activity activity, int id, boolean isFromSetMode) {
        if (activity != null) {

            androidx.appcompat.app.AlertDialog.Builder builder = new androidx.appcompat.app.AlertDialog.Builder(activity);
            builder.setMessage(getResources().getString(R.string.cmdConfermation))
                    .setCancelable(true)
                    .setTitle(getResources().getString(R.string.app_name))
                    .setPositiveButton(getResources().getString(R.string.yes), new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M)
                                dialogOptionTouch(id, isFromSetMode);
                            else
                                dialog_password(id, isFromSetMode);
                        }
                    })
                    .setNegativeButton(getResources().getString(R.string.no), new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            dialogInterface.dismiss();
                        }
                    });

            //Creating dialog box
            androidx.appcompat.app.AlertDialog alert = builder.create();
            //Setting the title manually
            alert.requestWindowFeature(Window.FEATURE_NO_TITLE);
            alert.show();
        }
    }

    /*void setNavigation2() {
        Bundle objBundle = new Bundle();

        ArrayList<LatLng> latLngs = new ArrayList<>();

        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < mListFinal2.size(); i++) {
            if (mListFinal2.get(i).isChecked()) {
                LatLng latLng = new LatLng(Double.valueOf(mListFinal2.get(i).getLatitude()), Double.valueOf(mListFinal2.get(i).getLongitude()));
                latLngs.add(latLng);
            }
        }

        String sortedLatLng = objUtils.getSortedLatLngString(spf, latLngs);
        Log.i(AppConstants.TAG, sortedLatLng);
        objBundle.putString(AppConstants.SOURTED_ROUTE, sortedLatLng);
        RouteFragment fragment = new RouteFragment();
        fragment.setArguments(objBundle);
        objUtils.loadFragment(new RouteFragment(), getActivity());
    }*/

    void setNavigation() {

        if (objUtils.isGoogleMapsInstalled(getActivity())) {
            ArrayList<LatLng> latLngs = new ArrayList<>();

            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < mListFinal2.size(); i++) {
                if (mListFinal2.get(i).isChecked()) {

                    if (
                            mListFinal2.get(i).getLatitude().equalsIgnoreCase("")
                                    || mListFinal2.get(i).getLongitude().equalsIgnoreCase("")
                                    || mListFinal2.get(i).getLatitude().equalsIgnoreCase("0.0")
                                    || mListFinal2.get(i).getLongitude().equalsIgnoreCase("0.0")
                                    || mListFinal2.get(i).getLatitude().equalsIgnoreCase("0")
                                    || mListFinal2.get(i).getLongitude().equalsIgnoreCase("0")
                    ) {
                        //nothing put here
                    } else {
                        LatLng latLng = new LatLng(Double.valueOf(mListFinal2.get(i).getLatitude()), Double.valueOf(mListFinal2.get(i).getLongitude()));
                        latLngs.add(latLng);
                    }
                }
            }

            String sortedLatLng = objUtils.getSortedLatLngString(spf, latLngs);
            Log.i(AppConstants.TAG, sortedLatLng);

            String uri = String.format(Locale.ENGLISH, "http://maps.google.com/maps?daddr=" + sortedLatLng);
            Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(uri));
            intent.setPackage("com.google.android.apps.maps");
            startActivity(intent);
        } else {
            objUtils.dialogForNavigatePlayStore(getActivity(), getResources().getString(R.string.google_map_not_installed), "com.google.android.apps.maps");
        }

        //String uri = String.format(Locale.ENGLISH, "http://maps.google.com/maps?daddr=%s,%s", lat, lng);
        //String uri = String.format(Locale.ENGLISH, "http://maps.google.com/maps?daddr=%s,%s+to:%s,%s+to:%s,%s", "23.1235", "72.875747", "23.0977", "72.5491", "23.8851", "72.9998");
        //String uri = String.format(Locale.ENGLISH, "http://maps.google.com/maps?daddr=%s,%s+to:%s,%s+", lat, lng,"23.0977", "72.5491")
        //if (mListFinal2.size() == 1)
        //    sb.append(mListFinal2.get(i).getLatitude() + "," + mListFinal2.get(i).getLongitude());
        //else
        //    sb.append("+to:" + mListFinal2.get(i).getLatitude() + "," + mListFinal2.get(i).getLongitude());
       /* java.util.List<String> urls = getDirectionsUrl(latLngs);
        if (urls.size() > 1) {
            for (int i = 0; i < urls.size(); i++) {
                String url = urls.get(i);
                Log.i(AppConstants.TAG, "URL: " + url);
            }
        }*/
    }

    private java.util.List<String> getDirectionsUrl(ArrayList<LatLng> markerPoints) {
        java.util.List<String> mUrls = new ArrayList<>();
        if (markerPoints.size() > 1) {
            String str_origin = markerPoints.get(0).latitude + "," + markerPoints.get(0).longitude;
            String str_dest = markerPoints.get(1).latitude + "," + markerPoints.get(1).longitude;

            String sensor = "sensor=false";
            String parameters = "origin=" + str_origin + "&destination=" + str_dest + "&" + sensor;
            String output = "json";
            String url = "https://maps.googleapis.com/maps/api/directions/" + output + "?" + parameters;

            mUrls.add(url);
            for (int i = 2; i < markerPoints.size(); i++)//loop starts from 2 because 0 and 1 are already printed
            {
                str_origin = str_dest;
                str_dest = markerPoints.get(i).latitude + "," + markerPoints.get(i).longitude;
                parameters = "origin=" + str_origin + "&destination=" + str_dest + "&" + sensor;
                url = "https://maps.googleapis.com/maps/api/directions/" + output + "?" + parameters;
                mUrls.add(url);
            }
        }
        return mUrls;
    }

    private void request6plus() {
        if (!Utils.hasPermissions(getActivity(), PERMISSIONS)) {
            ActivityCompat.requestPermissions(getActivity(), PERMISSIONS, PERMISSION_ALL);
            return;
        } else {
            getLocation();
        }
    }

    GPSTracker gps;

    private void getLocation() {
        gps = new GPSTracker(getActivity());
        if (gps.canGetLocation()) {

            double latitude = gps.getLatitude();
            double longitude = gps.getLongitude();
            Log.i(AppConstants.TAG, latitude + " | " + longitude);

            if (latitude == 0.0 || longitude == 0.0) {
                dialogForLatlong.show();
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        getLocation();//recursion
                    }
                }, 5000);

            } else {
                if (dialogForLatlong.isShowing())
                    dialogForLatlong.dismiss();

                spf.edit().putString(AppConstants.LATTITUDE, String.valueOf(latitude)).apply();
                spf.edit().putString(AppConstants.LONGITUDE, String.valueOf(longitude)).apply();

                if (isNearMePressNav) {
                    nearMe();
                } else {
                    resetNearMe();
                    getDataStatus2(1, id, false);
                }

            }

            Log.d(AppConstants.TAG, "onLocationChanged ->" + latitude + "--" + longitude);
        } else {
            // Ask user to enable GPS/network in settings
            gps.showSettingsAlert();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {
        switch (requestCode) {
            case PERMISSION_ALL: {

                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    //String DEVICE_ID = getDeviceId(LoginActivity.this);
                    // spf.edit().putString(AppConstants.DEVICE_ID, DEVICE_ID).apply();
                    //Log.d(AppConstants.TAG, "Permission accept: DeviceID:" + DEVICE_ID);
                    getLocation();
                } else {
                    Toast.makeText(getActivity(), getResources().getString(R.string.permission_error), Toast.LENGTH_SHORT).show();
                }
                return;
            }
        }
    }

    void nearMe() {
        try {
            //int meter = 400;
            /*int meter=1610;
            double miles = meter * 0.00062137119;
            distance = String.valueOf(miles);*/

            distance = spf.getString(AppConstants.MILES, "");

            Log.i(AppConstants.TAG, "MILE: " + distance);

            spf.edit().putString(AppConstants.DISTANCE_NAV, distance).apply();

            String latStr = spf.getString(AppConstants.LATTITUDE, "").toString();
            String lngStr = spf.getString(AppConstants.LONGITUDE, "").toString();

            if (latStr.equals("") || lngStr.equals("")) {
                lat = 0.0;
                lng = 0.0;

            } else {
                lat = Double.valueOf(spf.getString(AppConstants.LATTITUDE, ""));
                lng = Double.valueOf(spf.getString(AppConstants.LONGITUDE, ""));
            }
            //ivNearme.setImageDrawable(getResources().getDrawable(R.drawable.arrow_1));
            ivNearme.setColorFilter(ContextCompat.getColor(getActivity(), R.color.colorSkyBlue));

            spf.edit().putBoolean(AppConstants.IS_NEAR_ME, true).apply();

            getDataStatus2(1, id, false);
        } catch (Exception e) {
        }
    }

    String getCheckedSLC() {

        StringBuilder sb = new StringBuilder();
        String SEPARATOR = "";
        for (int i = 0; i < mListFinal2.size(); i++) {
            if (mListFinal2.get(i).isChecked()) {

                sb.append(SEPARATOR);
                sb.append(mListFinal2.get(i).getSlcNo());
                SEPARATOR = ",";
            }
        }
        return sb.toString();
    }

    Callback<CommandResponse> objCallback = new Callback<CommandResponse>() {
        @Override
        public void onResponse(Call<CommandResponse> call, Response<CommandResponse> response) {
            objUtils.dismissProgressDialog(dialog_wait);
            if (response.code() == 200) {

                CommandResponse obj = response.body();

                obj.getData().getMsg();
                StringBuilder track_id = new StringBuilder();
                java.util.List<String> id = obj.getData().getTrackingIDs();
                for (int i = 0; i < id.size(); i++) {
                    if (id.size() == 1)
                        track_id.append(id.get(i));
                    else
                        track_id.append(id.get(i) + ", ");
                }

                String formatedString;

                if (id.size() == 1) {
                    /*formatedString = "\nThe command has been queued." +
                            "\n\nTrack ID: " + track_id.toString() + "" +
                            "\n\nYou can track this command from " +
                            "\n'More' Page.";*/
                    formatedString = getResources().getString(R.string.cmd_confirmation, track_id.toString());
                } else {
                    /*formatedString = "\nThe command has been queued." +
                            "\n\nYou can track this command from " +
                            "\n'More' Page.";*/
                    formatedString = getActivity().getResources().getString(R.string.cmd_cofirmation_mulitple);
                }

                //Utils.dialogForMessageWithTitle(getActivity(), formatedString, "LightingGale");
                dialogForMessageRefreshData(getActivity(), formatedString, getString(R.string.app_name));

            } else {
                objUtils.responseHandle(getActivity(), response.code(), response.errorBody());
            }
        }

        @Override
        public void onFailure(Call<CommandResponse> call, Throwable t) {
            objUtils.dismissProgressDialog(dialog_wait);
        }
    };

    void fireCommand(String type, String slc, int dimValue, int modeType) {

        if (isChecked()) {
            if (Utils.isInternetAvailable(getActivity())) {

                String bodyRaw = "";
                if (type.equalsIgnoreCase(AppConstants.RESET_SLC_TRIP)) {
                    bodyRaw = "{\n" +
                            "\"ResetSLCTrip\":" +
                            "[{" +
                            "\"SLCIdList\":\"" + slc + "\",\n" +
                            "\"Command\" : \"Reset SLC Trip\"\n" +
                            "}]\n" +
                            "}";
                } else if (type.equalsIgnoreCase(AppConstants.READ_DATA)) {
                    bodyRaw = "{\n" +
                            "\"ReadDataList\":" +
                            "[{" +
                            "\"SLCIdList\":\"" + slc + "\"\n" +
                            "}]\n" +
                            "}";
                } else if (type.equalsIgnoreCase(AppConstants.SWITCH_ON)) {
                    bodyRaw = "{\n" +
                            "\"SwitchONOFF\":" +
                            "[{" +
                            "\"SLCIdList\":\"" + slc + "\",\n" +
                            "\"Command\" : \"ON\"\n" +
                            "}]\n" +
                            "}";
                } else if (type.equalsIgnoreCase(AppConstants.SWITCH_OFF)) {
                    bodyRaw = "{\n" +
                            "\"SwitchONOFF\":" +
                            "[{" +
                            "\"SLCIdList\":\"" + slc + "\",\n" +
                            "\"Command\" : \"OFF\"\n" +
                            "}]\n" +
                            "}";
                } else if (type.equalsIgnoreCase(AppConstants.SWITCH_DIM)) {
                    bodyRaw = "{\n" +
                            "\"DIMSLC\":" +
                            "[{" +
                            "\"SLCIdList\":\"" + slc + "\",\n" +
                            "\"DIMValue\" : \"" + dimValue + "\"\n" +
                            "}]\n" +
                            "}";
                } else if (type.equalsIgnoreCase(AppConstants.SET_MODE)) {
                    bodyRaw = "{\n" +
                            "\"SetModeSLC\":" +
                            "[{" +
                            "\"SLCIdList\":\"" + slc + "\",\n" +
                            "\"ModeTypeValue\" : \"" + modeType + "\"\n" +
                            "}]\n" +
                            "}";
                } else if (type.equalsIgnoreCase(AppConstants.GET_MODE)) {
                    bodyRaw = "{\n" +
                            "\"GetMode\":" +
                            "[{" +
                            "\"SLCIdList\":\"" + slc + "\"\n" +
                            "}]\n" +
                            "}";
                }

                JSONObject jsonObj = null;
                try {
                    jsonObj = new JSONObject(bodyRaw);
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                String jsonfinal = jsonObj.toString();
                Log.i(AppConstants.TAG, "jsonFinal: " + jsonfinal);
                RequestBody body = RequestBody.create(okhttp3.MediaType.parse("application/json; charset=utf-8"), jsonfinal);
                dialog_wait.show();

                String event_name = "";

                bundleAnalytics = new Bundle();
                if (type.equalsIgnoreCase(AppConstants.RESET_SLC_TRIP)) {
                    event_name = spf.getString(AppConstants.GLOBAL_EVENT_NAME, "") + "resetTrip";
                    bundleAnalytics.putString(FirebaseAnalytics.Param.ITEM_NAME, event_name);
                    objApi.resetTrip(token, body, AppConstants.IS_SECURED).enqueue(objCallback);
                } else if (type.equalsIgnoreCase(AppConstants.READ_DATA)) {
                    event_name = spf.getString(AppConstants.GLOBAL_EVENT_NAME, "") + "readData";
                    bundleAnalytics.putString(FirebaseAnalytics.Param.ITEM_NAME, event_name);
                    objApi.ReadData(token, body, AppConstants.IS_SECURED).enqueue(objCallback);
                } else if (type.equalsIgnoreCase(AppConstants.SWITCH_ON) || type.equalsIgnoreCase(AppConstants.SWITCH_OFF)) {
                    event_name = spf.getString(AppConstants.GLOBAL_EVENT_NAME, "") + "switchOn";
                    bundleAnalytics.putString(FirebaseAnalytics.Param.ITEM_NAME, event_name);
                    objApi.switchOnOffSlc(token, body, AppConstants.IS_SECURED).enqueue(objCallback);
                } else if (type.equalsIgnoreCase(AppConstants.SWITCH_DIM)) {
                    event_name = spf.getString(AppConstants.GLOBAL_EVENT_NAME, "") + "dim";
                    bundleAnalytics.putString(FirebaseAnalytics.Param.ITEM_NAME, event_name);
                    objApi.dimSlc(token, body, AppConstants.IS_SECURED).enqueue(objCallback);
                } else if (type.equalsIgnoreCase(AppConstants.SET_MODE)) {
                    event_name = spf.getString(AppConstants.GLOBAL_EVENT_NAME, "") + "setMode";
                    bundleAnalytics.putString(FirebaseAnalytics.Param.ITEM_NAME, event_name);
                    objApi.setMode(token, body, AppConstants.IS_SECURED).enqueue(objCallback);
                } else if (type.equalsIgnoreCase(AppConstants.GET_MODE)) {
                    event_name = spf.getString(AppConstants.GLOBAL_EVENT_NAME, "") + "getMode";
                    bundleAnalytics.putString(FirebaseAnalytics.Param.ITEM_NAME, event_name);
                    objApi.getMode(token, body, AppConstants.IS_SECURED).enqueue(objCallback);
                }

                Log.d(AppConstants.TAG, "Event:" + event_name);
                mFirebaseAnalytics.logEvent(FirebaseAnalytics.Event.SELECT_CONTENT, bundleAnalytics);
            } else {
                Utils.dialogForMessage(getActivity(), getResources().getString(R.string.no_internet_connection));
            }
        }

    }

    void getSLCName(String slc, final ListDialogSentCommandAdapter adapter, Dialog dialog) {
        //dialog_wait.show();
        mListSearch.clear();
        adapter.notifyDataSetChanged();

        lamptypid = spf.getString(AppConstants.SELECTED_LAMP_TYPE_ID, "");

        if (isSingleNodeType)
            nodeTypeid = "";
        else
            nodeTypeid = spf.getString(AppConstants.SELECTED_NODE_TYPE_ID, "");

        objApi.getSLCName(token, slc, "", lamptypid, AppConstants.IS_SECURED, nodeTypeid).enqueue(new Callback<SLCName>() {
            @Override
            public void onResponse(Call<SLCName> call, Response<SLCName> response) {
                objUtils.dismissProgressDialog(dialog_wait);

                if (response.code() == 200) {
                    SLCName slcName = response.body();
                    if (slcName.getStatus().equalsIgnoreCase("1")) {

                        if (slcName.getData().size() == 0) {
                            dialog.findViewById(R.id.rvDialog).setVisibility(View.GONE);
                            dialog.findViewById(R.id.tvNoRecord).setVisibility(View.VISIBLE);
                        } else {
                            dialog.findViewById(R.id.rvDialog).setVisibility(View.VISIBLE);
                            dialog.findViewById(R.id.tvNoRecord).setVisibility(View.GONE);
                        }
                        mListSearch.clear();
                        for (int i = 0; i < slcName.getData().size(); i++) {
                            CommonDialogResponse response1 = new CommonDialogResponse();
                            //response1.setValue(slcName.getData().get(i).getText());
                            response1.setValue(slcName.getData().get(i).getValue());
                            response1.setId_str(slcName.getData().get(i).getValue());
                            response1.setType("SLC_NAME");
                            mListSearch.add(response1);
                        }
                        adapter.notifyDataSetChanged();
                    } else {
                        dialog.findViewById(R.id.rvDialog).setVisibility(View.GONE);
                        dialog.findViewById(R.id.tvNoRecord).setVisibility(View.VISIBLE);
                    }
                } else
                    objUtils.responseHandle(getActivity(), response.code(), response.errorBody());
            }

            @Override
            public void onFailure(Call<SLCName> call, Throwable t) {
                objUtils.dismissProgressDialog(dialog_wait);
            }
        });
    }

    void getSLCGroup(String gid, final ListDialogSentCommandAdapter adapter, Dialog dialog) {
        mListSearch.clear();
        //dialog_wait.show();
        lamptypid = spf.getString(AppConstants.SELECTED_LAMP_TYPE_ID, "");

        if (isSingleNodeType)
            nodeTypeid = "";
        else
            nodeTypeid = spf.getString(AppConstants.SELECTED_NODE_TYPE_ID, "");

        objApi.getSLCGp(token, gid, lamptypid, AppConstants.IS_SECURED, nodeTypeid).enqueue(new Callback<GroupName>() {
            @Override
            public void onResponse(Call<GroupName> call, Response<GroupName> response) {
                objUtils.dismissProgressDialog(dialog_wait);
                if (response.code() == 200) {
                    GroupName slcName = response.body();
                    if (slcName.getStatus().equalsIgnoreCase("1")) {

                        if (slcName.getData().size() == 0) {
                            dialog.findViewById(R.id.rvDialog).setVisibility(View.GONE);
                            dialog.findViewById(R.id.tvNoRecord).setVisibility(View.VISIBLE);
                        } else {
                            dialog.findViewById(R.id.rvDialog).setVisibility(View.VISIBLE);
                            dialog.findViewById(R.id.tvNoRecord).setVisibility(View.GONE);
                        }

                        for (int i = 0; i < slcName.getData().size(); i++) {
                            CommonDialogResponse response1 = new CommonDialogResponse();
                            response1.setValue(slcName.getData().get(i).getGroupName());
                            response1.setId(Integer.valueOf(slcName.getData().get(i).getGroupID()));
                            response1.setType("SLC_GP");
                            mListSearch.add(response1);
                        }
                        adapter.notifyDataSetChanged();
                    } else {
                        dialog.findViewById(R.id.rvDialog).setVisibility(View.GONE);
                        dialog.findViewById(R.id.tvNoRecord).setVisibility(View.VISIBLE);
                    }
                } else
                    objUtils.responseHandle(getActivity(), response.code(), response.errorBody());
            }

            @Override
            public void onFailure(Call<GroupName> call, Throwable t) {
                objUtils.dismissProgressDialog(dialog_wait);
            }
        });
    }

    void getGatway(final ListDialogSentCommandAdapter adapter, Dialog dialog) {
        dialog_wait.show();
        mListSearch.clear();

        objApi.getGatwayList(token, "", AppConstants.IS_SECURED).enqueue(new Callback<GetGatway>() {
            @Override
            public void onResponse(Call<GetGatway> call, Response<GetGatway> response) {
                objUtils.dismissProgressDialog(dialog_wait);
                if (response.body() != null) {
                    if (response.body().getStatus().equalsIgnoreCase("1")) {
                        dialog.findViewById(R.id.rvDialog).setVisibility(View.VISIBLE);
                        dialog.findViewById(R.id.tvNoRecord).setVisibility(View.GONE);
                        GetGatway objGetCommand = response.body();
                        db.deleteTableData(DBHelper.GATWAY_TABLE_NAME);
                        mListSearch.clear();

                        if (objGetCommand.getData().size() == 0) {
                            dialog.findViewById(R.id.rvDialog).setVisibility(View.GONE);
                            dialog.findViewById(R.id.tvNoRecord).setVisibility(View.VISIBLE);
                        } else {
                            dialog.findViewById(R.id.rvDialog).setVisibility(View.VISIBLE);
                            dialog.findViewById(R.id.tvNoRecord).setVisibility(View.GONE);
                        }

                        for (int i = 0; i < objGetCommand.getData().size(); i++) {
                            CommonDialogResponse objCommonDialogResponse = new CommonDialogResponse();
                            objCommonDialogResponse.setId(Integer.valueOf(objGetCommand.getData().get(i).getId()));
                            objCommonDialogResponse.setValue(String.valueOf(objGetCommand.getData().get(i).getGatewayName()));
                            objCommonDialogResponse.setType("SLC_GATWAY");
                            mListSearch.add(objCommonDialogResponse);
                            db.insertGatway(objGetCommand.getData().get(i).getId(), objGetCommand.getData().get(i).getGatewayName());
                        }
                        adapter.notifyDataSetChanged();

                    } else {
                        dialog.findViewById(R.id.rvDialog).setVisibility(View.GONE);
                        dialog.findViewById(R.id.tvNoRecord).setVisibility(View.VISIBLE);
                    }
                }
            }

            @Override
            public void onFailure(Call<GetGatway> call, Throwable t) {
                objUtils.dismissProgressDialog(dialog_wait);
            }
        });
    }

    void dialog_search_list(final TextInputEditText editText) {
        final Dialog dialog = new Dialog(getActivity());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_serach_list);
        dialog.setCancelable(false);

        TextView dialogButtondir = dialog.findViewById(R.id.btGetDirection);
        dialogButtondir.setVisibility(View.GONE);

        TextView tvNoRecord = dialog.findViewById(R.id.tvNoRecord);


        TextView dialogButton = dialog.findViewById(R.id.btCancel);
        dialogButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        RecyclerView objRecyclerView = dialog.findViewById(R.id.rvDialog);

        objRecyclerView.setHasFixedSize(true);

        objRecyclerView.setItemViewCacheSize(20);
        objRecyclerView.setDrawingCacheEnabled(true);
        objRecyclerView.setDrawingCacheQuality(View.DRAWING_CACHE_QUALITY_HIGH);

        objRecyclerView.addItemDecoration(new DividerItemDecoration(getActivity(), LinearLayoutManager.VERTICAL));
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
        objRecyclerView.setLayoutManager(linearLayoutManager);
        final ListDialogSentCommandAdapter adapterDialog = new ListDialogSentCommandAdapter(getActivity(), mListSearch, false, new ListDialogSentCommandAdapter.MyCallbackForControlDialog() {
            @Override
            public void onClickForControl(int position, CommonDialogResponse response) {
                String displayText = response.getValue().toString();
                editText.setText(displayText);
                if (response.getType().equalsIgnoreCase("SLC_NAME")) {
                    slc_id = response.getId_str();
                    spf.edit().putString(AppConstants.SLC_ID_NAV, slc_id).apply();
                    spf.edit().putString(AppConstants.SLC_ID_VALUE_NAVE, displayText).apply();
                } else if (response.getType().equalsIgnoreCase("SLC_GP")) {
                    gp_id2 = String.valueOf(response.getId());
                    spf.edit().putString(AppConstants.GP_ID_NAV, gp_id2).apply();
                    spf.edit().putString(AppConstants.GP_ID_VALUE_NAVE, displayText).apply();
                } else if (response.getType().equalsIgnoreCase("SLC_GATWAY")) {
                    gatway_id = String.valueOf(response.getId());
                    spf.edit().putString(AppConstants.G_ID_NAV, gatway_id).apply();
                    spf.edit().putString(AppConstants.G_ID_VALUE_NAVE, displayText).apply();
                }
                dialog.dismiss();
            }
        });
        objRecyclerView.addItemDecoration(new DividerItemDecoration(getActivity(), 0));
        objRecyclerView.setAdapter(adapterDialog);


        androidx.appcompat.widget.SearchView objSearchView = dialog.findViewById(R.id.svSLCs);
        SearchManager searchManager = (SearchManager) getActivity().getSystemService(Context.SEARCH_SERVICE);
        objSearchView.setSearchableInfo(searchManager
                .getSearchableInfo(getActivity().getComponentName()));

        objSearchView.setQueryHint(getResources().getString(R.string.search));
        objSearchView.setMaxWidth(Integer.MAX_VALUE);

        if (editText == edtSLCName) {
            getSLCName("", adapterDialog, dialog);
        } else if (editText == edtGroup) {
            getSLCGroup("", adapterDialog, dialog);
        } else if (editText == edtGatwayName) {
            getGatway(adapterDialog, dialog);
        }


        objSearchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String s) {

                if (editText == edtSLCName) {
                    if (s.toString().trim().equalsIgnoreCase("")) {
                        getSLCName("", adapterDialog, dialog);
                    } else {
                        getSLCName(s.toString(), adapterDialog, dialog);
                    }
                } else if (editText == edtGroup) {
                    if (s.toString().trim().equalsIgnoreCase("")) {
                        getSLCGroup("", adapterDialog, dialog);
                    } else {
                        getSLCGroup(s.toString(), adapterDialog, dialog);
                    }
                } else {
                    ArrayList<CommonDialogResponse> mTemp = new ArrayList();
                    mTemp.addAll(db.getAllGatway());
                    adapterDialog.filter(s, mTemp);

                    ArrayList<CommonDialogResponse> temp = new ArrayList<>();
                    temp = adapterDialog.filter(s, mTemp);

                    if (temp.size() == 0) {
                        tvNoRecord.setVisibility(View.VISIBLE);
                        objRecyclerView.setVisibility(View.GONE);
                    } else {
                        tvNoRecord.setVisibility(View.GONE);
                        objRecyclerView.setVisibility(View.VISIBLE);
                    }
                }

                return false;
            }


            @Override
            public boolean onQueryTextChange(String s) {

                if (editText == edtSLCName) {
                    if (s.toString().trim().equalsIgnoreCase("")) {
                        getSLCName("", adapterDialog, dialog);
                    } else {
                        getSLCName(s.toString(), adapterDialog, dialog);
                    }
                } else if (editText == edtGroup) {
                    if (s.toString().trim().equalsIgnoreCase("")) {
                        getSLCGroup("", adapterDialog, dialog);
                    } else {
                        getSLCGroup(s.toString(), adapterDialog, dialog);
                    }
                } else {
                    ArrayList<CommonDialogResponse> mTemp = new ArrayList();
                    mTemp.addAll(db.getAllGatway());

                    ArrayList<CommonDialogResponse> temp = new ArrayList<>();
                    temp = adapterDialog.filter(s, mTemp);

                    if (temp.size() == 0) {
                        tvNoRecord.setVisibility(View.VISIBLE);
                        objRecyclerView.setVisibility(View.GONE);
                    } else {
                        tvNoRecord.setVisibility(View.GONE);
                        objRecyclerView.setVisibility(View.VISIBLE);
                    }
                }
                return false;
            }
        });

        // dialogButton.setOnTouchListener(Util.colorFilter());

        Rect displayRectangle = new Rect();
        Window window = getActivity().getWindow();

        window.getDecorView().getWindowVisibleDisplayFrame(displayRectangle);
        dialog.getWindow().setLayout((int) (displayRectangle.width() * 0.9f), (int) (displayRectangle.height() * 0.9f));
        dialog.show();
    }

    void dialog_set_mode_cmd_list(final ArrayList<DatumSetMode> mlistDialog) {

        final Dialog dialog = new Dialog(getActivity());

        dialog.setTitle(getResources().getString(R.string.select_mode));

        dialog.setContentView(R.layout.dialog_serach_list);
        dialog.setCancelable(false);


        TextView dialogButtondir = dialog.findViewById(R.id.btGetDirection);
        dialogButtondir.setVisibility(View.GONE);

        TextView dialogButton = dialog.findViewById(R.id.btCancel);
        dialogButton.setText(getResources().getString(R.string.cancel));
        dialogButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        RecyclerView objRecyclerView = dialog.findViewById(R.id.rvDialog);
        objRecyclerView.setHasFixedSize(true);
        objRecyclerView.addItemDecoration(new DividerItemDecoration(getActivity(), LinearLayoutManager.VERTICAL));
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
        objRecyclerView.setLayoutManager(linearLayoutManager);
        final ListDialogSetModeCommandAdapter adapterDialog = new ListDialogSetModeCommandAdapter(getActivity(), mlistDialog, new ListDialogSetModeCommandAdapter.MyCallbackForControlDialog() {
            @Override
            public void onClickForControl(int position, DatumSetMode response) {
                //cmd fire apiapi
                dialog.dismiss();
                Log.i(AppConstants.TAG, response.getModeName());
                if (isChecked()) {
                    int modeId = Integer.parseInt(response.getModeID());
                    /*if (response.getModeName().equalsIgnoreCase("Manual")) {
                        dialogForMessage(getActivity(), getResources().getString(R.string.strManual), modeId);
                    } else if (response.getModeName().equalsIgnoreCase("Astro Clock")) {
                        dialogForMessage(getActivity(), getResources().getString(R.string.strAstroClock), modeId);
                    } else if (response.getModeName().equalsIgnoreCase("Mix-Mode")) {
                        dialogForMessage(getActivity(), getResources().getString(R.string.strMixMode), modeId);
                    } else {*/

                    if (response.getModeName().equalsIgnoreCase("Manual")) {
                        dialogForMessage(getActivity(), getResources().getString(R.string.strManual), modeId);
                    } else {
                        if (getCheckedSLCCount() > 10) {
                            dialogMulitpleCmdFireConfermation(getActivity(), Integer.parseInt(response.getModeID()), true);
                        } else
                            fireCommand(AppConstants.SET_MODE, getCheckedSLC(), 0, modeId);
                    }
                }
            }
        });
        objRecyclerView.addItemDecoration(new DividerItemDecoration(getActivity(), 0));
        objRecyclerView.setAdapter(adapterDialog);

        androidx.appcompat.widget.SearchView objSearchView = dialog.findViewById(R.id.svSLCs);
        objSearchView.setVisibility(View.GONE);

        // dialogButton.setOnTouchListener(Util.colorFilter());

        Rect displayRectangle = new Rect();
        Window window = getActivity().getWindow();

        window.getDecorView().getWindowVisibleDisplayFrame(displayRectangle);
        dialog.getWindow().setLayout((int) (displayRectangle.width() * 0.9f), (int) (displayRectangle.height() * 0.9f));
        dialog.show();
    }

    public void dialogForMessage(Activity activity, String message, int modeId) {
        if (activity != null) {
            androidx.appcompat.app.AlertDialog.Builder builder = new androidx.appcompat.app.AlertDialog.Builder(activity);
            builder.setMessage(message)
                    .setCancelable(true)
                    .setTitle(getResources().getString(R.string.app_name))
                    .setPositiveButton(getActivity().getResources().getString(R.string.ok), new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            dialogInterface.dismiss();
                            if (getCheckedSLCCount() > 10) {
                                dialogMulitpleCmdFireConfermation(getActivity(), modeId, true);
                            } else
                                fireCommand(AppConstants.SET_MODE, getCheckedSLC(), 0, modeId);
                        }
                    })
                    .setNegativeButton(getResources().getString(R.string.cancel), new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            dialogInterface.dismiss();
                        }
                    });

            //Creating dialog box
            androidx.appcompat.app.AlertDialog alert = builder.create();
            //Setting the title manually
            alert.requestWindowFeature(Window.FEATURE_NO_TITLE);
            alert.show();
        }
    }


    ArrayList<CommonDialogResponse> dummyData2() {
        mListDialog.clear();
        for (int i = 0; i < 5; i++) {
            com.cl.lg.pojo.CommonDialogResponse objList = new com.cl.lg.pojo.CommonDialogResponse();

            String temp = "<b>SLC:</b> SLC-" + i + "<br><b>Status:</b> Sent Succesfully" + "<br><b>ACK Time:</b> 13/09/2019 09:38:10" + "<br><b><Response Time:</b> N/A";
            objList.setValue("2236" + i);

            mListDialog.add(objList);
        }
        return mListDialog;

    }

    void dialog_status_detail_list(final ArrayList<CommonResponseStatusDetailsDialog> mlistDialog, final String latitude, final String longitude) {

        final Dialog dialog = new Dialog(getActivity());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);

        dialog.setContentView(R.layout.dialog_serach_list);
        dialog.setCancelable(false);

        TextView dialogButton = dialog.findViewById(R.id.btCancel);
        dialogButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        TextView btGetDirection = dialog.findViewById(R.id.btGetDirection);
        btGetDirection.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //mlistDialog.get();
                /*Intent intent = new Intent(android.content.Intent.ACTION_VIEW,
                        Uri.parse("http://maps.google.com/maps?saddr="+20.344,34.34+"&daddr="+20.5666,45.345+"));
                startActivity(intent);*/
                //https://stackoverflow.com/questions/2662531/launching-google-maps-directions-via-an-intent-on-android

                String uri = String.format(Locale.ENGLISH, "http://maps.google.com/maps?daddr=%s,%s", latitude, longitude);
                Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(uri));
                intent.setPackage("com.google.android.apps.maps");
                startActivity(intent);


            }
        });

        RecyclerView objRecyclerView = dialog.findViewById(R.id.rvDialog);

        objRecyclerView.setHasFixedSize(true);
        objRecyclerView.addItemDecoration(new DividerItemDecoration(getActivity(), LinearLayoutManager.VERTICAL));
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
        objRecyclerView.setLayoutManager(linearLayoutManager);
        final ListDialogStatusAdapter adapterDialog = new ListDialogStatusAdapter(getActivity(), mlistDialog);
        objRecyclerView.addItemDecoration(new DividerItemDecoration(getActivity(), 0));
        objRecyclerView.setAdapter(adapterDialog);

        androidx.appcompat.widget.SearchView objSearchView = dialog.findViewById(R.id.svSLCs);
        objSearchView.setVisibility(View.GONE);
        // dialogButton.setOnTouchListener(Util.colorFilter());

        Rect displayRectangle = new Rect();
        Window window = getActivity().getWindow();

        window.getDecorView().getWindowVisibleDisplayFrame(displayRectangle);
        dialog.getWindow().setLayout((int) (displayRectangle.width() * 0.9f), (int) (displayRectangle.height() * 0.9f));
        dialog.show();
    }

    void dialog_status_detail_info() {

        final Dialog dialog = new Dialog(getActivity());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().clearFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);
        dialog.setContentView(R.layout.dialog_status_info);
        dialog.setCancelable(false);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));

        TextView dialogButton = dialog.findViewById(R.id.btCancel);
        dialogButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        // dialogButton.setOnTouchListener(Util.colorFilter());

        Rect displayRectangle = new Rect();
        Window window = getActivity().getWindow();

        window.getDecorView().getWindowVisibleDisplayFrame(displayRectangle);
        dialog.getWindow().setLayout((int) (displayRectangle.width() * 0.9f), (int) (displayRectangle.height() * 0.9f));
        dialog.show();
    }

    Dialog dialogComands;

    void dialog_command(final ArrayList<Command> mlistDialog) {

        dialogComands = new Dialog(getActivity());
        dialogComands.requestWindowFeature(Window.FEATURE_NO_TITLE);

        dialogComands.setContentView(R.layout.dialog_serach_list);
        dialogComands.setCancelable(false);

        TextView dialogButtondir = dialogComands.findViewById(R.id.btGetDirection);
        dialogButtondir.setVisibility(View.GONE);

        TextView dialogButton = dialogComands.findViewById(R.id.btCancel);
        dialogButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialogComands.dismiss();
            }
        });

        SearchView svSLCs = dialogComands.findViewById(R.id.svSLCs);
        svSLCs.setVisibility(View.GONE);

        RecyclerView objRecyclerView = dialogComands.findViewById(R.id.rvDialog);
        objRecyclerView.addItemDecoration(new DividerItemDecoration(getActivity(), 0));
        objRecyclerView.setHasFixedSize(true);
        //objRecyclerView.addItemDecoration(new DividerItemDecoration(getActivity(), LinearLayoutManager.VERTICAL));

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
        objRecyclerView.setLayoutManager(linearLayoutManager);
        final DialogCommandAdapter adapterDialog = new DialogCommandAdapter(getActivity(), mlistDialog, new DialogCommandAdapter.MyCallbackForControlDialog() {
            @Override
            public void onClickForControl(int position, String cmdName) {
                if (type.toString().equalsIgnoreCase(AppConstants.APP_TYPE_PAID)) {


                    if (cmdName.equalsIgnoreCase(getActivity().getResources().getString(R.string.set_mode))) {
                        setModeApi();
                        dialogComands.dismiss();

                    } else {
                        if (isChecked()) {

                            if (cmdName.equalsIgnoreCase(getActivity().getResources().getString(R.string.light_dim))) {
                                //per dialog
                                //fireCommand(AppConstants.SWITCH_DIM, getCheckedSLC(), 0, 0);

                                if (getCheckedSLCCount() > 10) {
                                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M)
                                        dialogOptionTouch(R.id.fabDim, false);
                                    else
                                        dialog_password(R.id.fabDim, false);
                                } else
                                    dialog_dim(0);
                                Log.i("***", "dim");

                            } else if (cmdName.equalsIgnoreCase(getActivity().getResources().getString(R.string.reset_trip))) {
                                fireCommand(AppConstants.RESET_SLC_TRIP, getCheckedSLC(), 0, 0);
                                dialogComands.dismiss();
                                Log.i("***", "reset");
                            } else if (cmdName.equalsIgnoreCase(getActivity().getResources().getString(R.string.light_off))) {

                                if (getCheckedSLCCount() > 10)
                                    dialog_password(R.id.fabOff, false);
                                else
                                    fireCommand(AppConstants.SWITCH_OFF, getCheckedSLC(), 0, 0);

                                dialogComands.dismiss();
                                Log.i("***", "off");

                            } else if (cmdName.equalsIgnoreCase(getActivity().getResources().getString(R.string.light_on))) {

                                if (getCheckedSLCCount() > 10)
                                    dialog_password(R.id.fabOn, false);
                                else
                                    fireCommand(AppConstants.SWITCH_ON, getCheckedSLC(), 0, 0);
                                Log.i("***", "on");
                                dialogComands.dismiss();
                            } else if (cmdName.equalsIgnoreCase(getActivity().getResources().getString(R.string.read_data))) {
                                fireCommand(AppConstants.READ_DATA, getCheckedSLC(), 0, 0);
                                dialogComands.dismiss();
                                Log.i("***", "read");
                            } else if (cmdName.equalsIgnoreCase(getActivity().getResources().getString(R.string.get_mode))) {
                                fireCommand(AppConstants.GET_MODE, getCheckedSLC(), 0, 0);
                                dialogComands.dismiss();
                                Log.i("***", "get");
                            } else if (cmdName.equalsIgnoreCase(getResources().getString(R.string.route))) {
                                String event_name = spf.getString(AppConstants.GLOBAL_EVENT_NAME, "") + "route";
                                Log.d(AppConstants.TAG, "Event:" + event_name);
                                bundleAnalytics = new Bundle();
                                bundleAnalytics.putString(FirebaseAnalytics.Param.ITEM_NAME, event_name);
                                mFirebaseAnalytics.logEvent(FirebaseAnalytics.Event.SELECT_CONTENT, bundleAnalytics);

                                if (Utils.isInternetAvailable(getActivity())) {
                                    if (isChecked()) {
                                        if (getCheckedSLCCount() > 10)
                                            Utils.dialogForMessageRoute(getActivity(), getResources().getString(R.string.routeValidation));
                                        else {
                                            //navigate to map
                                            setNavigation();
                                        }
                                    }
                                } else
                                    Utils.dialogForMessage(getActivity(), getActivity().getResources().getString(R.string.no_internet_connection));
                                dialogComands.dismiss();
                            }
                        } else {
                            dialogComands.dismiss();
                        }
                    }
                } else {
                    objUtils.dialogForApptype(getActivity(), typeMessage);
                }

            }
        });

        RecyclerView.ItemDecoration decoration = new RecyclerView.ItemDecoration() {
            @Override
            public void onDraw(Canvas c, RecyclerView parent, RecyclerView.State state) {
                super.onDraw(c, parent, state);
                //empty
            }
        };

        objRecyclerView.addItemDecoration(decoration);

        objRecyclerView.addItemDecoration(new DividerItemDecoration(getActivity(), 0));
        objRecyclerView.setAdapter(adapterDialog);

        androidx.appcompat.widget.SearchView objSearchView = dialogComands.findViewById(R.id.svSLCs);
        objSearchView.setVisibility(View.GONE);
        // dialogButton.setOnTouchListener(Util.colorFilter());

        Rect displayRectangle = new Rect();
        Window window = getActivity().getWindow();

        window.getDecorView().getWindowVisibleDisplayFrame(displayRectangle);
        //dialogComands.getWindow().setLayout((int) (displayRectangle.width() * 0.9f), (int) (displayRectangle.height() * 0.7f));
        dialogComands.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT,
                WindowManager.LayoutParams.MATCH_PARENT);

        dialogComands.show();
    }

    void setModeApi() {
        if (Utils.isInternetAvailable(getActivity())) {

            dialog_wait.show();
            mListSetMode.clear();
            objApi.getSetMode(token, AppConstants.IS_SECURED).enqueue(new Callback<SetmodeCmd>() {
                @Override
                public void onResponse(Call<SetmodeCmd> call, Response<SetmodeCmd> response) {
                    objUtils.dismissProgressDialog(dialog_wait);
                    if (response.code() == 200) {

                        if (response.body() != null) {
                            SetmodeCmd mode = response.body();

                            if (mode.getStatus().equalsIgnoreCase("1")) {
                                java.util.List<DatumSetMode> objData = mode.getData();

                                for (int i = 0; i < objData.size(); i++) {
                                    DatumSetMode objDatum = new DatumSetMode();
                                    objDatum.setModeID(objData.get(i).getModeID());
                                    objDatum.setModeName(objData.get(i).getModeName());

                                    mListSetMode.add(objDatum);
                                }
                                dialog_set_mode_cmd_list(mListSetMode);
                            }
                        }
                    } else
                        objUtils.responseHandle(getActivity(), response.code(), response.errorBody());
                }

                @Override
                public void onFailure(Call<SetmodeCmd> call, Throwable t) {
                    objUtils.dismissProgressDialog(dialog_wait);
                }
            });
        } else {
            Utils.dialogForMessage(getActivity(), getResources().getString(R.string.no_internet_connection));
        }
    }

    void dialog_dim(int progress) {
        try {
            //final int[] progressGlobal = {0};
            final int[] progressGlobal = {progress};
            final Dialog dialog = new Dialog(getActivity());
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.getWindow().clearFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);
            dialog.setContentView(R.layout.custom_dialog_dim);
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));

            final TextView tvProgress = dialog.findViewById(R.id.title);

            TextView dialogButton = dialog.findViewById(R.id.btnOk);
            dialogButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    objUtils.dismissProgressDialog(dialogComands);

                    if (progressGlobal[0] > 70) {
                        dimValidationDialog(getActivity(), progressGlobal[0], dialog);
                    } else {
                        dialog.dismiss();
                        fireCommand(AppConstants.SWITCH_DIM, getCheckedSLC(), progressGlobal[0], 0);
                    }

                }
            });

            SeekBar seekBar = (SeekBar) dialog.findViewById(R.id.seekBar);

            tvProgress.setText("DIM: 0%");

            seekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
                @Override
                public void onProgressChanged(SeekBar seekBar, int progress,
                                              boolean fromUser) {
                    progressGlobal[0] = progress;
                    tvProgress.setText("DIM: " + String.valueOf(progress) + " %");
                }

                @Override
                public void onStartTrackingTouch(SeekBar seekBar) {
                }

                @Override
                public void onStopTrackingTouch(SeekBar seekBar) {

                }
            });

            Rect displayRectangle = new Rect();
            Window window = getActivity().getWindow();
            window.getDecorView().getWindowVisibleDisplayFrame(displayRectangle);
            dialog.getWindow().setLayout((int) (displayRectangle.width() * 0.9f), (int) (displayRectangle.height() * 0.8f));
            if (window != null) {
                window.addFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND); // This flag is required to set otherwise the setDimAmount method will not show any effect
                window.setDimAmount(0.5f); //0 for no dim to 1 for full dim
            }

            dialog.show();

            // getMapData(mClusterTest, date, type,googleMap12);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    void dummyData() {
        mList.clear();
        for (int i = 0; i < 10; i++) {
            com.cl.lg.pojo.ListResponse.List objList = new com.cl.lg.pojo.ListResponse.List();
            objList.setMacAddress("Rivera 20" + i);
            objList.setCreated("9/9/2019 19:19");
            objList.setSlcId("18" + i);
            mList.add(objList);
        }
        tvNorecordsList.setVisibility(View.GONE);
        rvStatus.setVisibility(View.VISIBLE);
        adapter.notifyDataSetChanged();
    }

    // slide the view from below itself to the current position
    public void slideUp(View view) {
        view.setVisibility(View.VISIBLE);
        TranslateAnimation animate = new TranslateAnimation(
                0,                 // fromXDelta
                0,                 // toXDelta
                view.getHeight(),  // fromYDelta
                0);                // toYDelta
        animate.setDuration(500);
        animate.setFillAfter(true);
        view.startAnimation(animate);

    }

    // slide the view from its current position to below itself
    public void slideDown(View view) {
        TranslateAnimation animate = new TranslateAnimation(
                0,                 // fromXDelta
                0,                 // toXDelta
                0,                 // fromYDelta
                view.getHeight()); // toYDelta
        animate.setDuration(500);
        animate.setFillAfter(true);
        view.startAnimation(animate);
    }

    /*
        @Override
        public void onClickStatusUI(int position, com.cl.lg.pojo.SLCStatus2.List objListDetail) {

            //dialog_status_detail_list(mList, objListDetail.getLatitude(), objListDetail.getLongitude());

            //load fragment
            FragmentManager fm = getActivity().getFragmentManager();
            FragmentTransaction fragmentTransaction = fm.beginTransaction();
            SLCDetailsPowerParamFragment fragment = new SLCDetailsPowerParamFragment();

            Bundle objBundle = new Bundle();
            objBundle.putString(AppConstants.LATTITUDE, objListDetail.getLatitude());
            objBundle.putString(AppConstants.LONGITUDE, objListDetail.getLongitude());
            objBundle.putInt(AppConstants.SLC_NO, objListDetail.getSlcNo());
            objBundle.putString(AppConstants.JSON_STRING, objListDetail.getJsonString());
            objBundle.putBoolean(AppConstants.ISFROMSTATUS, true);
            objBundle.putInt(AppConstants.UI_ID, id);
            objBundle.putString(AppConstants.UI_ID_status, statusParameter);

            fragment.setArguments(objBundle);

            fragmentTransaction.replace(R.id.frm1, fragment);
            fragmentTransaction.commitAllowingStateLoss();

        }
    */
    private static final String BACK_STACK_ROOT_TAG = "root_fragment";

    @Override
    public void onClickStatusUI(int position, StatusResponse2 objDialogResponse) {

        if (type.toString().equalsIgnoreCase(AppConstants.APP_TYPE_PAID)) {


            if (!objDialogResponse.getLastCommunicatedOn().isEmpty()) {
                FragmentManager fm = getActivity().getFragmentManager();
                //fm.popBackStack(BACK_STACK_ROOT_TAG, FragmentManager.POP_BACK_STACK_INCLUSIVE);

                FragmentTransaction fragmentTransaction = fm.beginTransaction();
                SLCDetailsPowerParamFragment fragment = new SLCDetailsPowerParamFragment();

                Bundle objBundle = new Bundle();
                objBundle.putString(AppConstants.LATTITUDE, objDialogResponse.getLatitude());
                objBundle.putString(AppConstants.LONGITUDE, objDialogResponse.getLongitude());
                objBundle.putString(AppConstants.SLC_NO, objDialogResponse.getSlcNo());
                objBundle.putString(AppConstants.JSON_STRING, objDialogResponse.getJsonString());
                objBundle.putBoolean(AppConstants.ISFROMSTATUS, true);
                objBundle.putInt(AppConstants.UI_ID, id);
                objBundle.putString(AppConstants.UI_ID_status, statusParameter);
                objBundle.putBoolean(AppConstants.IS_FROM_DASHBOARD, false);
                objBundle.putString(AppConstants.MODE_TEXT, "");

                objBundle.putInt(AppConstants.IS_SEARCH_ON, llSlcCommands.getVisibility());
                objBundle.putBoolean(AppConstants.IS_NEAR_ME, isNearMePressNav);

                fragment.setArguments(objBundle);

                //fragmentTransaction.addToBackStack(BACK_STACK_ROOT_TAG);
                /*fragmentTransaction.replace(R.id.frm1, fragment);
                fragmentTransaction.addToBackStack(null);
                fragmentTransaction.commitAllowingStateLoss();*/


                fragmentTransaction.add(R.id.frm1, fragment);
                fragmentTransaction.hide(StatusFragment.this);
                fragmentTransaction.addToBackStack(StatusFragment.class.getName());
                fragmentTransaction.commit();
            }
        } else
            objUtils.dialogForApptype(getActivity(), typeMessage);

    }

   /* @RequiresApi(api = Build.VERSION_CODES.M)
    void checkIsPasswordExist(Activity activity, EditText edtPassword,int cmdType) {

        String temp_pass = spf_login.getString(AppConstants.TEMP_PASS, "");
        if (temp_pass.equals("")) {
            if (edtPassword.getText().toString().equals("")) {
                Utils.dialogForMessage(activity, getResources().getString(R.string.empty_password));
            } else {
                getTokenApi(edtPassword.getText().toString(), cmdType, dialog);
            }
            //Utils.dialogForMessage(LoginActivity.this, getResources().getString(R.string.empty_password));
        } else {
            //dialogOption();
            dialogOptionTouch(cmdType);
        }
    }*/


    @RequiresApi(api = Build.VERSION_CODES.M)
    void dialogOptionTouch(final int cmdType, boolean isFromSetCommand) {

        final Dialog dialogTouch = new Dialog(getActivity());
        dialogTouch.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialogTouch.getWindow().clearFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);
        dialogTouch.setContentView(R.layout.custom_dialog_touch_id);
        dialogTouch.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));

        dialogTouch.setCancelable(true);

        checkFingurePrint(cmdType, dialogTouch, isFromSetCommand);

        TextView tvTouchIDAgain = dialogTouch.findViewById(R.id.tvTouchIDAgain);
        tvTouchIDAgain.setVisibility(View.GONE);

        View objView = dialogTouch.findViewById(R.id.view);
        objView.setVisibility(View.GONE);

        TextView tvUserName = dialogTouch.findViewById(R.id.tvUserName);
        ImageView ivFingurePrint = dialogTouch.findViewById(R.id.ivFingurePrint);

        Glide.with(this).load(R.drawable.fingure_print).into(ivFingurePrint);

        tvTouchIDAgain.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(getActivity(), getResources().getString(R.string.put_figure_on_hw), Toast.LENGTH_LONG).show();
                checkFingurePrint(cmdType, dialogTouch, isFromSetCommand);
            }
        });

        tvUserName.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialogTouch.dismiss();
                dialog_password(cmdType, isFromSetCommand);
            }
        });

        Rect displayRectangle = new Rect();
        Window window = getActivity().getWindow();
        window.getDecorView().getWindowVisibleDisplayFrame(displayRectangle);
        dialogTouch.getWindow().setLayout((int) (displayRectangle.width() * 0.9f), (int) (displayRectangle.height() * 0.8f));
        if (window != null) {
            window.addFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND); // This flag is required to set otherwise the setDimAmount method will not show any effect
            window.setDimAmount(0.5f); //0 for no dim to 1 for full dim
        }
        dialogTouch.show();
        //getMapData(mClusterTest, date, type,googleMap12);
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    void checkFingurePrint(int cmdType, Dialog dialogTouch, boolean isFromSetMode) {
        // Check whether the device has a Fingerprint sensor.

        try {
            if (fingerprintManager != null) {
                if (!fingerprintManager.isHardwareDetected()) {
                    /**
                     * An error message will be displayed if the device does not contain the fingerprint hardware.
                     * However if you plan to implement a default authentication method,
                     * you can redirect the user to a default authentication activity from here.
                     * Example:
                     * Intent intent = new Intent(this, DefaultAuthenticationActivity.class);
                     * startActivity(intent);
                     */
                    Utils.dialogForMessage(getActivity(), getResources().getString(R.string.your_device_does_not_have_a_fingerprint_sensor));
                } else {
                    // Checks whether fingerprint permission is set on manifest
                    if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.USE_FINGERPRINT) != PackageManager.PERMISSION_GRANTED) {
                        Toast.makeText(getActivity(), getResources().getString(R.string.fingerprint_authentication_permission_not_enabled), Toast.LENGTH_LONG).show();
                    } else {
                        // Check whether at least one fingerprint is registered
                        if (!fingerprintManager.hasEnrolledFingerprints()) {
                            Utils.dialogForMessage(getActivity(), getResources().getString(R.string.to_use_this_feature_please_setup_your_fingerprints_from_the_settings_tab_on_your_phone));
                        } else {
                            // Checks whether lock screen security is enabled or not
                            if (!keyguardManager.isKeyguardSecure()) {
                                Utils.dialogForMessage(getActivity(), getResources().getString(R.string.lock_screen_security_not_enabled_in_settings));
                            } else {
                                generateKey();
                                if (cipherInit()) {
                                    FingerprintManager.CryptoObject cryptoObject = new FingerprintManager.CryptoObject(cipher);
                                    FingerprintHandler helper = new FingerprintHandler(getActivity(), this, cmdType, dialogTouch, isFromSetMode);
                                    helper.startAuth(fingerprintManager, cryptoObject);
                                }
                            }
                        }
                    }
                }
            } else {
                Toast.makeText(getActivity(), getResources().getString(R.string.your_device_does_not_have_a_fingerprint_sensor), Toast.LENGTH_LONG).show();
            }
        } catch (Exception e) {
            e.printStackTrace();
            Toast.makeText(getActivity(), getResources().getString(R.string.http_error_msg), Toast.LENGTH_LONG).show();
        }
    }

    @TargetApi(Build.VERSION_CODES.M)
    protected void generateKey() {

        try {
            keyStore = KeyStore.getInstance("AndroidKeyStore");
        } catch (Exception e) {
            e.printStackTrace();
        }

        KeyGenerator keyGenerator;
        try {
            keyGenerator = KeyGenerator.getInstance(KeyProperties.KEY_ALGORITHM_AES, "AndroidKeyStore");
        } catch (NoSuchAlgorithmException | NoSuchProviderException e) {
            throw new RuntimeException("Failed to get KeyGenerator instance", e);
        }

        try {
            keyStore.load(null);
            keyGenerator.init(new
                    KeyGenParameterSpec.Builder(KEY_NAME,
                    KeyProperties.PURPOSE_ENCRYPT |
                            KeyProperties.PURPOSE_DECRYPT)
                    .setBlockModes(KeyProperties.BLOCK_MODE_CBC)
                    .setUserAuthenticationRequired(true)
                    .setEncryptionPaddings(
                            KeyProperties.ENCRYPTION_PADDING_PKCS7)
                    .build());
            keyGenerator.generateKey();
        } catch (NoSuchAlgorithmException |
                InvalidAlgorithmParameterException
                | CertificateException | IOException e) {
            throw new RuntimeException(e);
        }
    }

    @TargetApi(Build.VERSION_CODES.M)
    public boolean cipherInit() {
        try {
            cipher = Cipher.getInstance(KeyProperties.KEY_ALGORITHM_AES + "/" + KeyProperties.BLOCK_MODE_CBC + "/" + KeyProperties.ENCRYPTION_PADDING_PKCS7);
        } catch (NoSuchAlgorithmException | NoSuchPaddingException e) {
            throw new RuntimeException("Failed to get Cipher", e);
        }
        try {
            keyStore.load(null);
            SecretKey key = (SecretKey) keyStore.getKey(KEY_NAME,
                    null);
            cipher.init(Cipher.ENCRYPT_MODE, key);
            return true;
        } catch (KeyPermanentlyInvalidatedException e) {
            return false;
        } catch (KeyStoreException | CertificateException | UnrecoverableKeyException | IOException | NoSuchAlgorithmException | InvalidKeyException e) {
            throw new RuntimeException("Failed to init Cipher", e);
        }
    }

    @Override
    public void onSuccessForLogin() {
    }

    @Override
    public void onSuccessForStatusCmd(int cmdType, Dialog dialog, boolean isFromSetMode) {
        if (objUtils.isInternetAvailable(getActivity())) {
            getTokenApi(spf_login.getString(AppConstants.TEMP_PASS, ""), cmdType, dialog, isFromSetMode);
        } else
            Utils.dialogForMessage(getActivity(), getResources().getString(R.string.no_internet_connection));
    }


    void dimValidationDialog(Activity activity, int progress, Dialog dialog) {
        androidx.appcompat.app.AlertDialog.Builder builder = new androidx.appcompat.app.AlertDialog.Builder(getActivity());
        builder.setMessage(activity.getResources().getString(R.string.dimming_validation_msg))
                .setCancelable(false)
                .setTitle(activity.getString(R.string.app_name))
                .setPositiveButton(activity.getResources().getString(R.string.yes), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                        dialog.dismiss();
                        //body.getStatus();
                        fireCommand(AppConstants.SWITCH_DIM, getCheckedSLC(), progress, 0);
                    }
                })
                .setNegativeButton(activity.getResources().getString(R.string.no), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog1, int which) {
                        dialog1.dismiss();
                        //dialog_dim(progress);
                    }
                });

        //Creating dialog box
        androidx.appcompat.app.AlertDialog alert = builder.create();
        //Setting the title manually
        alert.requestWindowFeature(Window.FEATURE_NO_TITLE);
        alert.show();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        SharedPreferences.Editor edit = spf.edit();

        //edit.remove(AppConstants.IS_SEARCH_PRESSED_STATUS);
        /*edit.remove(AppConstants.UI_ID);
        edit.remove(AppConstants.IS_SEARCH_ON);
        edit.remove(AppConstants.IS_NEAR_ME);
        edit.remove(AppConstants.IS_SEARCH_ON_SENT_CMD);

        edit.remove(AppConstants.G_ID_VALUE_NAVE);
        edit.remove(AppConstants.SLC_ID_VALUE_NAVE);
        edit.remove(AppConstants.GP_ID_VALUE_NAVE);

        edit.remove(AppConstants.SLC_ID_NAV);
        edit.remove(AppConstants.G_ID_NAV);
        edit.remove(AppConstants.GP_ID_NAV);*/

        //edit.apply();
        //Log.i("***", "onDestroy");

    }
}
  /*  void navigationRouteMapBox() {

        Bundle objBundle = new Bundle();
        ArrayList<LatLng> latLngs = new ArrayList<>();


        for (int i = 0; i < mListFinal2.size(); i++) {
            if (mListFinal2.get(i).isChecked()) {
                LatLng latLng = new LatLng(Double.valueOf(mListFinal2.get(i).getLatitude()), Double.valueOf(mListFinal2.get(i).getLongitude()));
                latLngs.add(latLng);
            }
        }

        Double lat, lng;
        lat = Double.valueOf(spf.getString(AppConstants.LATTITUDE, ""));
        lng = Double.valueOf(spf.getString(AppConstants.LONGITUDE, ""));

        Location currLocation = new Location("Current");
        currLocation.setLatitude(lat);
        currLocation.setLongitude(lng);

        double distance;

        Set<LatLong> hash_Set = new TreeSet<LatLong>();
        int count = 0;

        for (int i = 0; i < latLngs.size(); i++) {

            Location location = new Location("Point" + i);
            location.setLongitude(latLngs.get(i).longitude);
            location.setLatitude(latLngs.get(i).latitude);
            distance = currLocation.distanceTo(location);

            LatLong objLatLng = new LatLong(latLngs.get(i).latitude, latLngs.get(i).longitude, distance);
            hash_Set.add(objLatLng);
            count++;
        }

        StringBuilder sb1 = new StringBuilder();
        for (LatLong lg : hash_Set) {
            sb1.append(lg.getLat() + "," + lg.getLng() + "#");
            Log.i(AppConstants.TAG, "Distance: " + lg.getDistance() + " | Lat: " + lg.getLng() + " Lng: " + lg.getLng());
        }

        String sortedLatLng = sb1.toString();
        Log.i(AppConstants.TAG, sortedLatLng);

        objBundle.putInt(AppConstants.SIZE_ROUTE, count);
        objBundle.putString(AppConstants.SOURTED_ROUTE, sortedLatLng);
        RouteFragment objFragment = new RouteFragment();
        objFragment.setArguments(objBundle);

        objUtils.loadFragment(objFragment, getActivity());

    }
*/


    /*if(value.equalsIgnoreCase("0")){
                        value="Manual";
                        }else if(value.equalsIgnoreCase("1")){
                            value="Photocell";
                        }else if(value.equalsIgnoreCase("2")){
                            value="Scheduled";
                        }else if(value.equalsIgnoreCase("3")){
                            value="Astro Clock";
                        }else if(value.equalsIgnoreCase("4")){
                            value="Astro Clock with Photocell Override";
                        }else if(value.equalsIgnoreCase("5")){
                            value="Civil Twilight";
                        }else if(){

                        }*/

    /*if (s.toString().trim().equalsIgnoreCase("")) {
        filterList = mListSearch;
        adapterDialog.notifyDataSetChanged();
    } else {
        ArrayList<CommonDialogResponse> temp = new ArrayList();
        for (CommonDialogResponse d : filterList) {
            //or use .equal(text) with you want equal match
            //use .toLowerCase() for better matches
            if (d.getValue().contains(s.toString())) {
                temp.add(d);
            }
        }
        //update recyclerview
        filterList = temp;
        adapterDialog.notifyDataSetChanged();
    }*/