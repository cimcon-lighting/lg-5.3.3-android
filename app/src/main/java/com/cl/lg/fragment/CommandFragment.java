package com.cl.lg.fragment;

import android.app.Fragment;
import android.app.FragmentManager;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.cl.lg.R;
import com.cl.lg.adapters.DialogCommandAdapter;
import com.cl.lg.utils.Utils;

import butterknife.BindView;
import butterknife.ButterKnife;

public class CommandFragment extends Fragment implements DialogCommandAdapter.MyCallbackForControlDialog, View.OnClickListener {

    @BindView(R.id.rvDialog)
    RecyclerView rvDialog;

    @BindView(R.id.btn_ok)
    TextView btn_ok;

    @BindView(R.id.btnBack)
    ImageView btnBack;

    Utils objUtils;
    DialogCommandAdapter adapter;
    View view;
    Bundle objBundle;
    boolean isFromStatus = true;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_sent_cmd, null);
        init();
        return view;
    }

    private void init() {
        ButterKnife.bind(this, view);

        objBundle = getArguments();
        if (objBundle != null) {
            isFromStatus = objBundle.getBoolean("isFromStatus");
        }
        objUtils = new Utils();
        //rvDialog.addItemDecoration(new DividerItemDecoration(getActivity(), 0));
        rvDialog.setHasFixedSize(true);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
        rvDialog.setLayoutManager(linearLayoutManager);
        adapter = new DialogCommandAdapter(getActivity(), objUtils.getCommands(getActivity()), this);
        rvDialog.setAdapter(adapter);

        btn_ok.setOnClickListener(this);
        btnBack.setOnClickListener(this);
    }

    @Override
    public void onClickForControl(int position, String slcNo) {
        if (isFromStatus)
            StatusFragment.getInstance().operationComandFire(slcNo);
        else
            SLCDetailsPowerParamFragment.getInstance().operationComandFire(slcNo);

        getActivity().onBackPressed();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_ok:
            case R.id.btnBack:
                getActivity().onBackPressed();
        }
    }
}
