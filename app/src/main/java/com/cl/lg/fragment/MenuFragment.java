package com.cl.lg.fragment;

import android.app.Fragment;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.cl.lg.R;
import com.cl.lg.activities.LoginActivity;
import com.cl.lg.activities.MainActivity;
import com.cl.lg.utils.AppConstants;
import com.cl.lg.utils.Utils;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MenuFragment extends Fragment implements View.OnClickListener {

    @BindView(R.id.tvLogout)
    TextView tvLogout;

    @BindView(R.id.llProfile)
    LinearLayout llProfile;

    @BindView(R.id.llGS)
    LinearLayout llGS;

    @BindView(R.id.llLogout)
    LinearLayout llLogout;
    @BindView(R.id.View1)
    View view1;

    SharedPreferences spf;
    View view;

    Utils objUtils;

    String cliet_typ;
    boolean isZigbeeContains;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_menu, null);
        init();
        return view;
    }

    void init() {
        ButterKnife.bind(this, view);
        llLogout.setOnClickListener(this);
        llProfile.setOnClickListener(this);
        llGS.setOnClickListener(this);
        objUtils = new Utils();
        spf = getActivity().getSharedPreferences(AppConstants.SPF, Context.MODE_PRIVATE);
        cliet_typ=spf.getString(AppConstants.CLIENT_TYPE, "");
        isZigbeeContains=spf.getBoolean(AppConstants.isZigbeeContains,false);


        if (isZigbeeContains) {
            llGS.setVisibility(View.VISIBLE);
            view1.setVisibility(View.VISIBLE);
        } else {
            llGS.setVisibility(View.GONE);
            view1.setVisibility(View.GONE);
        }

        ((MainActivity)getActivity()).selectDashboard(false);
        ((MainActivity)getActivity()).selectStatus(false);
        ((MainActivity)getActivity()).selectMap(false);
        ((MainActivity)getActivity()).selectMenu(true);
    }

    void dialog() {
        androidx.appcompat.app.AlertDialog.Builder builder = new androidx.appcompat.app.AlertDialog.Builder(getActivity());
        builder.setMessage(getResources().getString(R.string.do_you_want_to_logout))
                .setCancelable(true)
                .setTitle(getResources().getString(R.string.logout))
                .setPositiveButton(getResources().getString(R.string.yes), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        //spf.edit().clear().apply();
                        getActivity().finish();
                        startActivity(new Intent(getActivity(), LoginActivity.class));
                        dialogInterface.dismiss();
                        spf.edit().putBoolean(AppConstants.ISLOGGEDIN,false).commit();
                    }
                });
        builder.setNegativeButton(getResources().getString(R.string.no), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        //Creating dialog box
        androidx.appcompat.app.AlertDialog alert = builder.create();
        //Setting the title manuallyc
        alert.requestWindowFeature(Window.FEATURE_NO_TITLE);
        alert.show();
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.llGS:
                objUtils.loadFragment(new GatewayStatus(), getActivity());
                break;

            case R.id.llLogout:
                dialog();
                break;

            case R.id.llProfile:
                objUtils.loadFragment(new ProfileFragment(), getActivity());
                break;
        }
    }
}